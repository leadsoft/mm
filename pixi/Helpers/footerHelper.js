function disableEnableButtons(enable) {
    if (enable) {
        footer.infoBtn.texture = oAL.panel['info'];
        /*footer.cImage.texture = oAL.panel['coins_enable_n']*/
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        if (settings.MIN_BET === 10) {
            footer.plusButton.texture = oAL.panel['disable_plus_coin'];
        } else {
            footer.plusButton.texture = oAL.panel['plus_coin'];
        }

        if (settings.MIN_BET === 0.01) {
            footer.minusButton.texture = oAL.panel['coins_minus_d'];
        } else {
            footer.minusButton.texture = oAL.panel['coins_minus_n'];
        }

        /*Sound*/
        if (footer.isMuted) {
            footer.soundButton.texture = oAL.panel['mute_sound'];
        } else {
            footer.soundButton.texture = oAL.panel['sound'];
        }
        /*QuickSpin*/
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['normal_spin'];
        } else {
            footer.fastSpinButton.texture = oAL.panel['quick_spin'];
        }
        /*AutoSpin*/
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['cancel'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
        footer.homeBtn.texture = oAL.panel['home']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        if (!settings.DEVICE.includes('D')) {
            /*footer.mobileCoinsEnable.texture = oAL.panel['coins_enable_n'];*/
            /*footer.mobileCoinsClose.texture = oAL.panel['mobile_coins_close_n'];*/
            footer.mobileCoinMinus.texture = oAL.panel['coins_minus_n'];
            footer.mobileCoinPlus.texture = oAL.panel['coins_plus_n'];
            /*footer.mobileFastSpin.texture = oAL.panel['mobile_quickspin_n'];*/
            /*footer.mobileNormalSpin.texture = oAL.panel['mobile_normalspin_n'];*/
            footer.mobileMenu.texture = oAL.panel['menu_n'];
        }
    } else {
        footer.infoBtn.texture = oAL.panel['disable_info'];
        footer.plusButton.texture = oAL.panel['disable_plus_coin'];
        footer.minusButton.texture = oAL.panel['coins_minus_d'];
        //footer.cImage.texture = oAL.panel['coins_enable_d']
        console.log(footer.coinMenuBtn, 'footer.coinMenuBtn');
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];

        /*Sound*/
        if (footer.isMuted) {
            footer.soundButton.texture = oAL.panel['disable_mute_sound'];
        } else {
            footer.soundButton.texture = oAL.panel['disable_sound'];
        }
        /*QuickSpin*/
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['disable_quick_spin'];
        } else {
            footer.fastSpinButton.texture = oAL.panel['disable_normal_spin'];
        }
        /*AutoSpin*/
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['disabled_cancel'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }

        footer.homeBtn.texture = oAL.panel['home']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];

        if (!settings.DEVICE.includes('D')) {
            /*footer.mobileCoinsEnable.texture = oAL.panel['coins_enable_d'];*/
            /*footer.mobileCoinsClose.texture = oAL.panel['mobile_coins_close_d'];*/

            footer.mobileCoinMinus.texture = oAL.panel['coins_minus_d'];
            footer.mobileCoinPlus.texture = oAL.panel['coins_plus_d'];
            //footer.mobileFastSpin.texture = oAL.panel['mobile_quickspin_d'];
            //footer.mobileNormalSpin.texture = oAL.panel['mobile_normalspin_d'];
            footer.coinMenuContainer.getChildByName('coin-btn').texture = oAL.panel['menu_close_d'];
            footer.mobileMenu.texture = oAL.panel['menu_d'];
        }
    }

    footer.infoBtn.interactive = enable;
    footer.soundButton.interactive = enable;
    footer.autoSpinButton.interactive = enable;
    footer.fastSpinButton.interactive = enable;
    footer.plusButton.interactive = enable;
    footer.minusButton.interactive = enable;
    footer.homeBtn.interactive = enable;
    if (!settings.DEVICE.includes('D')) {
        footer.mobileCoinsEnable.interactive = enable;
        /*footer.mobileCoinsClose.interactive = enable;*/
        footer.mobileCoinMinus.interactive = enable;
        footer.mobileCoinPlus.interactive = enable;
        /*footer.mobileFastSpin.interactive = enable;*/
        /*footer.mobileNormalSpin.interactive = enable;*/
        footer.coinMenuContainer.getChildByName('coin-btn').interactive = enable;
        footer.mobileMenu.interactive = enable;
    }
}

function addText(isMobile) {
    let style = {font: "Arial", fill: isMobile ? "0xffffff" : "0x000000", fontWeight: "bold"};

    let linesBet = new PIXI.Text('LINES', style);
    footer.linesBetImage.addChild(linesBet);
    linesBet.name = 'lines-bet-text';

    let bet = new PIXI.Text("BET", style);
    footer.betImage.addChild(bet);
    bet.name = 'bet-text';

    let win = new PIXI.Text("WIN", style);
    win.name = 'win-text';
    footer.winImage.addChild(win);
    footer.winImage.zIndex = 1;

    let balance = new PIXI.Text("BALANCE", style);
    balance.name = 'balance-text';
    footer.balanceImage.addChild(balance);
    footer.balanceImage.zIndex = 1;

    let totalBalance = new PIXI.Text("TOTAL WIN", style);
    totalBalance.name = 'total-balance-text';
    footer.totalBalanceImage.addChild(totalBalance);
    footer.totalBalanceImage.zIndex = 1;

    let bonus = new PIXI.Text("BONUS", style);
    bonus.name = 'bonus-text';
    footer.bonusImage.addChild(bonus);
    footer.bonusImage.zIndex = 1;

    let betsToQualify = new PIXI.Text("BETS TO QUALIFY", style);
    betsToQualify.style.fontSize = 20;
    betsToQualify.x = (footer.betsToQualifyImage.width / 2) - (betsToQualify.width / 2);
    betsToQualify.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.betsToQualifyImage.addChild(betsToQualify);

    let betsMade = new PIXI.Text("BETS MADE", style);
    betsMade.style.fontSize = 20;
    betsMade.x = (footer.betsMadeImage.width / 2) - (betsMade.width / 2);
    betsMade.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.betsMadeImage.addChild(betsMade);

    let timeLeft = new PIXI.Text("TIME LEFT", style);
    timeLeft.style.fontSize = 20;
    timeLeft.x = (footer.timeLeftImage.width / 2) - (timeLeft.width / 2);
    timeLeft.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.timeLeftImage.addChild(timeLeft);

    let rankBalance = new PIXI.Text("RANK BALANCE", style);
    rankBalance.style.fontSize = 20;
    rankBalance.x = (footer.rankBalanceImage.width / 2) - (rankBalance.width / 2);
    rankBalance.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.rankBalanceImage.addChild(rankBalance);

    let rank = new PIXI.Text("RANK", style);
    rank.style.fontSize = 20;
    rank.x = (footer.rankImage.width / 2) - (rank.width / 2);
    rank.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.rankImage.addChild(rank);

    footer.balanceImage.addChild(footer.userBalance);
    footer.balanceImage.zIndex = 1;

    if (isMobile) {
        footer.balanceImage.x = 780;
        footer.bonusImage.x = 610;
        footer.winImage.x = 210;
        footer.betImage.x = 70;
    }


    /*setTimeout(() => {
        textAdjustment(isMobile)
    }, 1000)*/

}

function textAdjustment(isMobile) {

    let win = footerContainer.getChildByName('win-image').getChildByName('win-text')
    let linesBet = footerContainer.getChildByName('lines-bet-image').getChildByName('lines-bet-text')
    let balance = footerContainer.getChildByName('balance-image').getChildByName('balance-text')
    let totalBalance = footerContainer.getChildByName('total-balance-image').getChildByName('total-balance-text')
    let bonus = footerContainer.getChildByName('bonus-image').getChildByName('bonus-text')
    let bet = footerContainer.getChildByName('bet-image').getChildByName('bet-text')

    if (isMobile) {
        console.log(window.innerWidth > window.innerHeight, 'device position');
        if (window.innerWidth > window.innerHeight) {
            linesBet.style.fontSize = 20;
            win.style.fontSize = 17;
            balance.style.fontSize = 17;
            totalBalance.style.fontSize = 17;
            bonus.style.fontSize = 17;
            bet.style.fontSize = 20;
        } else {
            win.style.fontSize = 30;
            linesBet.style.fontSize = 30;
            balance.style.fontSize = 30;
            totalBalance.style.fontSize = 30;
            bonus.style.fontSize = 30;
            bet.style.fontSize = 30;
        }

        function resetPosition(){
            linesBet.x = footer.linesBetImage.width / 2 - linesBet.width / 2;
            linesBet.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            win.x = footer.winImage.width / 2 - win.width / 2;
            win.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            balance.x = (footer.balanceImage.width / 2) - (balance.width / 2);
            balance.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            totalBalance.x = (footer.totalBalanceImage.width / 2) - (totalBalance.width / 2);
            totalBalance.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            bonus.x = (footer.bonusImage.width / 2) - (bonus.width / 2);
            bonus.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            bet.x = footer.betImage.width / 2 - bet.width / 2;
            bet.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            footer.userBalance.y = footer.bonusImage.height - footer.userBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.userBalance.x = footer.bonusImage.width / 2 - footer.userBalance.width / 2;

            footer.linesBetAmount.y = footer.linesBetImage.height - footer.linesBetAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.linesBetAmount.x = footer.linesBetImage.width / 2 - footer.linesBetAmount.width / 2;

            footer.userBonusBalance.y = footer.bonusImage.height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.userBonusBalance.x = footer.bonusImage.width / 2 - footer.userBonusBalance.width / 2;

            footer.betAmount.y = footer.betImage.height - footer.betAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.betAmount.x = footer.betImage.width / 2 - footer.betAmount.width / 2;

            footer.winAmount.y = footer.winImage.height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.winAmount.x = footer.winImage.width / 2 - footer.winAmount.width / 2;
        }
        resetPosition()
        setTimeout(function () {
            resetPosition();
        }, 200)
    } else {
        linesBet.text = 'BET LINES'
        footer.userBalance.y = footer.userBalance.parent.height - footer.userBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
        footer.userBalance.x = footer.bonusImage.width / 2 - footer.userBalance.width / 2;

        footer.linesBetAmount.y = footer.linesBetImage.height - footer.linesBetAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
        footer.linesBetAmount.x = footer.linesBetImage.width / 2 - footer.linesBetAmount.width / 2;

        footer.userBonusBalance.y = footer.bonusImage.height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
        footer.userBonusBalance.x = footer.bonusImage.width / 2 - footer.userBonusBalance.width / 2;

        footer.betAmount.y = footer.betImage.height - footer.betAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
        footer.betAmount.x = footer.betImage.width / 2 - footer.betAmount.width / 2;

        footer.winAmount.y = footer.winImage.height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
        footer.winAmount.x = footer.winImage.width / 2 - footer.winAmount.width / 2;

    }

}


