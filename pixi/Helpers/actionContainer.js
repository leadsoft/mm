function loadActionContainerTextures(texturePack) {
    oAL.panel['menu'] = {
        'menu_mask_1': {
            'mobile': {
                'portrait': texturePack.textures['mobile/portrait/baseGame/menu1Mask.png.png'],
                'landscape': texturePack.textures['mobile/landscape/baseGame/menu1Mask.png.png']
            },
            'desktop': '',
        },
        'menu_mask_2': {
            'mobile': {
                'portrait': texturePack.textures['mobile/portrait/baseGame/menu2Mask.png'],
                'landscape': texturePack.textures['mobile/landscape/baseGame/menu2Mask.png']
            },
            'desktop': '',
        },
        'normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/menu_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_n.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/menu_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_h.png'],
        },
        'close_normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/menu_close_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_close_n.png'],
        },
        'close_hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/menu_close_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_close_h.png'],
        },
        'info_bar': {
            'mobile': {
                'portrait': texturePack.textures['mobile/portrait/baseGame/menu_mask_2.png'],
                'landscape': texturePack.textures['mobile/landscape/baseGame/menu_mask_2.png']
            },
            'desktop': texturePack.textures['desktop/baseGame/menu_close_h.png'],
        }
    };

    oAL.panel['coins_menu'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/coins_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/coins_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_d.png'],
        },
        'close_normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/coins_close_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_close_n.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/coins_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_h.png'],
        }
    };

    oAL.panel['spin'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/spin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/spin_n.png'],
        },
        'stop': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/stopSpin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/stop_lg_n.png'],
        },
        'stop_hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/stop_lg_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/stop_lg_h.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/spin_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/spin_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/spin_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/spin_h.png'],
        }
    };

    oAL.panel['autospin'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/autoplay_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/autoplay_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/autoplay_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/autoplay_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/autoplay_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/autoplay_h.png'],
        },
        'cancel': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/cancelAutoplay_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_n.png'],
        },
        'cancel_hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/cancelAutoplay_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_h.png'],
        },
        'disabled_cancel': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/cancelAutoplay_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_d.png'],
        },
    };

    oAL.panel['home'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/home_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/home_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/home_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/home_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/home_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/home_h.png'],
        },
        'cancel': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/cancelAutoplay_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_n.png'],
        },
        'cancel_hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/cancelAutoplay_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_h.png'],
        },
        'disabled_cancel': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/cancelAutoplay_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_d.png'],
        },
    };

    oAL.panel['fastspin'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/normalSpin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/normalSpin_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/normalSpin_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/normalSpin_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/normalSpin_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/normalSpin_h.png'],
        },
        'quick': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/quickspin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/quickspin_n.png'],
        },
        'quick_hover': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/quickspin_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/quickspin_h.png'],
        },
        'quick_disable': {
            'mobile': texturePack.textures['mobile/Sharedbuttons/quickspin_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/quickspin_d.png'],
        },
    };
}

function setActionContainerTextures() {
    console.log('set textures')
    let isMobile = !settings.DEVICE.includes('D');
    let key = 'desktop';
    if (isMobile)
        key = 'mobile';


    if(isMobile){
        footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][key];
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][key];
        footer.mobileInfoBar.texture = oAL.panel['menu']['menu_mask_2'][key][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait']; /*TODO: on desktop we need regenerate json*/
        footer.sliderBar.texture = oAL.panel['menu']['menu_mask_2'][key][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait']; /*TODO: on desktop we need regenerate json*/
    }

    footer.spinButtonSprite.texture = oAL.panel['spin']['normal'][key];
    footer.autoSpinButton.texture = oAL.panel['autospin']['normal'][key];
    footer.homeBtn.texture = oAL.panel['home']['normal'][key];
    footer.darkSpinButtonSprite.texture = oAL.panel['spin']['disabled'][key];//new PIXI.Sprite(oAL.panel.spin_disabled);
    footer.stopSpinButtonSprite.texture = oAL.panel['spin']['stop'][key];//new PIXI.Sprite(oAL.panel.stop_spin);
    footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][key]
}

function setActionContainerPosition() {
    console.log('set position')
    let isMobile = !settings.DEVICE.includes('D');

    if(isMobile){
        footer.mobileMenu.y = (function () {
            let position = window.innerWidth > window.innerHeight ? 100 : 0;
            return position;
        }());

        footer.mobileMenu.x = (function () {
            let position = window.innerWidth > window.innerHeight ? 0 : GAME_WIDTH - footer.mobileMenu.width - settings.ACTION_CONTAINER_MARGIN;
            return position;
        }());

        footer.coinMenuContainer.y = (function () {
            let position = 0;
            if (window.innerWidth > window.innerHeight) {
                position = footer.mobileMenu.y + footer.mobileMenu.height + settings.ACTION_CONTAINER_BUTTONS_MARGIN;
            }
            return position;
        }());

        footer.coinMenuContainer.x = (function () {
            let position = window.innerWidth > window.innerHeight ? 0 : footer.mobileMenu.x - ((footer.mobileMenu.x - (GAME_WIDTH / 2 - footer.spinButtonSprite.width / 2)) / 2 - footer.coinMenuContainer.width / 2)
            return position;
        }());
    }

    /**SPIN*/
    footer.stopSpinButtonSprite.x = footer.darkSpinButtonSprite.x = footer.spinButtonSprite.x = (function () {
        let position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = 0//GAME_WIDTH / 2 - footer.spinButtonSprite.width / 2;
            }
        }

        return position;
    }());

    footer.stopSpinButtonSprite.y = footer.darkSpinButtonSprite.y = footer.spinButtonSprite.y = (function () {
        let position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = window.innerWidth > window.innerHeight ? GAME_HEIGHT / 2 - footer.spinButtonSprite.height / 2 : -footer.spinButtonSprite.height;//footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());
    /**END SPIN*/

    footer.autoSpinButton.x = (function () {
        let position = footerCoordinates.auto_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.auto_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = (footer.spinButtonSprite.x - footer.homeBtn.width) / 2 - footer.autoSpinButton.width / 2 + settings.ACTION_CONTAINER_MARGIN
            }
        }

        return position;
    }());
    footer.autoSpinButton.y = (function () {
        let position = footerCoordinates.auto_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = window.innerWidth > window.innerHeight ? footer.spinButtonSprite.y + footer.spinButtonSprite.height + settings.ACTION_CONTAINER_BUTTONS_MARGIN : 0;

        return position;
    }());

    footer.homeBtn.x = (function () {
        let position = 0//footerCoordinates.home_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.home_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = settings.ACTION_CONTAINER_MARGIN;
            }
        }

        return position;
    }());
    footer.homeBtn.y = (function () {
        let position = 0//footerCoordinates.home_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = window.innerWidth > window.innerHeight ? footer.autoSpinButton.y + footer.autoSpinButton.height + settings.ACTION_CONTAINER_BUTTONS_MARGIN : 0;

        return position;
    }());

    footer.amountPerLine.x = (function () {
        let position = 0//footerCoordinates.amount_per_line[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile)
            position = footerCoordinates.amount_per_line[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

        return position;
    }());
    footer.amountPerLine.y = (function () {
        let position = 0//footerCoordinates.amount_per_line[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile){
            position = (footer.amountPerLine.parent.height / 2) - (footer.amountPerLine.height / 2);
        }
        return position;
    }());

    footer.infoBtn.x = (function () {
        let position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

        return position;
    }());
    footer.infoBtn.y = (function () {
        let position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.soundButton.x = (function () {
        let position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

        return position;
    }());
    footer.soundButton.y = (function () {
        let position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.fastSpinButton.x = (function () {
        let position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

        return position;
    }());
    footer.fastSpinButton.y = (function () {
        let position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());
}

/**
 * ACTIONS
 * */
var menuActions = {
    pointerover() {
        footer.mobileMenuBtn.texture = oAL.panel['menu']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerout() {
        footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerdown() {
        if (footer.mobileMenu.status) {
            footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.mobileMenu.status = false;
        } else {
            footer.mobileMenuBtn.texture = oAL.panel['menu']['close_normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.mobileMenu.status = true;
        }
        this.openCloseInfoBar();

    },
    changeInfoBarTexture() {
        if (window.innerWidth > window.innerHeight) {

        } else {

        }
    },
    openCloseInfoBar(changeView = false) {
        if (changeView) {
            footer.mobileInfoBar.visible = false;
            footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.mobileMenu.status = false;
        } else {
            footer.mobileInfoBar.visible = true;
        }
        if (window.innerWidth > window.innerHeight) {
            footer.mobileInfoBar.y = 0;
            TweenMax.to(footer.mobileInfoBar, 0.5, {
                x: footer.mobileMenu.status && !changeView ? -footer.mobileInfoBar.width + footer.mobileMenuBtn.width - (footer.mobileMenuBtn.width / 2) : actionContainer.width
            });
        } else {
            footer.mobileInfoBar.x = 0;
            TweenMax.to(footer.mobileInfoBar, 0.5, {
                y: footer.mobileMenu.status && !changeView ? -footer.mobileInfoBar.height + footer.mobileMenuBtn.height - (footer.mobileMenuBtn.height / 2) : actionContainer.height
            });
        }
    }
};
var coinMenuBtnActions = {
    pointerover() {
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
    },
    pointerout() {
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
    },
    pointerdown() {
        if (footer.coinMenuContainer.status) {
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.coinMenuContainer.status = false;
        } else {
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['close_normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.coinMenuContainer.status = true;
        }

        this.openCloseSliderBar();
    },
    openCloseSliderBar(changeView = false) {
        if (changeView) {
            footer.sliderBar.visible = false;
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.coinMenuContainer.status = false;
        } else {
            footer.sliderBar.visible = true;
        }
        if (window.innerWidth > window.innerHeight) {
            footer.sliderBar.y = 0;
            TweenMax.to(footer.sliderBar, 0.5, {
                x: footer.coinMenuContainer.status && !changeView ? -footer.sliderBar.width + footer.coinMenuBtn.width - (footer.coinMenuBtn.width / 2) : actionContainer.width,
            });
        } else {
            footer.sliderBar.x = footer.coinMenuContainer.x;
            TweenMax.to(footer.sliderBar, 0.5, {
                y: footer.coinMenuContainer.status && !changeView ? -footer.sliderBar.height + footer.coinMenuBtn.height - (footer.coinMenuBtn.height / 2) : actionContainer.height,
            });
        }
    }
};
var spinActions = {
    pointerover() {
        footer.spinButtonSprite.texture = oAL.panel['spin']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerout() {
        footer.spinButtonSprite.texture = oAL.panel['spin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerdown() {
        if (footer.disableFooter)
            return;

        if (sound)
            button.play();
        setSettings.ACTIVITY = 8;

        if (isMobile) {
            footer.switchMobileCoinSelector(true)
        }
        if (footer.spinCounter === 0)
            goToFullScreen();
        footer.spinCounter++;
        spinH.activeSpinButton();
    }
};
var fastSpinActions = {
    pointerover() {
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick_hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
    },
    pointerout() {
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
    },
    pointerdown() {
        if (footer.disableFooter) {
            return;
        }
        setSettings.ACTIVITY = 8;

        if (sound)
            button.play()

        footer.isPressedQuick = !footer.isPressedQuick;
        if (footer.isPressedQuick) {
            spinH.fastSpin = true;
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            spinH.fastSpin = false;
            footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']

        }
    }
};
var autoSpinActions = {
    pointerover() {
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['cancel_hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
            /*self.autoSpinButton.texture = oAL.panel['cancel_autoplay_hover'];*/
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
            /*self.autoSpinButton.texture = oAL.panel['autoplay_hover'];*/
        }

    },
    pointerout() {
        if (self.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['cancel'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
    },
    pointerdown() {
        if (footer.disableFooter) {
            return;
        }
        setSettings.ACTIVITY = 8;
        if (sound)
            button.play()

        footer.isPressedAutospin = !footer.isPressedAutospin;
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['cancel_autoplay']; //TODO: need to load this texture
            spinH.activeSpinButton();
            spinH.autospin = true;
        } else {
            footer.autoSpinButton.texture = oAL.panel['autoplay']; ////TODO: need to load this texture
            spinH.autospin = false;
        }
    }
};
