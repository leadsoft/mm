function loadFooterContainerTextures(texturePack) {
    oAL.panel['balance_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/baseGame/balanceMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/freeGames/balanceMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/baseGame/balanceMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeGames/balanceMask.png']
            }
        },
        'desktop': ''/*(function () {
                    let key = mSource.toLowerCase() + (isMobile ? (window.innerHeight > window.innerWidth ? '/portrait' : '/landscape') : '') + '/baseGame/balanceMask.png';
                    return panelsElements.textures[key];
            }())*/
    };
    oAL.panel['bet_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/baseGame/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/freeGame/bonusMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/baseGame/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/baseGame/bonusMask.png']
            },
        },
        'desktop': ''
    };
    oAL.panel['bonus_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/baseGame/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/freeGame/bonusMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/baseGame/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeGame/bonusMask.png']
            },
        },
        'desktop': ''
    };

    oAL.panel['lines_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/baseGame/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/freeGame/bonusMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/baseGame/linesMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeGame/linesMask.png']
            }
        },
        'desktop': ''
    };
    oAL.panel['win_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/baseGame/winMask_long.png'],
                'freeGame': texturePack.textures['mobile/portrait/freeGame/winMask_long.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/baseGame/winMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeGame/winMask.png']
            }
        },
        'desktop': ''
    };
}

function setFooterContainerTextures() {

    let isMobile = !settings.DEVICE.includes('D');
    let device = isMobile ? 'mobile' : 'desktop';
    let key = 'desktop';
    let gameType = settings.FS_STATUS ? 'freeGame' : 'baseGame';
    if (isMobile) {
        if (window.innerWidth > window.innerHeight) {
            key = 'landscape';
        } else {
            key = 'portrait';
        }
    }

    if(isMobile){
        footer.balanceImage.texture = oAL.panel['balance_mask'][device][key][gameType]; //TODO: need version for desktop
        footer.totalBalanceImage.texture = oAL.panel['balance_mask'][device][key][gameType];
        footer.betImage.texture = oAL.panel['bet_mask'][device][key][gameType];
        footer.winImage.texture = oAL.panel['win_mask'][device][key][gameType];
        footer.bonusImage.texture = oAL.panel['bonus_mask'][device][key][gameType];
        footer.linesBetImage.texture = oAL.panel['lines_mask'][device][key][gameType];
        footer.linesBetImage.texture = oAL.panel['lines_mask'][device][key][gameType];
    }
}

function setFooterContainerPosition() {
    let isMobile = !settings.DEVICE.includes('D');
    footer.linesBetImage.x = (function () {
        let position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop']['landscape'].x;
            } else {
                footerContainer.x = 0;
                position = settings.FOOTER_CONTAINER_MARGIN;
            }
        return position;
    }());
    footer.linesBetImage.y = (function () {
        let position = 0//footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.balanceImage.x = (function () {
        let position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = footer.linesBetImage.x + footer.balanceImage.width + settings.FOOTER_CONTAINER_MARGIN;
            }


        return position;
    }());

    footer.balanceImage.y = (function () {
        let position = 0//footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.totalBalanceImage.y = (function () {
        let position = 0//footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.totalBalanceImage.x = (function () {
        let position = 0//footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].x;
            } else {
                position = footer.balanceImage.x + footer.bonusImage.width + settings.FOOTER_CONTAINER_MARGIN;
            }

        return position;
    }());

    footer.bonusImage.x = (function () {
        let position = 0//footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = footer.balanceImage.x + footer.bonusImage.width + settings.FOOTER_CONTAINER_MARGIN;
            }

        return position;
    }());
    footer.bonusImage.y = (function () {
        let position = 0//footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.betImage.x = (function () {
        let position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = GAME_WIDTH - footer.betImage.width - settings.FOOTER_CONTAINER_MARGIN;
            }

        return position;
    }());
    footer.betImage.y = (function () {
        let position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.winImage.x = (function () {
        let position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                footerContainer.x = 0;
                position = GAME_WIDTH / 2 - footer.winImage.width / 2;
            }
        }

        return position;
    }());
    footer.winImage.y = (function () {
        let position = 0//footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());
}

function adjustFooterStyle() {
    footerContainer.getChildByName('win-image').getChildByName('win-text').style.fontSize = 30
    footerContainer.getChildByName('lines-bet-image').getChildByName('lines-bet-text').style.fontSize = 30
    footerContainer.getChildByName('balance-image').getChildByName('balance-text').style.fontSize = 30
    footerContainer.getChildByName('bonus-image').getChildByName('bonus-text').style.fontSize = 30
    footerContainer.getChildByName('bet-image').getChildByName('bet-text').style.fontSize = 30
}

