function switchTitleTexture() {
    let isMobile = !settings.DEVICE.includes('D');
    if (isMobile) {
        if (window.innerWidth > window.innerHeight) {
            oAL.titleImg.texture = oAL.backgroundElements['title']
        } else {
            oAL.titleImg.texture = oAL.backgroundElements['portrait_title']
        }

        /*setTimeout(() => {
            if (window.innerWidth > window.innerHeight) {
                titleContainer.x = GAME_WIDTH / 2 - titleContainer.width / 2
                oAL.titleImg.x = titleContainer.width / 2 - oAL.titleImg.width / 2
            } else {
                titleContainer.x = 0
                oAL.titleImg.x = GAME_WIDTH / 2 - oAL.titleImg.width / 2;
            }
        }, 100)*/
    }
}

function prepareLayoutForDevice() {
    let isMobile = !settings.DEVICE.includes('D');
    if (isMobile) {
        if (window.innerWidth > window.innerHeight) {
            oAL.reelDividerImg.visible = true;
            oAL.backgroundImg.visible = true;
        } else {
            oAL.reelDividerImg.visible = false;
            oAL.backgroundImg.visible = false;
        }
    }
}
