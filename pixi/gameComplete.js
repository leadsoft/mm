class gameComplete {
    constructor() {
        this.lines = [];
        this.loopPosition = 0;
        this.linesIndicators = [];
        this.containerLineIndicators = new PIXI.Container();
        this.containerLine = new PIXI.Container();

        this.paylinesNumber = 25;
        this.activePaylineIndicators = [];

        this.winText = new PIXI.Text('', {
            dropShadowAlpha: 0,
            dropShadowAngle: 3.1,
            dropShadowDistance: 8,
            fill: [
                "#0096ff",
                "#0432ff"
            ],
            fontFamily: "custom-f",
            fontSize: 60,
            fontWeight: "bold",
            miterLimit: 0,
            stroke: "#000500",
            strokeThickness: 3
        });

        this.test = '';
        this.showSymbols = '';
    }

    /*loadLines(resources) {
        this.containerLine.width = GAME_WIDTH;
        this.containerLine.height = GAME_HEIGHT;
        this.containerLine.visible = true;
        let sheet = resources['paylines'].spritesheet;
        for (let i = 1; i <= settings.START_GAME_PAYLINES; i++) {
            const line = new PIXI.Sprite(sheet.textures[i + ".png"])
            line.visible = false;
            line.name = i;
            this.lines.push(line);
            this.containerLine.addChild(line)
        }
        this.containerLine.name = 'line-container';

        this.containerLine.zIndex = 3;
        app.stage.addChild(this.containerLine);
    }*/

    loadLinesIndicators(resources) {
        this.containerLineIndicators.width = GAME_WIDTH;
        this.containerLineIndicators.height = GAME_HEIGHT;
        //this.containerLineIndicators.visible = false;
        let sheet = resources['indicators'].spritesheet;

        for (let i = 0; i <= settings.START_GAME_PAYLINES; i++) {
            const lineIndicators = new PIXI.Sprite(sheet.textures["indicators/lineInd_h_" + i.toString().padStart(2, '0') + ".png"]);
            lineIndicators.visible = false;

            this.linesIndicators.push(lineIndicators);
            this.containerLineIndicators.addChild(lineIndicators)
        }

        this.containerLineIndicators.name = 'line-indicators-container';
        this.containerLineIndicators.y = 18;
        this.containerLineIndicators.zIndex = 10
        gameContainer.addChild(this.containerLineIndicators);
    }

    async index() {
        this.loopPosition++;
        if (this.showSymbols !== '')
            this.showSymbols.destroy();
        this.showSymbols = new ShowSymbols();

        await this.showSymbols.showAllWinningSymbols(settings.CURRENT_SPIN_INDEX);

        if (game.paylines.length > 0 && !settings.GAME_RUNNING && !settings.STOP_PAYLINE_ANIMATION /*&& spinH.gameType.name !== 'free-spin'*/) {
            if (this.test !== "") {
                this.test.destroy();
            }
            this.test = new Test();
            this.test.showWinningSymbolsNew();
        } /*else if (spinH.gameType.name === 'free-spin'){
            spinH.winningRound = false;
            spinH.removeAllAnimatedSprite();
            gameCompleted.addRemoveBlurAllSymbols(false);
            spinH.activeSpinButton();
        }*/
    }

    hideActivePayline() {
        for (let i = 0; i < this.activePaylineIndicators.length; i++) {
            /*if (this.activePaylineIndicators[i] > 0) {
                this.linesIndicators[this.activePaylineIndicators[i]].texture = oAL.lineIndicators[this.activePaylineIndicators[i] + 'n'];
            }*/
            delete this.activePaylineIndicators[i];
        }

        this.activePaylineIndicators = [];
    }

    hideAllLines() {
        for (let i = 0; i < this.paylinesNumber; i++) {
            if (typeof this.lines[i] !== "undefined")
                this.lines[i].visible = false;
        }
    }

    hideAllLinesIndicators() {
        for (let i = 0; i <= this.paylinesNumber; i++) {
            if (typeof this.linesIndicators[i] !== "undefined")
                this.linesIndicators[i].visible = false;
        }
    }

    addRemoveBlurAllSymbols(add = true) {

        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 4; j++) {
                //game.reels[i].symbols[j].alpha = add ? 0.4 : 1;

                let filter = new PIXI.filters.ColorMatrixFilter();
                if (add) {
                    filter.greyscale(0.4, true);
                    game.reels[i].symbols[j].filters = [filter];
                } else {
                    game.reels[i].symbols[j].filters = [];
                }
            }
        }
    }
}


/*function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}*/

