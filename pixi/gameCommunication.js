/*var mandatoryParams = [/!*'authToken', *!//!*'cid', 'real',*!/ 'game', /!*'tournament', *!//!*'clientType'*!/];*/
var startGamePermission = false;
var responseVariable;

function startDemoMode() {
    demoLogin();
}

function startRealMode() {
    setSettings.GAME_PLAY_MODE = 'real';
    var queryParams = new URLSearchParams(window.location.search);
    history.replaceState(null, null, "?" + queryParams.toString());

    if (queryParams.get('authToken') !== null) {
        validate();
    } else {
        login()
    }
}

function startTournamentMode() {
    setSettings.GAME_PLAY_MODE = 'tournament';
    var queryParams = new URLSearchParams(window.location.search);
    history.replaceState(null, null, "?" + queryParams.toString());
    if (queryParams.get('authToken') !== null) {
        validateTournament();
    } else {
        loginTournament(queryParams.real);
    }
}

/*function validateGameLaunch(real) {
    var params = getUrlParameters();
    var validParams = true;

    if (real['real'] === 'demo') {
        return true
    } else {
        $.each(mandatoryParams, function (index, val) {
            if (!(val in params)) {
                validParams = false;
                return;
            }
        });
        return validParams;
    }
}

function getUrlParameters() {
    var params = window.location.search.split('&');
    var paramsArr = {};
    $.each(params, function (index, val) {
        paramsArr[val.split('=')[0].replace('?', '')] = val.split('=')[1];
    });
    return paramsArr;
}*/

function getAuthTokenOld() {
    var url = "http://206.223.191.28:8083/GSPWeb/account/Login?account=TT0000000040&password=ParrotParty&cid=TT&platform=F&key=&value=&ua=test&fp=1126c80f73aaaa927495ef7c4499f4c4&ts=1593462767825";
    $.get(url).then((response) => {
        authToken = response.authToken;
    });
}

function getAuthToken(ajaxurl) {
    return $.ajax({
        url: settings.BASE_URL + '/GSPWeb/account/Login?account=TT0000000040&password=ParrotParty&cid=TT&platform=F&key=&value=&ua=test&fp=1126c80f73aaaa927495ef7c4499f4c4&ts=1593462767825',
        type: 'GET',
    });
}

function launchGame(authToken) {
    var url = Communicator.getProxyUrl();
    url = url + '/game/szgamelaunch.html?authToken=' + authToken + '&cid=dd&Mode=demo&proxyUrl=' + Communicator.getProxyUrl() + '&Game-id=122&redirectUrl=https://www.google.com&Tournament=1&Language=en&Currency=USD&Platform=D';
    $.get(url).then((response) => {
        //console.log(response);
    });
}

function validateTournament() {
    /** check why server returns time out when validating real game **/
    var queryParams = getUrlParameters();
    var clientType = getClientType(queryParams);
    var version = '5.0';

    var xmlData = '<validate token="' + queryParams['authToken'] + '" clienttype="' + clientType + '" vers="' + version + '"/>';

    Communicator.sendRequest(xmlData, function (err, data) {
        if (typeof data !== "undefined" && typeof data.errorMessage !== "undefined") {
            modalDialog.show(data.errorMessage, 'refresh');
            startGamePermission = false;
        } else {
            startGamePermission = true;
            getTournamentList(); //TODO: need validation
            setTimeout(function () {
                let tournament = $(responseVariable).find('tournament');
                if (tournament.length > 0) {
                    setSettings.TOURNAMENT_START = tournament.attr('start');
                    setSettings.TOURNAMENT_END = tournament.attr('end');
                    setSettings.TOURNAMENT_NAME = tournament.attr('name');
                    setSettings.TOURNAMENT_REBUY_STAKE = parseFloat(tournament.attr('rebuystake')).toFixed(2);
                    setSettings.TOURNAMENT_REBUY_FEE = parseFloat(tournament.attr('rebuyfee')).toFixed(2);
                    setSettings.TOURNAMENT_CURRENCY = tournament.attr('currency');
                }
                setTimeout(function () {
                    startGame(queryParams.real);
                }, 500)
            }, 2000)
        }
    });
}

function loginTournament() {
    var queryParams = getUrlParameters();
    var xmlData = '<login\n' +
        'account="' + settings.GAME_USERNAME + '"\n' +
        'password="' + settings.GAME_PASSWORD + '"\n' +
        'casino="' + settings.CID + '"\n' +
        'realmoney="' + queryParams.real + '"\n' +
        'clienttype="' + getClientType(queryParams) + '"\n' +
        'vers="' + settings.GAME_VERSION + '"\n' +
        'machineid=""\n' +
        'ipaddress=""\n' +
        '/>';

    Communicator.sendRequest(xmlData, function (err, data) {
        if (typeof data !== "undefined" && typeof data.errorMessage !== "undefined") {
            modalDialog.show(data.errorMessage, 'refresh');
            startGamePermission = false;
        } else {
            startGamePermission = true;
            getTournamentList(); //TODO: need validation
            setTimeout(function () {
                let tournament = $(responseVariable).find('tournament');
                if (tournament.length > 0) {
                    setSettings.TOURNAMENT_START = tournament.attr('start');
                    setSettings.TOURNAMENT_END = tournament.attr('end');
                    setSettings.TOURNAMENT_NAME = tournament.attr('name');
                    setSettings.TOURNAMENT_REBUY_STAKE = parseFloat(tournament.attr('rebuystake')).toFixed(2);
                    setSettings.TOURNAMENT_REBUY_FEE = parseFloat(tournament.attr('rebuyfee')).toFixed(2);
                    setSettings.TOURNAMENT_CURRENCY = tournament.attr('currency');
                }
                setTimeout(function () {
                    startGame(queryParams.real);
                }, 1000)
            }, 1000)
        }
    });
}


function login() {
    var queryParams = getUrlParameters();
    var xmlData = '<login\n' +
        'account="' + settings.GAME_USERNAME + '"\n' +
        'password="' + settings.GAME_PASSWORD + '"\n' +
        'casino="' + settings.CID + '"\n' +
        'realmoney="' + settings.REAL_PARAMS + '"\n' +
        'clienttype="' + getClientType(queryParams) + '"\n' +
        'vers="' + settings.GAME_VERSION + '"\n' +
        'machineid=""\n' +
        'ipaddress=""\n' +
        '/>';
    Communicator.sendRequest(xmlData, function (err, data) {
        if (typeof data !== "undefined" && typeof data.errorMessage !== "undefined") {
            modalDialog.show(data.errorMessage, 'refresh');
            startGamePermission = false;
        } else {
            setTimeout(function () {
                startGame(queryParams.real);
            }, 1500);
        }
    })
    /*Communicator.sendRequest(xmlData);
    setTimeout(function () {
        if ($(responseVariable).find('errorCode').length > 0) {
            startGamePermission = false;
        } else {
            startGame(queryParams.real);
        }
    }, 1500);*/
}

function demoLogin() {
    var queryParams = getUrlParameters();
    var xmlData = '<login\n' +
        'account="demo"\n' +
        'password=""\n' +
        'casino="' + settings.CID + '"\n' +
        'realmoney="' + settings.REAL_PARAMS + '"\n' +
        'clienttype="' + getClientType(queryParams) + '"\n' +
        'vers="' + settings.GAME_VERSION + '"\n' +
        'machineid=""\n' +
        'ipaddress=""\n' +
        '/>';

    Communicator.sendRequest(xmlData, function (err, data) {
        if (typeof data !== "undefined" && typeof data.errorMessage !== "undefined") {
            modalDialog.show(data.errorMessage, 'refresh');
            startGamePermission = false;
        } else {
            setTimeout(function () {
                startGame(queryParams.real);
            }, 1500);
        }
    })
    /*Communicator.sendRequest(xmlData);

    setTimeout(function () {
        if ($(responseVariable).find('errorCode').length > 0) {
            startGamePermission = false;
        } else {
            startGame(queryParams.real);
        }
    }, 1500);*/
}

function getClientType(queryParams) {

    let type = queryParams.clientType;
    if (typeof type === "undefined")
        return '';

    if (type.length == 2)
        return type + 'M';


    return 'UUM';
}

function getParameter() {

}

function setResponseVariable(data) {
    responseVariable = data;
}

function getTournamentList() {
    var xmlData = '<gettournamentlist id="' + settings.TOURNAMENT_ID + '" />';
    Communicator.sendRequest(xmlData);
}

function validate() {
    /** check why server returns time out when validating real game **/
    var queryParams = getUrlParameters();
    var clientType = getClientType(queryParams);
    var version = '5.0';

    var xmlData = '<validate token="' + queryParams['authToken'] + '" clienttype="' + clientType + '" vers="' + version + '"/>';
    Communicator.sendRequest(xmlData, function (err, data) {
        if (typeof data !== "undefined" && typeof data.errorMessage !== "undefined") {
            modalDialog.show(data.errorMessage, 'refresh');
            startGamePermission = false;
        } else {
            startGamePermission = true;
            startGame(queryParams.real);
        }
    });

    /*setTimeout(function () {

    }, 2500);*/
}

function startGame(real) {
    var vers = '1';
    var tournament = real === 'true' ? settings.TOURNAMENT_ID : 0;
    GAME_CLASS = 'fivereelslots';
    var xmlData = `<startgame><game class="` + GAME_CLASS + `" vers="` + vers + `" tournament="` + tournament + `" instance="` + GAME_ID + `"></game></startgame>`;

    Communicator.sendRequest(xmlData, function (err, data) {
        if (typeof data !== "undefined" && typeof data.errorMessage !== "undefined") {
            modalDialog.show(data.errorMessage, 'refresh');
            startGamePermission = false;
        } else /*{
        if ($(responseVariable).find('error').length > 0) {
            startGamePermission = false;
        } else */{
            setTimeout(function () {
                prepareResponse();
                $(responseVariable).find('slotstatus').attr('chips');

                setSettings.FS_STATUS = 0;
                if ($(responseVariable).find('slotstatus freespin').length > 0) {
                    let freeSpin = $(responseVariable).find('slotstatus freespin');
                    setSettings.FS_COUNT = parseInt(freeSpin.attr('count')) || 0;
                    setSettings.FS_STATUS = parseInt(settings.FS_COUNT) > 0;
                    setSettings.FS_MULTIPLIER = freeSpin.attr('multiplier');
                    setSettings.FS_SUBSTITUTE_MULTIPLIER = freeSpin.attr('substituteMultiplier');
                    setSettings.FS_TRIGGER_REELS = freeSpin.attr('triggerReels');
                    setSettings.FS_TOTAL_WIN = freeSpin.attr('totalWin');
                    setSettings.FS_ORIGINAL_COUNT = freeSpin.attr('originalCount');
                    setSettings.FS_ORIGINAL_MULTIPLIER = freeSpin.attr('originalMultiplier');
                    setSettings.FS_SPIN_SPUN = freeSpin.attr('spinSpun');

                    setSettings.FS_START = true;
                }

                setSettings.USER_BALANCE = $(responseVariable).find('slotstatus').attr('chips');
                setSettings.RANK_USER_BALANCE = $(responseVariable).find('slotstatus').attr('rankbalance') || 0;
                setSettings.PROMO_CHIPS = $(responseVariable).find('slotstatus').attr('promochips');
                setSettings.GAME_MODE = $(responseVariable).find('game').find('type').attr('mode');
                if (settings.GAME_MODE === 'P') {
                    setSettings.FSV_COUPON_ID = $(responseVariable).find('game').find('type').attr('id');
                    let responseTypeStatus = $(responseVariable).find('game').find('type').find('status');
                    setSettings.FSV_TOTAL_WIN = responseTypeStatus.attr('totalwin') || 0;
                    setSettings.FSV_END = responseTypeStatus.attr('end');
                    if (parseInt(settings.FSV_END) === 0) {
                        setSettings.FSV_AWARDED = responseTypeStatus.attr('spinsawarded') || 0;
                        setSettings.FSV_MAXIMUM_WIN = responseTypeStatus.attr('maximumwin') || 0;
                        setSettings.FSV_MINIMUM_WIN = responseTypeStatus.attr('minimumwin') || 0;
                        setSettings.FSV_REMAINING = responseTypeStatus.attr('spinsremaining') || 0;
                        setSettings.FSV_STATUS = true;
                    }
                }
                setSettings.TOURNAMENT_ID = $(responseVariable).find('game').attr('tournament');
                setSettings.MIN_BET = parseFloat($(responseVariable).find('slotstatus').attr('coin'));
                setSettings.START_GAME_PAYLINES = $(responseVariable).find('slotstatus').attr('count');
                setSettings.START_GAME_BET = settings.MIN_BET * settings.START_GAME_PAYLINES;

                let tournament = $(responseVariable).find('tournament');

                if (tournament.length > 0) {
                    setSettings.TOURNAMENT_RANK_TYPE = tournament.attr('rankType');
                    setSettings.TOURNAMENT_TIME = tournament.attr('time');
                    setSettings.TOURNAMENT_RANK = tournament.attr('rank');
                    setSettings.TOURNAMENT_PLAY_COUNT = tournament.attr('playcount');
                    setSettings.TOURNAMENT_QUALIFY_NUM = tournament.attr('qualifynum');
                    setSettings.TOURNAMENT_REBUY = tournament.attr('rebuy');
                    setSettings.TOURNAMENT_ACCOUNT_CURRENCY = tournament.attr('accountcurrency');
                }

                startGamePermission = true;
                setSettings.REQUEST_END = true;

            }, 500);
        }
    });
}

async function slotOperation() {
    game.paylines = [];
    spinH.paylines = [];
    spinH.firstReel = [];
    return new Promise((resolve, reject) => {
        var vers = '1';
        var xmlData = `<slotoperation count="` + settings.START_GAME_PAYLINES + `" coin="` + settings.MIN_BET.toFixed(2) + `" credit="1"><game class="fivereelslots" vers="` + vers + `" tournament="` + settings.TOURNAMENT_ID + `" instance="` + GAME_ID + `"><type mode="` + settings.GAME_MODE + `" id="` + settings.FSV_COUPON_ID + `"></type></game></slotoperation>`;
        console.log(xmlData, 'xmlData');
        setSettings.LAST_BET_AMOUNT = settings.START_GAME_PAYLINES * settings.MIN_BET.toFixed(2);

        //Communicator.setCookie("LAST_BET_AMOUNT", setSettings.LAST_BET_AMOUNT, 100);
        reelsConfiguration = [];
        winPaylines = [];
        Communicator.sendRequest(xmlData, async function (err, success) {
            if (typeof success !== "undefined" && typeof success.errorMessage !== "undefined") {
                modalDialog.show(success.errorMessage, 'refresh');
                startGamePermission = false;
            } else if (success) {
                k = 0;
                if (setSettings.GAME_MODE === 'P') {
                    setSettings.FSV_COUPON_ID = $(responseVariable).find('game').find('type').attr('id');
                    let responseTypeStatus = $(responseVariable).find('game').find('type').find('status');

                    setSettings.FSV_TOTAL_WIN = responseTypeStatus.attr('totalwin');
                    setSettings.TOTAL_BONUS_WIN = 0;
                    setSettings.FSV_AWARDED = responseTypeStatus.attr('spinsawarded');
                    setSettings.FSV_MAXIMUM_WIN = responseTypeStatus.attr('maximumwin');
                    setSettings.FSV_MINIMUM_WIN = responseTypeStatus.attr('minimumwin');
                    setSettings.FSV_REMAINING = responseTypeStatus.attr('spinsremaining');
                    setSettings.FSV_END = responseTypeStatus.attr('end');
                    if (settings.FSV_END === 0)
                        setSettings.FSV_STATUS = true;
                }

                if ($(responseVariable).find('slotstatus freespin').length > 0) {
                    let freeSpin = $(responseVariable).find('slotstatus freespin');
                    setSettings.FS_COUNT = parseInt(freeSpin.attr('count')) || 0;
                    setSettings.FS_STATUS = parseInt(settings.FS_COUNT) > 0;
                    setSettings.FS_MULTIPLIER = freeSpin.attr('multiplier');
                    setSettings.FS_SUBSTITUTE_MULTIPLIER = freeSpin.attr('substituteMultiplier');
                    setSettings.FS_TRIGGER_REELS = freeSpin.attr('triggerReels');
                    setSettings.FS_TOTAL_WIN = freeSpin.attr('totalWin');
                    setSettings.FS_ORIGINAL_COUNT = freeSpin.attr('originalCount');
                    setSettings.FS_ORIGINAL_MULTIPLIER = freeSpin.attr('originalMultiplier');
                    setSettings.SPIN_SPUN = freeSpin.attr('spinSpun');

                    if ($(responseVariable).find('slotstatus features feature').length > 0) {
                        let features = $(responseVariable).find('slotstatus features feature')
                        setSettings.FS_FEATURES_STATUS = features.attr('avail');
                        setSettings.FS_FEATURES_NAME = features.attr('name');
                        setSettings.FS_FEATURES_WIN = features.attr('win');
                        setSettings.FS_FEATURES_RES = features.find('symbol').attr('res');
                    }
                } else {
                    setSettings.FS_STATUS = false;
                }

                if (settings.GAME_PLAY_MODE === 'tournament') {
                    setSettings.TOURNAMENT_TIME = $(responseVariable).find('tournament').attr('time');
                    setSettings.TOURNAMENT_RANK = $(responseVariable).find('tournament').attr('rank');
                    setSettings.TOURNAMENT_QUALIFY_NUM = $(responseVariable).find('tournament').attr('qualifynum');
                    setSettings.TOURNAMENT_PLAY_COUNT = $(responseVariable).find('tournament').attr('playcount');
                    setSettings.TOURNAMENT_REBUY = $(responseVariable).find('tournament').attr('rebuy');
                }

                let increment = 0;
                $.each($(responseVariable).find('reel'), function (index, value) {
                    if ($(value).parent().prop('tagName') != 'bonus') {
                        game.slotPosition[$(value).attr('no')] = {
                            1: $(value).attr('s1'),
                            2: $(value).attr('s2'),
                            3: $(value).attr('s3'),
                            0: 2
                        };

                        let symbolSource = settings.FS_ENABLE_STATUS ? freeS.slotTextures : game.slotTextures

                        if ($(responseVariable).find('slotstatus features feature').length > 0 && !["2", "3"].includes($(value).attr('no'))) {
                            spinH.freeSlotPosition[increment] = {
                                0: Math.floor(Math.random() * 10),
                                1: $(value).attr('s1'),
                                2: $(value).attr('s2'),
                                3: $(value).attr('s3'),
                                4: generateRandom(0, symbolSource.length),//Math.floor(Math.random() * symbolSource.length),
                                5: generateRandom(0, symbolSource.length),
                                6: generateRandom(0, symbolSource.length),
                                7: generateRandom(0, symbolSource.length),
                                8: generateRandom(0, symbolSource.length),
                                9: generateRandom(0, symbolSource.length),
                                10: generateRandom(0, symbolSource.length),
                                11: generateRandom(0, symbolSource.length),
                            };
                            increment++
                        }

                        spinH.slotPosition[$(value).attr('no')] = {
                            0: Math.floor(Math.random() * symbolSource.length),
                            1: $(value).attr('s1'),
                            2: $(value).attr('s2'),
                            3: $(value).attr('s3'),
                            4: generateRandom(0, symbolSource.length),
                            5: generateRandom(0, symbolSource.length),
                            6: generateRandom(0, symbolSource.length),
                            7: generateRandom(0, symbolSource.length),
                            8: generateRandom(0, symbolSource.length),
                            9: generateRandom(0, symbolSource.length),
                            10: generateRandom(0, symbolSource.length),
                            11: generateRandom(0, symbolSource.length),
                        };

                        function generateRandom(min, max) {
                            var num = Math.floor(Math.random() * (max - min + 1)) + min;
                            return ([9].includes(num)) ? generateRandom(min, max) : num;
                        }

                        if ($(value).attr('no') === '0')
                            spinH.firstReel.push($(value).attr('s1'),
                                $(value).attr('s2'),
                                $(value).attr('s3'))
                    }
                });

                if ($(responseVariable).find('win').length > 0) {
                    for (let j = 0; j < $(responseVariable).find('win').length; j++) {
                        let resp = $(responseVariable).find('win')[j];
                        var data = {};
                        data['payline'] = parseInt($(resp).attr('payline'));
                        data['combination'] = parseInt($(resp).attr('combination'));
                        data['amount'] = parseFloat($(resp).attr('amount'));
                        data['win_symbols'] = [];
                        var winSymbols = $(resp).attr('highlights').split(',');
                        for (var i = 0; i < winSymbols.length; i++) {
                            var row = parseInt(winSymbols[i] / 5) + 1;
                            var col = parseInt(winSymbols[i] % 5);

                            var winData = {};
                            winData['row'] = row;
                            winData['col'] = col;
                            winData['value'] = parseInt(game.slotPosition[col][row] === 0 ? 1 : game.slotPosition[col][row]);
                            data['win_symbols'].push(winData);
                        }
                        if (data['payline'] !== 0) {
                            game.paylines.push(data);
                            spinH.paylines.push(data);
                        }

                        if (j === $(responseVariable).find('win').length - 1)
                            resolve(1);
                    }
                } else {
                    resolve(1);
                }
            }
        });
    })
}

async function requestForCurrency() {
    return new Promise((resolve, reject) => {
        var xmlData = `<currencyoperation action="convert" from="` + user.currency.toUpperCase() + `" to="` + settings.TOURNAMENT_CURRENCY + `" amount="` + settings.TOURNAMENT_REBUY_STAKE + `"/>`;
        Communicator.sendRequest(xmlData, async function (err, success) {
            if (success) {
                resolve(1);
            }
        });
    })
}

async function requestForRebuy(callback) {
    var xmlData = `<entertourney rebuy="true" id="` + settings.TOURNAMENT_ID + `"/>`;
    Communicator.sendRequest(xmlData, async function (err, success) {
        setSettings.GAME_RUNNING = false;
        if (success)
            return callback(success);
        return callback(null, err);
    });
}

function requestStartGameAfterRebuy(callback) {
    var xmlData = `<startgame><game class="fivereelslots" vers="1" tournament="` + settings.TOURNAMENT_ID + `" instance="` + GAME_ID + `"/><sendback rebuyed="true"/></startgame>`;
    Communicator.sendRequest(xmlData, async function (err, success) {
        if (typeof success !== "undefined" && typeof success.errorMessage !== "undefined") {
            modalDialog.show(success.errorMessage, 'refresh');
            startGamePermission = false;
        } else if (success) {
            setTimeout(function () {

                prepareResponse();
                setSettings.USER_BALANCE = $(responseVariable).find('slotstatus').attr('chips');
                setSettings.RANK_USER_BALANCE = $(responseVariable).find('slotstatus').attr('rankbalance') || 0;
                setSettings.PROMO_CHIPS = $(responseVariable).find('slotstatus').attr('promochips');
                setSettings.GAME_MODE = $(responseVariable).find('game').find('type').attr('mode');
                setSettings.TOURNAMENT_ID = $(responseVariable).find('game').attr('tournament');

                setSettings.MIN_BET = parseFloat($(responseVariable).find('slotstatus').attr('coin'));
                setSettings.START_GAME_PAYLINES = $(responseVariable).find('slotstatus').attr('count');
                setSettings.START_GAME_BET = settings.MIN_BET * settings.START_GAME_PAYLINES;

                let tournament = $(responseVariable).find('tournament');

                if (tournament.length > 0) {
                    setSettings.TOURNAMENT_RANK_TYPE = tournament.attr('rankType');
                    setSettings.TOURNAMENT_TIME = tournament.attr('time');
                    setSettings.TOURNAMENT_RANK = tournament.attr('rank');
                    setSettings.TOURNAMENT_PLAY_COUNT = tournament.attr('playcount');
                    setSettings.TOURNAMENT_QUALIFY_NUM = tournament.attr('qualifynum');
                    setSettings.TOURNAMENT_REBUY = tournament.attr('rebuy');
                    setSettings.TOURNAMENT_ACCOUNT_CURRENCY = tournament.attr('accountcurrency');
                }
            }, 1000);
            return callback(success);
        }

        return callback(null, err);
    });
}

function prepareResponse() {

    $.each($(responseVariable).find('reel'), function (index, value) {
        if ($(value).parent().prop('tagName') != 'bonus') {
            game.slotPosition[$(value).attr('no')] = {
                1: $(value).attr('s1'),
                2: $(value).attr('s2'),
                3: $(value).attr('s3'),
                0: 2
            };
            spinH.slotPosition[$(value).attr('no')] = {
                1: $(value).attr('s1'),
                2: $(value).attr('s2'),
                3: $(value).attr('s3'),
                0: 2
            };
        }
    });

    if ($(responseVariable).find('win').length > 0) {
        for (let j = 0; j < $(responseVariable).find('win').length; j++) {
            let resp = $(responseVariable).find('win')[j];
            var data = {};
            data['payline'] = parseInt($(resp).attr('payline'));
            data['combination'] = parseInt($(resp).attr('combination'));
            data['amount'] = parseFloat($(resp).attr('amount'));
            data['win_symbols'] = [];
            var winSymbols = $(resp).attr('highlights').split(',');

            for (var i = 0; i < winSymbols.length; i++) {

                var row = parseInt(winSymbols[i] / 5) + 1;
                var col = parseInt(winSymbols[i] % 5);

                var winData = {};
                winData['row'] = row;
                winData['col'] = col;
                winData['value'] = parseInt(game.slotPosition[col][row] === 0 ? 1 : game.slotPosition[col][row]);
                data['win_symbols'].push(winData);

            }
        }
    }
}

