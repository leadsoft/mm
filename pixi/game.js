class Game {
    constructor(app, gameContainer) {
        this.name = 'game';
        this.app = app;
        this.coinSequienceAnimation;
        this.tweenCoinAnim;
        this.gameContainer = gameContainer;
        this.slotPosition = [];
        this.animations = [];
        this.paylines = [];
        this.anticipation = [];
        this.lines = [];
        this.tweening = [];
        this.reels = [];
        this.slotTextures = [];
        this.win = 0;
        this.reelContainer = new PIXI.Container();
        this.reelBackground = new PIXI.Sprite();
        this.reelOverlay = new PIXI.Sprite(PIXI.Texture.WHITE);
        this.reelOverlay.name = 'reel-overlay';
        this.reelOverlay.tint = 0x000000;
        this.symbols = [];
        this.reelContainer.name = 'reel-container';
        this.fakeReelContainer = new PIXI.Container();
        this.fakeReelContainer.name = 'fake-reel-container';

        this.reel1ScatterActive = false;
        this.reel2ScatterActive = false;
        this.randomElement = {0: true, 1: true, 2: true, 3: true, 4: true};
        this.app.ticker.add((delta) => {
            let now = new Date();

            const remove = [];
            for (let i = 0; i < this.tweening.length; i++) {
                const t = this.tweening[i];

                const phase = Math.min(1, (now - t.start) / t.time);

                t.propertyBeginValue = 0; //reset value
                t.target = 180; //reset value

                t.object[t.property] = spinH.lerp(t.propertyBeginValue, t.target, t.easing(phase));

                if (t.change)
                    t.change(t);

                if (this.reel1ScatterActive && this.reel2ScatterActive && parseInt(t.object.reelPosition) === 4 && !settings.RESET_SPIN_VALUE) {
                    //now = new Date(new Date - 6000);
                    t.start = Date.now(Date.now() - 2000);
                    t.target = 60; //reset value
                    setSettings.RESET_SPIN_VALUE = true;
                }

                if (phase > 0.6)
                    this.randomElement[parseInt(t.object.reelPosition)] = false;

                if (phase === 1) {
                    if (t.object.scatterSymbol) {
                        if (parseInt(t.object.reelPosition) === 0) { //reel 0 is reel 1
                            footer.winAmount.text = '';

                            this.reel1ScatterActive = true;
                            scatterDrop1.play();
                        } else if (parseInt(t.object.reelPosition) === 2 && this.reel1ScatterActive) { //reel 2 is reel 3
                            scatterDrop2.play();
                            setSettings.ANTICIPATION = true;
                            this.reel2ScatterActive = true;

                            let anticipationSequence = game.anticipation[game.anticipation.length - 1];
                            anticipationSequence.visible = true;
                            anticipationSequence.gotoAndPlay(0)
                        } else if (parseInt(t.object.reelPosition) === 4 && this.reel1ScatterActive && this.reel2ScatterActive) {
                            scatterDrop3.play();
                            setSettings.ANTICIPATION = false;
                        }
                    } else {
                        if (parseInt(t.object.reelPosition) === 1) {
                            footer.winAmount.text = '';
                        }
                    }

                    /*FS STATUS ACTIVE*/
                    if (settings.FS_STATUS && parseInt(t.object.reelPosition) === 0 && !settings.RESET_SPIN_VALUE) {
                        this.tweening[this.tweening.length - 1].time += 4000;
                        setSettings.RESET_SPIN_VALUE = true;
                    }
                    t.object.scatterSymbol = false;
                    reelStop.play();
                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                } else if (settings.STOP_REEL) {
                    for(let l = 0; l < Object.keys(game.randomElement).length; l++){
                        this.randomElement[l] = false;
                    }

                    setSettings.ANTICIPATION = false;
                    reelStop.play();
                    footer.winAmount.text = '';
                    t.object.scatterSymbol = false;
                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                }
            }

            for (let i = 0; i < remove.length; i++)
                this.tweening.splice(this.tweening.indexOf(remove[i]), 1);

        });
        this.app.ticker.add((delta) => {
            // Update the slots.
            for (let i = 0; i < this.reels.length; i++) {
                const r = this.reels[i];
                // Update blur filter y amount based on speed.
                // This would be better if calculated with time in mind also. Now blur depends on frame rate.
                r.blur.blurY = (r.position - r.previousPosition) * 40;
                r.previousPosition = r.position;

                // Update symbol positions on reel.
                for (let j = 0; j < r.symbols.length; j++) {

                    const s = r.symbols[j];

                    const prevy = s.y;


                    s.y = /*j * SYMBOL_SIZE + (j * SYMBOL_MARGIN_TOP)*/((r.position + j) % r.symbols.length) * SYMBOL_CONTAINER_SIZE - SYMBOL_CONTAINER_SIZE/* + (j * SYMBOL_MARGIN_TOP)*/;
                    /*s.y = ((r.position + j) % r.symbols.length) * SYMBOL_SIZE - SYMBOL_SIZE;*/
                    if ((s.y < 0 && prevy > SYMBOL_SIZE) || Object.keys(spinH.slotPosition).length > 0) {
                        let symbolSource = settings.FS_ENABLE_STATUS ? freeS.slotTextures : this.slotTextures;
                        if (typeof spinH.slotPosition[i] !== "undefined") {
                            if (parseInt(spinH.slotPosition[i][j]) === 9) {
                                r.scatterSymbol = true;
                            }
                            r.reelPosition = i;

                            if(!game.randomElement[i])
                                console.log(game.randomElement[i], 'random');
                            //s.texture = !game.randomElement[i] ? symbolSource[this.slotPosition[i][j]] : symbolSource[Math.floor(Math.random() * symbolSource.length)];
                            s.texture = symbolSource[spinH.slotPosition[i][j]];

                            /*let tezt = new PIXI.Text(j, {font: "Arial", fill: "0xffe000"})
                            tezt.style.fontSize = 18;
                            s.addChild(tezt);*/
                            s.symbolElement = spinH.slotPosition[i][j];
                            //s.x = Math.round((SYMBOL_WIDTH - s.width) / 2);
                            //if(!game.randomElement[i]){
                                delete spinH.slotPosition[i][j];
                                if (Object.keys(spinH.slotPosition[i]).length === 0)
                                    delete spinH.slotPosition[i];
                            //}
                        }
                    }
                }
            }
        });
    }
}

