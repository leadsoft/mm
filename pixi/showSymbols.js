class ShowSymbols {
    constructor() {
        this.lines = [];
        this.stop = false;
        this.frameAnimPosition = {};
    }

    destroy() {
        this.stop = true;
    }

    async buildFakeMap() {
        return new Promise(async function (resolve) {
            let winPayline = game.paylines;
            let winSymbolValue = [];
            let totalMargin = settings.REEL_TOTAL_MARGIN;
            let symbolValue;
            let symbolElement;

            for (let i = 0; i < winPayline.length; i++) {
                let paylineContainer = new PIXI.Container();
                paylineContainer.name = winPayline[i].payline;

                let bigSymbol = false;
                for (let j = 0; j < winPayline[i].win_symbols.length; j++) {
                    let symbol = game.reels[winPayline[i].win_symbols[j].col].symbols[winPayline[i].win_symbols[j].row];
                    if (spinH.gameType.name === 'free-spin' && [1, 2, 3].includes(winPayline[i].win_symbols[j].col)) {
                        if (!bigSymbol) {
                            /*TODO: build big symbol functionality*/

                            symbolValue = settings.FS_FEATURES_RES;
                            winSymbolValue.push(symbolValue);
                            symbolElement = freeS.bigSymbolHighlighted[symbolValue];
                            let symbolContainer = new PIXI.Container();
                            symbolContainer.sortableChildren = true;
                            symbolContainer.name = 'big';
                            symbolContainer.bigName = 'big' + j + '-' + symbolValue;

                            /*let bSelementMargin = 0;
                            if (symbol.column > 0)
                                bSelementMargin = symbol.column * (totalMargin / (settings.REEL_NUMBER - 1));*/

                            symbolContainer.x = SYMBOL_CONTAINER_WIDTH//game.reelContainer.width / 2 - BIG_SYMBOL_WIDTH / 2;
                            symbolContainer.bigSymbolContainer = true;
                            paylineContainer.addChild(symbolContainer);

                            game.fakeReelContainer.addChild(paylineContainer);

                            bigSymbol = true;
                        }
                    }

                    symbolValue = winPayline[i].win_symbols[j].value;
                    winSymbolValue.push(symbolValue);

                    symbolElement = game.symbolAnimation[symbolValue];

                    let symbolContainer = new PIXI.Container();
                    symbolContainer.sortableChildren = true;
                    symbolContainer.name = j + '-' + symbolValue;

                    /*let elementMargin = 0;
                    if (symbol.column > 0)
                        elementMargin = symbol.column * (totalMargin / (settings.REEL_NUMBER - 1));*/

                    symbolContainer.x = SYMBOL_CONTAINER_WIDTH * symbol.column//symbol.width * symbol.column + elementMargin;
                    /*symbolContainer.y = symbol.height * (symbol.row - 1) + (symbol.row * SYMBOL_MARGIN_TOP);*/
                    symbolContainer.y = SYMBOL_CONTAINER_SIZE * (symbol.row - 1)/* + (symbol.row * SYMBOL_MARGIN_TOP)*/;

                    paylineContainer.addChild(symbolContainer);

                    game.fakeReelContainer.addChild(paylineContainer);

                    if (i === winPayline.length - 1 && j === winPayline[i].win_symbols.length - 1)
                        return resolve();
                }
            }
        })
    }

    async showAllWinningSymbols(spinIndex = 0) {
        setSettings.STATUS_SENT = false;
        setSettings.INITIAL_WIN = true;
        let winSound = 'minorWin';
        let self = this;
        self.frameAnimPosition = {};
        let winPayline = [];
        let winSymbolValue = [];
        let totalMargin = settings.REEL_TOTAL_MARGIN;
        await this.buildFakeMap();
        //await sleep(10000);
        return new Promise(async function (resolve) {
            winPayline = game.paylines;

            let symbolValue;
            let symbolElement;
            gameCompleted.addRemoveBlurAllSymbols(true);
            game.fakeReelContainer.visible = true;

            for (let payline = 0; payline < game.paylines.length; payline++) {
                let bigSymbolSelected = false;
                for (let j = 0; j < game.paylines[payline].win_symbols.length; j++) {

                    let fakeContainer = game.fakeReelContainer.getChildByName(game.paylines[payline].payline);

                    symbolValue = game.paylines[payline].win_symbols[j].value;
                    winSymbolValue.push(symbolValue);
                    let fakeSymbol = fakeContainer.getChildByName(j + '-' + symbolValue);
                    //fakeSymbol.x = SYMBOL_CONTAINER_WIDTH / 2 - SYMBOL_WIDTH  / 2
                    fakeSymbol.visible = true;
                    /*if (symbolValue !== 0 && symbolValue !== 11) {*/
                    let fakeSymbolFrame = /*settings.FS_ENABLE_STATUS ? freeS.animations :*/ game.animations;
                    let symbolFrameAnim = new PIXI.AnimatedSprite(fakeSymbolFrame['frame_symbol_flashing'])

                    fakeSymbol.addChild(symbolFrameAnim);

                    symbolFrameAnim.animationSpeed = settings.SYMBOL_FRAME_SPEED;
                    symbolFrameAnim.zIndex = 1;
                    symbolFrameAnim.name = 'frame-animation';
                    symbolFrameAnim.x = (SYMBOL_CONTAINER_WIDTH - symbolFrameAnim.width) / 2;
                    symbolFrameAnim.y = (SYMBOL_SIZE - symbolFrameAnim.height) / 2;

                    symbolFrameAnim.loop = true;
                    symbolFrameAnim.play();
                    if(self.frameAnimPosition[fakeSymbol.name] !== undefined)
                        symbolFrameAnim.visible = false;

                    self.frameAnimPosition[fakeSymbol.name] = true;
                    /*}*/

                    symbolElement = game.symbolAnimation[symbolValue];
                    let symbolAnim = new PIXI.AnimatedSprite(symbolElement);

                    symbolAnim.name = 'symbolAnim-' + symbolValue;
                    symbolAnim.zIndex = 2;
                    symbolAnim.x = SYMBOL_CONTAINER_WIDTH / 2 - SYMBOL_WIDTH  / 2;
                    symbolAnim.loop = false;
                    fakeSymbol.addChild(symbolAnim);
                    symbolAnim.animationSpeed = 0.300;
                    symbolAnim.play();

                    symbolAnim.y = -(symbolAnim.height - SYMBOL_SIZE);

                    if([0, 9].includes(symbolValue)){
                        let symbolAnimHighlighted = new PIXI.AnimatedSprite(game.symbolAnimation[symbolValue + '-h']);
                        symbolAnimHighlighted.name = 'symbolAnim-' + symbolValue + '-h';
                        symbolAnimHighlighted.zIndex = 2;

                        symbolAnimHighlighted.loop = false;
                        symbolAnimHighlighted.visible = false;
                        symbolAnimHighlighted.x = SYMBOL_CONTAINER_WIDTH / 2 - SYMBOL_WIDTH  / 2;
                        fakeSymbol.addChild(symbolAnimHighlighted);
                        symbolAnimHighlighted.animationSpeed = 0.300;
                        symbolAnimHighlighted.y = -(symbolAnimHighlighted.height - SYMBOL_SIZE);
                    }

                    if (spinH.gameType.name === 'free-spin' && [1, 2, 3].includes(game.paylines[payline].win_symbols[j].col)) {
                        fakeSymbol.visible = false;
                        if (!bigSymbolSelected) {
                            symbolValue = settings.FS_FEATURES_RES;
                            winSymbolValue.push(symbolValue);

                            let bigSymbol = new PIXI.Sprite(freeS.bigSymbolHighlighted[symbolValue]);
                            let fakeBigSymbol = fakeContainer.getChildByName('big');
                            bigSymbol.name = 'big-symbol';
                            bigSymbolSelected = true;
                            fakeBigSymbol.addChild(bigSymbol);
                            bigSymbol.y = 15;
                            bigSymbol.x = 0//game.reelContainer.width / 2 - bigSymbol.width / 2;

                            let fakeBigSymbolFrame = freeS.animations;
                            let symbolFrameAnim = new PIXI.AnimatedSprite(fakeBigSymbolFrame['frame_big_symbol_flashing']);
                            fakeBigSymbol.addChild(symbolFrameAnim);
                            console.log(fakeBigSymbol, 'fakeBigSymbol');
                            symbolFrameAnim.animationSpeed = settings.SYMBOL_FRAME_SPEED;
                            symbolFrameAnim.zIndex = 1;

                            symbolFrameAnim.x = (bigSymbol.width - symbolFrameAnim.width) / 2;
                            symbolFrameAnim.y = (bigSymbol.height - symbolFrameAnim.height) / 2;


                            symbolFrameAnim.name = 'big-frame-animation';

                            symbolFrameAnim.loop = true;
                            symbolFrameAnim.play();
                            if(self.frameAnimPosition[fakeBigSymbol.name] !== undefined)
                                symbolFrameAnim.visible = false;

                            self.frameAnimPosition[fakeBigSymbol.name] = true;
                        }
                    }
                }

                if (typeof gameCompleted.linesIndicators[winPayline[payline].payline] !== "undefined" && !settings.STOP_PAYLINE_ANIMATION) {
                    gameCompleted.linesIndicators[winPayline[payline].payline].visible = true;
                }
            }

            /*if(spinH.gameType.name === 'free-spin')
                await sleep(1000000);*/

            if (typeof winSymbolValue.find(element => element === 0) !== "undefined" && !winSymbolValue.find(element => element === 11)) {
                winSound = 'majorWildWin';
            } else if (winSymbolValue.some(r => [1, 2, 3, 4].indexOf(r) >= 0) && !winSymbolValue.some(r => [0, 11].indexOf(r) >= 0)) {
                winSound = 'majorWin';
            }

            if (settings.LAST_WIN_AMOUNT.toFixed(2) > 0) {
                gameCompleted.winText.text = user.currencySymbol + settings.LAST_WIN_AMOUNT.toFixed(2);
                gameCompleted.winText.x = (GAME_WIDTH / 2) - (gameCompleted.winText.width / 2);
                gameCompleted.winText.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - (gameCompleted.winText.height / 2));
                //oAL.winLine.visible = true;
            }

            if (!gameCompleted.winTextAdded) {
                gameCompleted.winTextAdded = true;
                gameCompleted.winText.name = 'win-text';

                gameCompleted.winText.zIndex = 5;
                gameContainer.addChild(gameCompleted.winText);
            }

            if (settings.GAME_RUNNING)
                return;
            if (settings.LAST_WIN_AMOUNT.toFixed(2) > 0) {
                let wSound = eval(winSound).play();
                setTimeout(() => {
                    oAL.tick.startTick(settings.LAST_WIN_AMOUNT.toFixed(2), null, footer.winAmount, 'normal-win', false);
                }, 500)

                wSound.on('end', async function () {
                    if (settings.GAME_RUNNING)
                        return;
                    setSettings.TICKER_SOUND_STATUS = true;

                    if (spinIndex !== settings.CURRENT_SPIN_INDEX)
                        return;

                    /*oAL.tick.startTick(settings.LAST_WIN_AMOUNT.toFixed(2), null, footer.winAmount, 'normal-win', false);*/
                    if (!settings.GAME_RUNNING && !settings.FS_STATUS)
                        footer.showHideSpinButton(true)

                    //await checkTickerStatus();
                    if (self.stop) return;

                    await sleep(1500)
                    await waitUntilIsDone('TICK_STATUS');
                    if (!settings.STATUS_SENT) {
                        setSettings.STATUS_SENT = true;

                        gameCompleted.addRemoveBlurAllSymbols(false);
                        gameCompleted.hideActivePayline();
                        gameCompleted.hideAllLines();
                        gameCompleted.hideAllLinesIndicators();

                        if (!settings.FS_STATUS && settings.FS_COUNT === 0)
                            setSettings.FS_STOP = true;
                        else if (settings.FS_STATUS && !settings.FS_START)
                            setSettings.FS_START = true;

                        return resolve(1);
                    }
                });
            } else {
                await sleep(1500)
                if (!settings.STATUS_SENT) {
                    setSettings.STATUS_SENT = true;

                    gameCompleted.addRemoveBlurAllSymbols(false);
                    gameCompleted.hideActivePayline();
                    gameCompleted.hideAllLines();
                    gameCompleted.hideAllLinesIndicators();

                    if (!settings.FS_STATUS && settings.FS_COUNT === 0)
                        setSettings.FS_STOP = true;
                    else if (settings.FS_STATUS && !settings.FS_START)
                        setSettings.FS_START = true;

                    return resolve(1);
                }
            }
        })
    }
}

/*function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}*/

