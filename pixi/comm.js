// communicator

var Communicator = new function () {
    let playType = settings.TEST_MODE || settings.TEST_MODE !== false ? 'WebProxyTest' : 'WebProxy';
    var proxyUrl; //global proxy address
    this.sendRequest = function (data, callback = false) {
        this.secureToken = this.getCookie('mSessionId');
        if (typeof ig !== 'undefined' && ig.trace)
            console.log(ig.currentTime() + "Communicator: sendRequest: Retrieved securityToken " + this.secureToken);

        var paramString = "xmlRequest=" + encodeURIComponent(data) + "&securetoken=" + (!this.secureToken ? '' : this.secureToken) + '&t=' +
            new Date().getMilliseconds();

        if (!proxyUrl) {
            proxyUrl = this.getProxyUrl();
            var pattern = /^((http|https|ftp):\/\/)/;

            if(!pattern.test(proxyUrl)) {
                proxyUrl = "http://" + proxyUrl;
            }

            /*proxyUrl = proxyUrl;*/
            //console.log("Communicator: proxyUrl: " + proxyUrl);
        }

        var jqXhr = jQuery.ajax({
            url: proxyUrl + '/' + playType + ';sessiontoken=' + this.secureToken + '?callback=?',
            type: 'POST',
            dataType: 'json',
            data: paramString,
            timeout: 30000
        }).done(function (data) {
            if (data.securityToken) {
                Communicator.setCookie("mSessionId", data.securityToken, 1);
                /*console.log('security Token:' + data.securityToken);
                console.log(data, 'data');*/
            }
            Communicator.handleResponse(data);
            if (callback)
                return callback(null, data);


        }).fail(function (xhr, status) {
            if (typeof ig !== 'undefined' && ig.trace)
                console.log(ig.currentTime() + "Communicator: error: status = " + status);
            Communicator.onResponse($.parseXML('<error lobby="true" text="Network error: ' + status + '" \/>'));
            /*console.log('data.xmlResponse', xhr)*/
            if (callback)
                return callback(xhr);
        }).always(function (data) {
            if (data.xmlResponse === '' && typeof data['errorMessage'] !== "undefined"){
                if(callback){
                    if (data['errorCode']){
                        return callback(null, {errorMessage: typeof messages[data['errorCode']] !== "undefined" ? messages[data['errorCode']] : 'There was an error processing the request'});
                    } else
                        return callback(null, {errorMessage: 'There was an error processing the request'});
                } else {
                    if (data['errorCode']){
                        modalDialog.show(typeof messages[data['errorCode']] !== "undefined" ? messages[data['errorCode']] : 'There was an error processing the request');
                    } else
                        modalDialog.show('There was an error processing the request');
                }

            }
        });
    };

    this.handleResponse = function (data) {
        //take the json object and construct whatever is necessary
        //to pass back to the calling function
        //alert("Response: " + data.xmlResponse);
        if (data.xmlResponse === undefined) {
            return;
        }

        var xmlString = decodeURIComponent(data.xmlResponse);
        if (typeof ig !== 'undefined' && ig.trace)
            console.log(ig.currentTime() + "Communicator: received: " + xmlString);
        Communicator.onResponse($.parseXML(xmlString));
    };

    this.onResponse = function (data) {
        // to be bind

        setResponseVariable(data);
    };


    this.loadVersion = function (versionUrl) {
        $.getJSON(versionUrl, function (data) {
            Communicator.onVersion(data);
        });
    };

    this.onVersion = function (data) {
        // to be bind by message.js
    };

    this.validateRequest = function (data) {
        this.secureToken = null;
        if (typeof ig !== 'undefined' && ig.trace)
            console.log("Communicator: validate request");
        var paramString = "xmlRequest=" + data + "&securetoken=" + (!this.secureToken ? '' : this.secureToken) + '&t=' + (new Date()).getMilliseconds();
        if (!proxyUrl) {
            proxyUrl = this.getProxyUrl();
            proxyUrl = 'http://' + proxyUrl;
            if (typeof ig !== 'undefined' && ig.trace)
                console.log("Communicator: proxyUrl: " + proxyUrl);
        }
        var jqXhr = jQuery.ajax({
            url: proxyUrl + '/' + playType + ';sessiontoken=' + this.secureToken + '?callback=?',
            type: 'POST',
            dataType: 'json',
            data: paramString,
            timeout: 30000
        }).done(function (data) {
            if (data.securityToken) {
                Communicator.setCookie("mSessionId", data.securityToken, 1);
                Communicator.onValidateDone();
            } else {
                Communicator.onValidateFail();
            }
        }).fail(function (xhr, status) {
            if (typeof ig !== 'undefined' && ig.trace)
                console.log("Communicator: error: status = " + status);
            Communicator.onValidateFail();
        }).always(function () {
            //console.log( "complete" );
        });
    };

    this.onValidateDone = function () {
        // to be bind by message.js
    };

    this.onValidateFail = function () {
        Communicator.onResponse($.parseXML('<error lobby="true" text="Session Timeout/Invalid" \/>'));
    };

    this.setCookie = function (c_name, value, exdays) {
        if (typeof ig !== 'undefined' && ig.trace)
            console.log("Communicator: saving cookie: " + c_name);
        $.cookie(c_name, value, {path: "/", expires: exdays});
        $('#mSessionId').val(value);
    };

    this.getCookie = function (c_name) {
        if (typeof ig !== 'undefined' && ig.trace)
            console.log("Communicator: loading from cookie: " + c_name);
        var c_value = $.cookie(c_name);
        if (!c_value) {
            if (typeof ig !== 'undefined' && ig.trace)
                console.log("Communicator: loading from localstorage: " + c_name);
            c_value = $('#mSessionId').val();
        }
        return c_value;
    };

    this.removeCookie = function (c_name) {
        if (typeof ig !== 'undefined' && ig.trace)
            console.log("Communicator: removing cookie: " + c_name);
        $.removeCookie(c_name);
    };

    // change url to point to different server instances
    this.getProxyUrl = function () {
        /** staging server **/
        //var url = 'http://10.70.4.63:10090';

        /** michael's private server**/
        // var url = 'http://10.70.0.93:10090';

        /** provided server **/
        var url = settings.PROXY_URL;

        if (!window.location.search)
            return url;

        var query = window.location.search.substring(1);
        var pairs = query.split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split("=");
            if (typeof pair[0] === "undefined") {
                continue;
            } else if (typeof pair[0] === "string" && pair[0] === 'proxyUrl') {
                return decodeURIComponent(pair[1]);
            }
        }
        return url;
    }
};
