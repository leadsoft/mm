class Info {
    constructor(app) {
        this.infoSprites = [];
        this.mainContainer = new PIXI.Container();
        this.mainContainer.name = 'info-container';

        this.pageIndex = 1;

        app.stage.addChild(this.mainContainer);
    }

    init() {
        console.log('info-ajunge');
        this.mainContainer.y = -app.stage.height;
        this.mainContainer.alpha = 0;
        this.mainContainer.zIndex = 9999;
        this.mainContainer.sortChildren = true;
        this.mainContainer.name = 'main-info'

        /*let bg = new PIXI.Sprite(this.infoSprites.textures["close.png"]);
        bg.x = 0;
        bg.y = 0;
        bg.width = GAME;
        bg.height = app.stage.height;
        bg.name = 'bg-info'*/


        this.closeBtn = new PIXI.Sprite(this.infoSprites.textures["buttons and page indicators/close_n.png"]);
        this.closeBtn.name = 'close-info-btn';
        this.closeBtn.interactive = true;
        this.closeBtn.buttonMode = true;
        this.closeBtn.on('pointerdown', this.hideInfo.bind(this));
        this.panel = new PIXI.Sprite(this.infoSprites.textures['ipPage1.png']);
        this.controlContainer = new PIXI.Container();
        this.rightArrow = new PIXI.Sprite(this.infoSprites.textures['buttons and page indicators/R_arrow_n.png']);
        this.leftArrow = new PIXI.Sprite(this.infoSprites.textures['buttons and page indicators/L_arrow_d.png']);
        this.bulletPage = new PIXI.Sprite(this.infoSprites.textures['buttons and page indicators/page1_h.png']);

        this.controlContainer.addChild(this.rightArrow, this.bulletPage, this.leftArrow);

        this.closeBtn.zIndex = 9;
        //bg.zIndex = 2
        this.panel.zIndex = 3;
        this.controlContainer.zIndex = 4;
        this.mainContainer.addChild(/*bg, */this.panel, this.controlContainer, this.closeBtn);

        this.closeBtn.width = 24;
        this.closeBtn.height = 24;
        this.closeBtn.x = GAME_WIDTH - this.closeBtn.width - 20;
        this.closeBtn.y = 20;
        this.panel.x = (GAME_WIDTH / 2) - (this.panel.width / 2);
        this.panel.y = (GAME_HEIGHT / 2) - (this.panel.height / 2);

        this.controlContainer.x = (GAME_WIDTH / 2) - (this.controlContainer.width / 2);
        this.controlContainer.y = this.panel.height - this.controlContainer.height;

        this.bulletPage.x = (this.controlContainer.width / 2) - (this.bulletPage.width / 2);
        this.rightArrow.x = (this.controlContainer.width / 2) + (this.bulletPage.width / 2);
        this.leftArrow.x = (this.controlContainer.width / 2) - (this.bulletPage.width / 2) - (this.leftArrow.width) - 11;

        this.controlPanel();
    }

    controlPanel() {
        this.rightArrow.interactive = true;
        this.leftArrow.interactive = true;
        let self = this;
        /*RIGHT*/
        this.rightArrow.on('pointerover', function () {
            if (self.pageIndex < 4)
                self.rightArrow.texture = self.infoSprites.textures['buttons and page indicators/R_arrow_h.png']
        });

        this.rightArrow.on('pointerdown', function () {
            if (self.pageIndex >= 4)
                return;
            button.play()
            self.pageIndex += 1;
            self.bulletPage.texture = self.infoSprites.textures['buttons and page indicators/page' + self.pageIndex + '_h.png'];
            self.panel.texture = self.infoSprites.textures['ipPage' + self.pageIndex + '.png'];
            if (self.pageIndex > 1)
                self.leftArrow.texture = self.infoSprites.textures['buttons and page indicators/L_arrow_n.png'];

            if (self.pageIndex > 3)
                self.rightArrow.texture = self.infoSprites.textures['buttons and page indicators/R_arrow_d.png'];

        });

        this.rightArrow.on('pointerout', function () {
            if (self.pageIndex < 4)
                self.rightArrow.texture = self.infoSprites.textures['buttons and page indicators/R_arrow_n.png']
            else
                self.rightArrow.texture = self.infoSprites.textures['buttons and page indicators/R_arrow_d.png']
        });

        /*LEFT*/
        this.leftArrow.on('pointerover', function () {
            //console.log(self.pageIndex, self.pageIndex > 1);
            if (self.pageIndex > 1)
                self.leftArrow.texture = self.infoSprites.textures['buttons and page indicators/L_arrow_h.png']
        });

        this.leftArrow.on('pointerdown', function () {
            if (self.pageIndex <= 1)
                return;
            button.play()
            self.pageIndex -= 1;
            self.bulletPage.texture = self.infoSprites.textures['buttons and page indicators/page' + self.pageIndex + '_h.png'];
            self.panel.texture = self.infoSprites.textures['ipPage' + self.pageIndex + '.png'];
            if (self.pageIndex < 3)
                self.rightArrow.texture = self.infoSprites.textures['buttons and page indicators/R_arrow_n.png'];

            if (self.pageIndex < 2)
                self.leftArrow.texture = self.infoSprites.textures['buttons and page indicators/L_arrow_n.png'];

        });

        this.leftArrow.on('pointerout', function () {

            if (self.pageIndex > 1)
                self.leftArrow.texture = self.infoSprites.textures['buttons and page indicators/L_arrow_n.png']
            else
                self.leftArrow.texture = self.infoSprites.textures['buttons and page indicators/L_arrow_d.png']

        });
    }

    showInfo() {
        TweenMax.to(this.mainContainer, 0.4, {y: 0, alpha: 1, ease: Power1.easeOut});
        footer.infoBtn.interactive = false;
        footer.spinButtonSprite.interactive = false;
        footer.soundButton.interactive = false;
        footer.plusButton.interactive = false;
        footer.minusButton.interactive = false;
        footer.fastSpinButton.interactive = false;
        footer.autoSpinButton.interactive = false;
    }

    hideInfo() {
        TweenMax.to(this.mainContainer, 0.4, {y: -app.stage.height, alpha: 0, ease: Power1.easeOut});
        footer.infoBtn.interactive = true;
        footer.spinButtonSprite.interactive = true;
        footer.soundButton.interactive = true;
        footer.plusButton.interactive = true;
        footer.minusButton.interactive = true;
        footer.fastSpinButton.interactive = true;
        footer.autoSpinButton.interactive = true;
    }
}
