var mandatoryParams = [/*'authToken', *//*'cid', 'real',*/ 'game', /*'tournament', *//*'clientType'*/];
let app = [];
let oAL = [];
let game = [];
let freeS = [];
let fsv = [];
let spinH = [];
let footer = [];
let gameCompleted = [];
let user = [];
let infoModal = [];
let rebuy = [];
let modalDialog = [];
let gameContainer = [];
let footerContainer = [];
let titleContainer = [];
let actionContainer = [];
let preloaderContainer = [];
let curtainsContainer = [];
let chairsContainer = [];

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

loadJSON(function (response) {
    let data = JSON.parse(response);
    if (!data.soundStatus)
        PIXI.sound.toggleMuteAll();
});
$(document).ready(async function () {
    var rendererOptions = {
        antialiasing: false,
        transparent: false,
        resolution: window.devicePixelRatio,
        autoResize: true,
    };

    renderer = PIXI.autoDetectRenderer(GAME_WIDTH, GAME_HEIGHT,
        rendererOptions);

// Put the renderer on screen in the corner
    renderer.view.style.position = "absolute";
    renderer.view.style.top = "0px";
    renderer.view.style.left = "0px";
    renderer.view.style.transform = 'translate3d( 50%, 50%, 0% )';

    const canvas = document.getElementById('mycanvas');

    app = new PIXI.Application({
        antialias: true,
        backgroundColor: 0x5b0600,
        view: canvas,
        width: GAME_WIDTH,
        height: GAME_HEIGHT,
        resolution: window.devicePixelRatio,
        autoDensity: true,
    });

    app.stage.sortableChildren = true;
    preloaderContainer = new PIXI.Container();
    preloaderContainer.name = 'preloader-container';
    preloaderContainer.visible = true;
    preloaderContainer.sortableChildren = true;

    gameContainer = new PIXI.Container();
    gameContainer.name = 'game-container';
    gameContainer.width = GAME_WIDTH;
    gameContainer.height = GAME_HEIGHT;
    gameContainer.visible = false;
    gameContainer.sortableChildren = true;
    gameContainer.zIndex = 1;

    footerContainer = new PIXI.Container();
    footerContainer.name = 'footer-container';
    footerContainer.visible = false;
    footerContainer.zIndex = 2;

    actionContainer = new PIXI.Container();
    actionContainer.name = 'action-container';
    actionContainer.visible = true;
    actionContainer.zIndex = 9;
    actionContainer.sortableChildren = true;
    actionContainer.zIndex = 3

    titleContainer = new PIXI.Container();
    titleContainer.name = 'title-container';
    titleContainer.visible = false;
    titleContainer.sortableChildren = true;
    titleContainer.zIndex = 1

    curtainsContainer = new PIXI.Container();
    curtainsContainer.name = 'curtains-container';
    curtainsContainer.visible = false;
    curtainsContainer.sortableChildren = true;
    curtainsContainer.zIndex = 2
    curtainsContainer.width = GAME_WIDTH;
    curtainsContainer.height = GAME_HEIGHT;

    chairsContainer = new PIXI.Container();
    chairsContainer.name = 'chairs-container';
    chairsContainer.visible = true;
    chairsContainer.sortableChildren = true;
    chairsContainer.zIndex = 1


    oAL = new onAssetsLoad(app, gameContainer);

    game = new Game(app, gameContainer);
    spinH = new spinHelper();
    footer = new Footer(app, gameContainer);

    gameCompleted = new gameComplete();
    user = new User();
    infoModal = new Info(app);
    rebuy = new Rebuy(gameContainer);
    freeS = new freeSpin();

    app.stage.addChild(gameContainer, titleContainer, actionContainer, footerContainer, preloaderContainer, curtainsContainer, chairsContainer);
    modalDialog = new ModalDialog(gameContainer);
    fsv = new freeSpinVoucher();
    resize(app)();
    window.addEventListener("resize", resize(app, false));
});

function resize(app, initialCall = true) {
    return function () {
        const vpw = window.innerWidth;  // Width of the viewport
        const vph = window.innerHeight; // Height of the viewport
        let nvw; // New game width
        let nvh; // New game height

        if (vph / vpw < GAME_HEIGHT / GAME_WIDTH) {
            nvh = vph;
            nvw = (nvh * GAME_WIDTH) / GAME_HEIGHT;
        } else {
            // In the else case, the opposite is happening.
            nvw = vpw;
            nvh = (nvw * GAME_HEIGHT) / GAME_WIDTH;
        }
        setTimeout(function () {
            if (settings.DEVICE === 'D') {
                app.renderer.resize(nvw, nvh);

                app.stage.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
            } else {
                app.renderer.resize(vpw, vph);
                gameContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                curtainsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                footerContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                titleContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                actionContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                preloaderContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                curtainsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                chairsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                infoModal.mainContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);

                if (!initialCall) {
                    menuActions.openCloseInfoBar(true);
                    coinMenuBtnActions.openCloseSliderBar(true);
                    setActionContainerTextures();
                    setFooterContainerTextures();
                    setActionContainerPosition();
                    setFooterContainerPosition();
                    footer.adjustFooter(!settings.DEVICE.includes('D') ? 'mobile' : 'desktop');
                    switchTitleTexture();
                    prepareLayoutForDevice();
                    textAdjustment(!settings.DEVICE.includes('D'))
                }
                resetPosition();
            }
        }, 100)
    };
}

function resetPosition() {
    if (settings.DEVICE !== 'D') {
        gameContainer.x = window.innerWidth / 2 - gameContainer.width / 2;
        curtainsContainer.x = window.innerWidth / 2 - gameContainer.width / 2;

        chairsContainer.y = window.innerHeight - chairsContainer.height;
        chairsContainer.x = window.innerWidth / 2 - chairsContainer.width / 2;

        titleContainer.x = window.innerWidth / 2 - titleContainer.width / 2;
        if (window.innerWidth > window.innerHeight) {
            footerContainer.x = window.innerWidth / 2 - footerContainer.width / 2;
            actionContainer.x = window.innerWidth - actionContainer.width;
            actionContainer.y = 0;
            footerContainer.y = window.innerHeight - footerContainer.height - 12;
        } else {
            footerContainer.x = 0;
            footerContainer.y = window.innerHeight - ((window.innerHeight - gameContainer.height * gameContainer.scale.y) / 2 - actionContainer.height);
            actionContainer.y = window.innerHeight - 50;// TODO: need to calculate exact height of actionContainer
            actionContainer.x = 0;
        }
    } else if (settings.DEVICE === 'D') {
        actionContainer.y = GAME_HEIGHT - actionContainer.height - 12;
    }
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        //console.log(e)
    }
};

function isMobile() {
    return settings.DEVICE !== 'D';
}

function tickerStartBalance() {
    let tickStart = tickerStart.play();
    tickStart.on('end', function () {
        tickerMidBalance()
    });
}

function tickerMidBalance() {
    if (settings.STOP_PAYLINE_ANIMATION)
        return;
    let tickMid = tickerMid.play();
    tickMid.on('end', function () {
        if (!settings.TICK_STATUS) {
            if (!settings.STOP_PAYLINE_ANIMATION) {
                let tEnd = tickerEnd.play();
                tEnd.on('end', function () {
                    setSettings.TICKER_SOUND_STATUS = false;
                })
            }
        } else if (!settings.STOP_PAYLINE_ANIMATION)
            tickerMidBalance();
    })
}

function checkTickerStatus() {
    return new Promise((resolve) => {
        var waitForElement = async function () {
            if (!settings.TICKER_SOUND_STATUS) {
                resolve(1);
            } else {
                window.requestAnimationFrame(waitForElement);
            }
        };
        waitForElement();
    })
}

async function waitUntilIsTrue(property) {
    return new Promise((resolve) => {
        var wait = async function () {

            if (settings[property]) {
                return resolve(1);
            } else {
                await sleep(100)
                wait();
            }
        };
        wait();
    })
}


async function waitUntilIsDone(property) {
    return new Promise((resolve) => {
        var wait = async function () {

            if (!settings[property]) {
                return resolve(1);
            } else {
                await sleep(100)
                wait();
            }
        };
        wait();
    })
}

function getUrlParameters() {
    var params = window.location.search.split('&');
    var paramsArr = {};
    $.each(params, function (index, val) {
        paramsArr[val.split('=')[0].replace('?', '')] = val.split('=')[1];
    });
    return paramsArr;
}

function validateGameLaunch(real) {
    var params = getUrlParameters();
    var validParams = true;

    if (real['real'] === 'demo') {
        return true
    } else {
        $.each(mandatoryParams, function (index, val) {
            if (!(val in params)) {
                validParams = false;
                return;
            }
        });
        return validParams;
    }
}

function plus() {

    if (footer.disableFooter) {
        return;
    }
    setSettings.STOP_PAYLINE_ANIMATION = true;
    setSettings.ACTIVITY = 8;

    if (sound)
        button.play()

    if (settings.MIN_BET >= 0.01 && settings.MIN_BET < 0.05) {
        footer.mobileCoinMinus.texture = oAL.panel['coins_minus_n'];
        footer.mobileCoinMinus.interactive = true;
        setSettings.MIN_BET = settings.MIN_BET + 0.01;
    } else if (settings.MIN_BET > 0.04 && settings.MIN_BET < 0.25) {
        setSettings.MIN_BET = settings.MIN_BET + 0.05;
    } else if (settings.MIN_BET >= 0.25 && settings.MIN_BET < 1) {
        setSettings.MIN_BET = settings.MIN_BET + 0.25;
    } else if (settings.MIN_BET >= 1 && settings.MIN_BET < 10) {
        setSettings.MIN_BET = settings.MIN_BET + 1;
    }
    if (settings.MIN_BET === 10) {
        footer.mobileCoinPlus.texture = oAL.panel['coins_plus_d'];
        footer.mobileCoinPlus.interactive = false;
        button.stop()
    }
    footer.betAmount.y = footer.betImage.height - footer.betAmount.height;
}


function minus() {

    if (footer.disableFooter) {
        return;
    }
    setSettings.STOP_PAYLINE_ANIMATION = true;
    setSettings.ACTIVITY = 8;

    if (sound)
        button.play()


    if (settings.MIN_BET < 0.02) {
        setSettings.MIN_BET = 0.01;

    } else if (settings.MIN_BET < 0.06) {
        setSettings.MIN_BET = settings.MIN_BET - 0.01;
    } else if (settings.MIN_BET > 0.06 && settings.MIN_BET <= 0.25) {
        setSettings.MIN_BET = settings.MIN_BET - 0.05;
    } else if (settings.MIN_BET >= 0.25 && settings.MIN_BET <= 1) {
        setSettings.MIN_BET = settings.MIN_BET - 0.25;
    } else if (settings.MIN_BET <= 10 && settings.MIN_BET > 1) {
        setSettings.MIN_BET = settings.MIN_BET - 1;
        footer.mobileCoinPlus.texture = oAL.panel['coins_plus_n'];
        footer.mobileCoinPlus.interactive = true;
    }

    if (settings.MIN_BET === 0.01) {
        footer.mobileCoinMinus.texture = oAL.panel['coins_minus_d'];
        footer.mobileCoinMinus.interactive = false;
        button.stop()
    }
    footer.betAmount.y = footer.betImage.height - footer.betAmount.height;
}

enableFullscreen = ()=> {

    let userInputEventNames = ['click', 'contextmenu', 'auxclick', 'dblclick', 'mousedown', 'mouseup', 'pointerup',

        'touchend', 'keydown', 'keyup'];

    for (var event of userInputEventNames) {

        document.addEventListener(event, this.requestFullscreen);

    }

}



requestFullscreen = ()=> {

    if (this.requesting)

        return;

    this.requesting = true;

    this.removeListeners();



    if (screenfull.isEnabled)

        screenfull.request();

}



removeListeners = ()=> {

    let userInputEventNames = ['click', 'contextmenu', 'auxclick', 'dblclick', 'mousedown', 'mouseup', 'pointerup',

        'touchend', 'keydown', 'keyup'];

    for (var event of userInputEventNames) {

        document.removeEventListener(event, this.requestFullscreen);
    }
}
