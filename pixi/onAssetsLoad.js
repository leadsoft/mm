class onAssetsLoad {
    leftSideIndicatorNumbersImg;
    rightSideIndicatorNumbersImg;

    constructor(app, gameContainer) {
        this.app = app;
        this.gameContainer = gameContainer;
        /*this.mainTextures = [];*/
        this.footer = [];
        this.panel = [];
        this.backgroundElements = [];
        this.winLine;
        this.titleImg;
        this.chairsImg;
        this.curtainsImg;
        this.reelDividerImg;
        this.backgroundImg;
        this.sideIndicatorsImg;
        this.sideIndicatorNumbersImg;
        this.leftSideIndicatorNumbersImg;
        this.rightSideIndicatorNumbersImg;
        this.tick;

        let preloaderTexture = PIXI.Texture.from('newAssets/preload/preload.png?ver=' + version);
        this.preloaderImg = new PIXI.Sprite(preloaderTexture);
        this.preloaderImg.width = GAME_WIDTH;
        this.preloaderImg.x = 0 //-427;
        this.preloaderImg.y = 0;
        this.preloaderImg.height = GAME_HEIGHT;
        preloaderContainer.addChild(this.preloaderImg);

        this.loadingBar = new PIXI.Container();
        this.loadingBar.name = 'loading-bar';
        this.loadingBar.position.y = 426;

        this.loadingBarSprite = new PIXI.Sprite(PIXI.Texture.from('newAssets/preload/loading_bar_bg.png?ver=' + version));
        this.loadingBar.addChild(this.loadingBarSprite);

        PIXI.Loader.shared.add("newAssets/preload/anim/preload_ani.json?ver=" + version).load(loadBrand);

        function loadBrand() {
            let sheet = PIXI.Loader.shared.resources["newAssets/preload/anim/preload_ani.json?ver=" + version].spritesheet;
            let WGS = new PIXI.AnimatedSprite(sheet.animations["preloader_anim"]);
            app.stage.addChild(WGS);
            WGS.animationSpeed = 0.350;
            WGS.name = 'WGS';
            WGS.x = (GAME_WIDTH / 2) - (WGS.width / 2);
            WGS.y = 455/*app.stage.height - WGS.height * 3 */;
            WGS.play();
            logoSound.play();
            WGS.loop = false;
        }

        this.progressAnimation = new PIXI.Sprite(PIXI.Texture.from('newAssets/preload/loading_bar.png?ver=' + version));
        this.progressAnimation.visible = false;
        this.progressAnimationMask = new PIXI.Graphics();
        this.progressAnimationMask.beginFill(0xFFCC00, 1);
        this.progressAnimationMask.drawRoundedRect(4, 0, 520, 24, 50);
        this.progressAnimationMask.endFill();
        this.progressAnimationMask.name = 'mask';

        this.loadingBar.addChild(this.progressAnimationMask); // make sure mask it added to display list somewhere!
        this.loadingBar.mask = this.progressAnimationMask;

        this.loadingBar.addChild(this.progressAnimation);


        this.loadingBarProgressText = new PIXI.Text('', {font: "10px Arial", fill: "white"});
        this.loadingBarProgressText.style.fontSize = 18;

        this.loadingBarProgressText.y = 452;
        this.loadingBarProgressText.name = 'loadingBarProgressText';
        this.preloaderImg.addChild(this.loadingBarProgressText);

        let loader = this.app.loader;
        loader.baseUrl = "newAssets";
        loader
        /*MM START*/
            .add('panels', '_movie magic/dist/common/panels.json?ver=' + version)
            .add('basegame', '_movie magic/dist/basegame.json?ver=' + version)
            .add('freeGamesLayout', '_movie magic/dist/freeGames/freeGamesLayout.json?ver=' + version)
            .add('indicators', '_movie magic/dist/common/payLines.json?ver=' + version)
            .add('dialog', '_movie magic/dist/common/dialog Templates.json?ver=' + version)
            .add('infoPages', '_movie magic/dist/common/infopages.json?ver=' + version)
            .add('preloader', '_movie magic/dist/common/preLoader.png?ver=' + version)
            .add('feature_page', '_movie magic/dist/common/featurePage.json?ver=' + version)
            /*MM END*/
            .add('FS_floodlights', '_movie magic/dist/freeGames/transitionToFreeGames/floodlight.json?ver=' + version)
            .add('curtains', '_movie magic/dist/freeGames/transitionToFreeGames/curtains.json?ver=' + version)
            .add('FS_symbols_animated_bigSymbols_anticipation', '_movie magic/dist/freeGames/symbols/anticipation.json?ver=' + version)
            .add('FS_symbols_fsframes', '_movie magic/dist/freeGames/symbols/frameHighlight.json?ver=' + version)
            /*.add('FS_symbols_animated_bigSymbols_anticipation', 'freespins/symbols/animated/big symbols/anticipation.json?ver=' + version)*/
            .add('FS_symbols_animated_bigSymbols_scatter', 'freespins/symbols/animated/big symbols/scatter.json?ver=' + version)
            .add('FS_symbols_animated_bigSymbols_wild', 'freespins/symbols/animated/big symbols/wild.json?ver=' + version)
            .add('FS_symbols_static', 'freespins/symbols/static.json?ver=' + version)
            /*INFO*/
            .add('background_sound', 'sounds/basegameBackground.mp3?ver=' + version)

            /*TODO: need animation for this*/
            .add('coin_sequence', 'panel/coin_sequence/coin_sequence.json?ver=' + version)
            .add('coin_fountain_animation', 'panel/coin_animation/coin_animation.json?ver=' + version)
            .add('ticker_start', 'sounds/tcikerStart.mp3?ver=' + version)
            .add('ticker_mid', 'sounds/tickerMid.mp3?ver=' + version)
            .add('ticker_end', 'sounds/tickerEnd.mp3?ver=' + version)

        console.log('test')
        loader.onStart.add(this.startLoading.bind(this));
        // loader.onProgress.add(this.showProgress.bind(this));
        // loader.onComplete.add(this.doneLoading.bind(this));
        loader.load(this.onAssetsLoaded.bind(this));
    }

    async startGame() {
        if (!startGamePermission)
            return;

        if (settings.GAME_START)
            return;
        setSettings.GAME_START = true;

        this.buildFooter();

        if (app.stage.getChildByName('feature-container') !== null)
            app.stage.removeChildAt(app.stage.getChildIndex(app.stage.getChildByName('feature-container')));

        freeS.reelContainer.visible = false;
        preloaderContainer.children = [];
        titleContainer.visible = true;
        footerContainer.visible = true
        curtainsContainer.visible = true
        this.chairsImg.visible = true;
        this.curtainsImg.visible = true;
        this.reelDividerImg.visible = true;
        this.sideIndicatorsImg.visible = true;
        this.sideIndicatorNumbersImg.visible = true;
        this.leftSideIndicatorNumbersImg.visible = true;
        this.rightSideIndicatorNumbersImg.visible = true;
        this.gameContainer.visible = true;
        gameCompleted.containerLineIndicators.visible = true;
        oAL.sound.background.loop = true;
        oAL.sound.background.volume = 0.25;
        oAL.sound.background.play();
        setSettings.ACTIVITY = 8;
        game.gameContainer.addChild(game.fakeReelContainer);
        game.gameContainer.addChild(freeS.reelContainer);

        game.gameContainer.getChildByName('reel-container').zIndex = 1;
        game.gameContainer.getChildByName('fake-reel-container').zIndex = 2;
        infoModal.init();
        startInterval();

        switch (settings.GAME_PLAY_MODE) {
            case "real":
                footer.showRealPanel();
                break;
            case "tournament":
                rebuy.showRebuy();
                if (settings.TOURNAMENT_REBUY === "true") {
                    setTimeout(function () {
                        rebuy.showRebuyPopup();
                    }, 500)
                }
                footer.fixTournamentTextPosition();
                break;
            case "demo":
            default:
                footer.showDemoPanel();
                break;
        }
        footer.setPosition(!settings.DEVICE.includes('D'));

        buildMessage();
        this.tick = new Tick();
        freeS.init();

        if (settings.FSV_STATUS) {
            fsv.init();
            disableEnableButtons(false);
            footer.showHideDarkSpinButton(true);
            fsv.showHideAwardedModal();
        }

        if (settings.FS_STATUS) {
            disableEnableButtons(false);
            footer.showHideDarkSpinButton(true);
        }

        setTimeout(function () {
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 2000);
        setTimeout(() => {
            resetPosition();
        }, 200)

        setActionContainerPosition();
        switchTitleTexture();
        prepareLayoutForDevice();
        footerContainer.sortableChildren = true;
        var appGraphics = new PIXI.Sprite(this.panel['app_graphic']);
        appGraphics.name = 'app-graphic';
        appGraphics.zIndex = 0;
        appGraphics.width = window.innerWidth
        appGraphics.height = window.innerHeight
        app.stage.addChild(appGraphics)
        setTimeout(() => {
            textAdjustment(true)
        }, 200)
    }

    startLoading() {
        console.log('loading')
        var queryParams = getUrlParameters();
        var validateParameters = validateGameLaunch(queryParams);

        if (!validateParameters) {
        } else if (queryParams['real'] == 'demo' && (typeof queryParams['tournament'] === 'undefined')) {
            console.log('caca')
            startDemoMode();
        } else if (queryParams['real'] == 'true' && (typeof queryParams['tournament'] === 'undefined' || parseInt(queryParams['tournament']) === 0)) {
            console.log('caca 2')
            startRealMode();
        } else if (typeof queryParams['tournament'] !== 'undefined' && parseInt(queryParams['tournament']) > 0) {
            console.log('caca 3')
            startTournamentMode();
        } else {
            startDemoMode();
        }

        this.progressAnimation.x = 0 //-528;
        this.progressAnimation.visible = true;
        this.progressAnimation.name = 'progress-animation';

        this.loadingBar.position.x = 0 //(GAME_WIDTH / 2) - (/*this.loadingBar.width*/528 / 2);
        preloaderContainer.x = window.innerWidth / 2 - preloaderContainer.width / 2
    }

    async showProgress(v) {
        console.log('progress')
        await sleep(500)
        this.loadingBarProgressText.text = parseInt(v.progress) + '%';
        this.progressAnimation.x = 500//(this.progressAnimation.width * v.progress / 100) - this.progressAnimation.width;
        this.progressAnimation.y = 200
        this.progressAnimation.name = 'progressAnimation';

        this.loadingBarProgressText.x = (GAME_WIDTH / 2) - (this.loadingBarProgressText.width / 2);
        preloaderContainer.x = window.innerWidth / 2 - preloaderContainer.width / 2
    }

    async doneLoading(loader, resources) {
        console.log('done')
        var queryParams = getUrlParameters();
        var validateParameters = validateGameLaunch(queryParams);
        if (!validateParameters) {
            startGamePermission = false;
            modalDialog.show('Invalid Parameters. Please try again later')
        } else {
            /*startGamePermission = false;*/
            await sleep(1000);
            app.stage.getChildByName('WGS').visible = false;
            this.showFeaturePage(resources);
            footer.setFooterImages()

        }
    }

    async showFeaturePage(resources) {
        let self = this;
        let featurePage = resources['feature_page'].spritesheet;
        let featurePageContainer = new PIXI.Container();
        featurePageContainer.name = 'feature-container';
        let featureBg = new PIXI.Sprite(featurePage.textures['fpFeaturepage.png']);
        /*let featureTitle = new PIXI.Sprite(PIXI.Texture.from('feature_title'));*/
        /*featureTitle.x = GAME_WIDTH / 2 - featureTitle.width / 2;
        featureTitle.y = 30;*/
        featureBg.width = GAME_WIDTH;
        featureBg.x = 0;
        featureBg.y = 0;
        featureBg.height = GAME_HEIGHT;

        let playButton = new PIXI.Container();
        playButton.name = 'play-button';
        playButton.visible = true;
        playButton.position.y = 490;

        var graphics = new PIXI.Sprite(featurePage.textures['fpContinue_d.png']);
        playButton.position.x = GAME_WIDTH / 2 - graphics.width / 2 - 427;

        playButton.addChild(graphics);

        playButton
            .on('pointerdown', function () {
                self.startGame();
                goToFullScreen();
                graphics.texture = featurePage.textures['fpContinue_p.png']
            })
            .on('pointerover', function () {
                graphics.texture = featurePage.textures['fpContinue_h.png']
            })
            .on('pointerout', function () {
                graphics.texture = featurePage.textures['fpContinue_n.png']
            });

        featurePageContainer.addChild(featureBg);

        featurePageContainer.addChild(playButton);
        preloaderContainer.children = [];
        preloaderContainer.addChild(featurePageContainer);
        wildFeature.play();

        playButton.buttonMode = true;
        playButton.interactive = true;
        graphics.texture = featurePage.textures['fpContinue_n.png']

        await sleep(6000);
        this.startGame();
    }

    onAssetsLoaded(loader, resources) {
        console.log('loaded')
        this.sound = {
            'background': PIXI.sound.Sound.from(resources['background_sound']),
            'ticker_start': PIXI.sound.Sound.from(resources['ticker_start']),
            'ticker_mid': PIXI.sound.Sound.from(resources['ticker_mid']),
            'ticker_end': PIXI.sound.Sound.from(resources['ticker_end']),
        };

        let mSource = !settings.DEVICE.includes('D') ? 'Mobile' : 'Desktop';
        let isMobile = !settings.DEVICE.includes('D');

        console.log('mS', mSource, 'isMobile', isMobile)

        let backgroundElements = resources['basegame'].spritesheet;
        let panelsElements = resources['panels'].spritesheet;

        this.backgroundElements = {
            'footer_panel': isMobile ? panelsElements.textures['mobile/landscape/baseGame/baseGame/panel_blank.png'] : panelsElements.textures['desktop/baseGame/bgDesktopPanel.png'],
            'background': backgroundElements.textures['baseGameLayout/basegame_bg.png'],
            'chairs': backgroundElements.textures['baseGameLayout/seats.png'],
            'curtains': backgroundElements.textures['baseGameLayout/curtain.png'],
            'reel_divider': backgroundElements.textures['baseGameLayout/reel_divider.png'],
            'side_indicator': backgroundElements.textures['baseGameLayout/side_indicator.png'],
            'side_indicator_bg': backgroundElements.textures['baseGameLayout/side_indicator.png'],
            'side_indicator_numbers_h': backgroundElements.textures['baseGameLayout/side_indicator_numbers_h.png'],
            'left_side_indicator_numbers_n': backgroundElements.textures['baseGameLayout/left_side_indicators_n.png'],
            'right_side_indicator_numbers_n': backgroundElements.textures['baseGameLayout/right_side_indicators_n.png'],
            'title': backgroundElements.textures['baseGameLayout/title.png'],
            'portrait_title': panelsElements.textures['mobile/portrait/baseGame/header.png'],
            'landscape_title': panelsElements.textures['mobile/landscape/baseGame/header.png'],
        };

        this.panel = {
            'app_graphic': panelsElements.textures['mobile/portrait/baseGame/body.png'],
            'coins': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coins_n.png';
                return panelsElements.textures[key];
            }()),
            'quick_spin': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/normalSpin_n.png';
                return panelsElements.textures[key];
            }()),
            'quick_spin_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/normalSpin_h.png';
                return panelsElements.textures[key];
            }()),
            'disable_quick_spin': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/quickspin_d.png';
                return panelsElements.textures[key];
            }()),
            'menu_d': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/menu_d') + '/menu_d.png';
                return panelsElements.textures[key];
            }()),
            'menu_close_d': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/menu_close_d.png';
                return panelsElements.textures[key];
            }()),
            'sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/sound_n.png';
                return panelsElements.textures[key];
            }()),
            'mute_sound_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/soundOff_h.png';
                return panelsElements.textures[key];
            }()),
            'sound_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/sound_h.png';
                return panelsElements.textures[key];
            }()),
            'disable_sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/sound_d.png';
                return panelsElements.textures[key];
            }()),
            'mute_sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/soundOff_n.png';
                return panelsElements.textures[key];
            }()),
            'disable_mute_sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/soundOff_d.png';
                return panelsElements.textures[key];
            }()),
            'info': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/paytable_n.png';
                return panelsElements.textures[key];
            }()),
            'info_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/paytable_h.png';
                return panelsElements.textures[key];
            }()),
            'disable_info': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/paytable_d.png';
                return panelsElements.textures[key];
            }()),

            'coins_plus_n':  (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsUP_n.png';
                return panelsElements.textures[key];
            }()),
            'coins_plus_h':  (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsUP_h.png';
                return panelsElements.textures[key];
            }()),
            'coins_plus_d':  (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsUP_d.png';
                return panelsElements.textures[key];
            }()),
            'coins_plus_p':  (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsUP_p.png';
                return panelsElements.textures[key];
            }()),
            'coins_minus_n': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsDOWN_n.png';
                return panelsElements.textures[key];
            }()),
            'coins_minus_h': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsDOWN_h.png';
                return panelsElements.textures[key];
            }()),
            'coins_minus_d': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsDOWN_d.png';
                return panelsElements.textures[key];
            }()),
            'coins_minus_p': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coinsDOWN_p.png';
                return panelsElements.textures[key];
            }()),

            'plus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_up_n.png'],
            'plus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_up_h.png'],
            'disable_plus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_up_d.png'],
            'minus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_down_n.png'],
            'minus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_down_h.png'],
            'footer_panel': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/spin_n.png'],
            'rebuy_pop_up': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/spin_n.png'],
            'win_popup': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/spin_n.png'],
        };
        loadFooterContainerTextures(panelsElements);
        loadActionContainerTextures(panelsElements);
        freeS.spritesheet.background = resources['freeGamesLayout'].spritesheet;
        freeS.spritesheet.floodlights = resources['FS_floodlights'].spritesheet;
        /*freeS.spritesheet.transitions = [];
        freeS.spritesheet.transitions.push(resources['curtains-0'].spritesheet, resources['curtains-1'].spritesheet)*/
        freeS.spritesheet.transitions = resources['curtains'].spritesheet;
        freeS.spritesheet.bigSymbols_anticipation = resources['FS_symbols_animated_bigSymbols_anticipation'].spritesheet;
        freeS.spritesheet.bigSymbols_scatter = resources['FS_symbols_animated_bigSymbols_scatter'].spritesheet;
        freeS.spritesheet.bigSymbols_wild = resources['FS_symbols_animated_bigSymbols_wild'].spritesheet;
        freeS.spritesheet.fsframes = resources['FS_symbols_fsframes'].spritesheet;
        freeS.spritesheet.static = resources['FS_symbols_static'].spritesheet;
        freeS.freeSpinPanel = {
            'spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_h.png'],
            'spin_disabled': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_d.png'],
            'stop_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames/' + (mSource === 'Mobile/landscape' ? 'stopSpin_n.png' : 'stop_lg_n.png')],
            'stop_spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/' + (mSource === 'Mobile/landscape' ? 'stopSpin_h.png' : 'stop_lg_h.png')],
            'info': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/paytable_n.png'],
            'info_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/paytable_h.png'],
            'disable_info': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/paytable_d.png'],
            'sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/sound_n.png'],
            'sound_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/sound_h.png'],
            'disable_sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/sound_d.png'],
            'mute_sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/soundOff_n.png'],
            'mute_sound_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/soundOff_h.png'],
            'disable_mute_sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/soundOff_d.png'],
            'quick_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_n.png'],
            'quick_spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_h.png'],
            'normal_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_n.png'],
            'normal_spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_h.png'],
            'disable_normal_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_d.png'],
            'disable_quick_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_d.png'],
            'autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/autoplay_n.png'],
            'autoplay_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/autoplay_h.png'],
            'cancel_autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/cancelAutoplay_n.png'],
            'cancel_autoplay_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/cancelAutoplay_h.png'],
            'disable_autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/autoplay_d.png'],
            'disable_cancel_autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/cancelAutoplay_d.png'],
            'plus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_up_n.png'],
            'plus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_up_h.png'],
            'disable_plus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_up_d.png'],
            'minus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_down_n.png'],
            'minus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_down_h.png'],
            'disable_minus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_down_d.png'],
            'footer_panel': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'rebuy_pop_up': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'win_popup': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'home_n': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_n.png'],
            'home_d': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_d.png'],
            'home_h': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_h.png'],
            'home_p': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_p.png'],
            'mobile_coin_selector_bg': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'footer_panel_bg': !isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/fgDesktopPanel.png'] : panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/panel_blank.png'],
            'mobile_bet_set': isMobile ? panelsElements.textures['mobile/landscape/baseGame/baseGame/display_screen2.png'] : '',
            'mobile_coins_enable_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_n.png'] : '',
            'mobile_coins_enable_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_d.png'] : '',
            'mobile_coins_enable_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_h.png'] : '',
            'mobile_coins_enable_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_p.png'] : '',
            'mobile_coins_close_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_n.png'] : '',
            'mobile_coins_close_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_d.png'] : '',
            'mobile_coins_close_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_h.png'] : '',
            'mobile_coins_close_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_p.png'] : '',
            'mobile_coins_plus_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_n.png'] : '',
            'mobile_coins_plus_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_h.png'] : '',
            'mobile_coins_plus_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_d.png'] : '',
            'mobile_coins_plus_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_p.png'] : '',
            'mobile_info_menu': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/display_screen1.png'] : '',
            'mobile_quickspin_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_n.png'] : '',
            'mobile_quickspin_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_h.png'] : '',
            'mobile_quickspin_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_d.png'] : '',
            'mobile_quickspin_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_p.png'] : '',
            'mobile_normalspin_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_n.png'] : '',
            'mobile_normalspin_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_h.png'] : '',
            'mobile_normalspin_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_d.png'] : '',
            'mobile_normalspin_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_p.png'] : '',
            'mobile_menu_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_n.png'] : '',
            'mobile_menu_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_h.png'] : '',
            'mobile_menu_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_d.png'] : '',
            'mobile_menu_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_p.png'] : '',
            'mobile_menu_close_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_p.png'] : '',
            'mobile_menu_close_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_h.png'] : '',
            'mobile_menu_close_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_d.png'] : '',
            'mobile_menu_close_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_p.png'] : '',
        };
        freeS.panelBackup['original_panel'] = this.panel;

        freeS.dialog = resources['dialog'].spritesheet;
        game.symbols = resources['basegame'].spritesheet;


        freeS.slotTextures = [
            game.symbols.textures["symbols/static/sym_00_wild_n.png"],
            game.symbols.textures["symbols/static/sym_01_ms1_n.png"],
            game.symbols.textures["symbols/static/sym_02_ms2_n.png"],
            game.symbols.textures["symbols/static/sym_03_ms3_n.png"],
            game.symbols.textures["symbols/static/sym_04_ms4_n.png"],
            game.symbols.textures["symbols/static/sym_05_A_n.png"],
            game.symbols.textures["symbols/static/sym_06_K_n.png"],
            game.symbols.textures["symbols/static/sym_07_Q_n.png"],
            game.symbols.textures["symbols/static/sym_08_J_n.png"],
            game.symbols.textures["symbols/static/sym_09_scatter_n.png"],
        ];

        freeS.bigSlotTextures = [
            freeS.spritesheet.static.textures["big symbols/big_sym_wild_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms1_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms2_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms3_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms4_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_A_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_K_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_Q_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_J_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_scatter_n.png"],
        ];

        freeS.bigSymbolHighlighted = {
            "0": freeS.spritesheet.static.textures["big symbols/big_sym_wild_h.png"],
            "1": freeS.spritesheet.static.textures["big symbols/big_sym_ms1_h.png"],
            "2": freeS.spritesheet.static.textures["big symbols/big_sym_ms2_h.png"],
            "3": freeS.spritesheet.static.textures["big symbols/big_sym_ms3_h.png"],
            "4": freeS.spritesheet.static.textures["big symbols/big_sym_ms4_h.png"],
            "5": freeS.spritesheet.static.textures["big symbols/big_sym_A_h.png"],
            "6": freeS.spritesheet.static.textures["big symbols/big_sym_K_h.png"],
            "7": freeS.spritesheet.static.textures["big symbols/big_sym_Q_h.png"],
            "8": freeS.spritesheet.static.textures["big symbols/big_sym_J_h.png"],
            "9": freeS.spritesheet.static.textures["big symbols/big_sym_scatter_h.png"]
        };

        game.slotTextures = [
            game.symbols.textures["symbols/static/sym_00_wild_n.png"],
            game.symbols.textures["symbols/static/sym_01_ms1_n.png"],
            game.symbols.textures["symbols/static/sym_02_ms2_n.png"],
            game.symbols.textures["symbols/static/sym_03_ms3_n.png"],
            game.symbols.textures["symbols/static/sym_04_ms4_n.png"],
            game.symbols.textures["symbols/static/sym_05_A_n.png"],
            game.symbols.textures["symbols/static/sym_06_K_n.png"],
            game.symbols.textures["symbols/static/sym_07_Q_n.png"],
            game.symbols.textures["symbols/static/sym_08_J_n.png"],
            game.symbols.textures["symbols/static/sym_09_scatter_n.png"],
        ];

        game.slotTexturesHighlighted = {
            'symbol0_h': game.symbols.textures["symbols/static/sym_00_wild_n.png"],
            'symbol1_h': game.symbols.textures["symbols/static/sym_01_ms1_n.png"],
            'symbol2_h': game.symbols.textures["symbols/static/sym_02_ms2_n.png"],
            'symbol3_h': game.symbols.textures["symbols/static/sym_03_ms3_n.png"],
            'symbol4_h': game.symbols.textures["symbols/static/sym_04_ms4_n.png"],
            'symbol5_h': game.symbols.textures["symbols/static/sym_05_A_n.png"],
            'symbol6_h': game.symbols.textures["symbols/static/sym_06_K_n.png"],
            'symbol7_h': game.symbols.textures["symbols/static/sym_07_Q_n.png"],
            'symbol8_h': game.symbols.textures["symbols/static/sym_08_J_n.png"],
            'symbol9_h': game.symbols.textures["symbols/static/sym_09_scatter_n.png"]
        };

        game.symbolAnimation = {
            "0": game.symbols.animations['symbols/animated/sym_00_wild_a/sym_00_wild_a'],
            "0-h": game.symbols.animations['symbols/highlighted/sym_00_Wild_h/sym_00_wild_h'],
            "1": game.symbols.animations['symbols/highlighted/sym_01_ms1_h/sym_01_ms1_h'],
            "2": game.symbols.animations['symbols/highlighted/sym_02_ms2_h/sym_02_ms2_h'],
            "3": game.symbols.animations['symbols/highlighted/sym_03_ms3_h/sym_03_ms3_h'],
            "4": game.symbols.animations['symbols/highlighted/sym_04 ms4_h/sym_04_ms4_h'],
            "5": game.symbols.animations['symbols/highlighted/sym_05_A_h/sym_05_A_h'],
            "6": game.symbols.animations['symbols/highlighted/sym_06_K__h/sym_06_K_h'],
            "7": game.symbols.animations['symbols/highlighted/sym_06_Q_h/sym_06_Q_h'],
            "8": game.symbols.animations['symbols/highlighted/sym_07_J_h/sym_07_J_h'],
            "9": game.symbols.animations['symbols/animated/sym_09_scatter_a/sym_09_scatter_a'],
            "9-h": game.symbols.animations['symbols/highlighted/sym_09_Scatter_h/sym_09_scatter_h']
        };

        game.animations = {
            "frame_symbol_anim": game.symbols.animations['symbols/frameHighlight'],
            "frame_symbol_flashing": game.symbols.animations['symbols/frameHighlight/sm_flashing'],
            "coin_sequence": resources['coin_sequence'].spritesheet,
            "coin_fountain_animation": resources['coin_fountain_animation'].spritesheet,
        };

        infoModal.infoSprites = resources['infoPages'].spritesheet;

        this.chairsImg = new PIXI.Sprite(this.backgroundElements['chairs']);
        this.chairsImg.name = 'chairsImg';
        this.curtainsImg = new PIXI.Sprite(this.backgroundElements['curtains']);
        this.curtainsImg.name = 'curtainsImg';
        this.reelDividerImg = new PIXI.Sprite(this.backgroundElements['reel_divider']);
        this.sideIndicatorsImg = new PIXI.Sprite(this.backgroundElements['side_indicator_bg']);
        this.sideIndicatorNumbersImg = new PIXI.Sprite(this.backgroundElements['side_indicator_numbers_h']);
        this.leftSideIndicatorNumbersImg = new PIXI.Sprite(this.backgroundElements['left_side_indicator_numbers_n']);
        this.rightSideIndicatorNumbersImg = new PIXI.Sprite(this.backgroundElements['right_side_indicator_numbers_n']);
        this.titleImg = new PIXI.Sprite(this.backgroundElements['title']);

        this.buildBackground();

        game.paylines = [];
        let totalMargin = settings.REEL_TOTAL_MARGIN;
        game.fakeReelContainer.y = freeS.reelContainer.y = game.reelContainer.y = REEL_CONTAINER_MARGIN_TOP
        game.fakeReelContainer.x = freeS.reelContainer.x = game.reelContainer.x = (GAME_WIDTH / 2) - ((SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER) / 2)
        game.fakeReelContainer.height = freeS.reelContainer.height = game.reelContainer.height = SYMBOL_SIZE * 3;
        game.fakeReelContainer.width = freeS.reelContainer.width = game.reelContainer.width = SYMBOL_CONTAINER_WIDTH * 5;

        let color = [0xfff000, 0xffaa00, 0xff1200, 0xffabc0, 0xff9900];

        let count = 0;
        for (let i = 0; i < settings.REEL_NUMBER; i++) {
            var bg = new PIXI.Sprite(PIXI.Texture.WHITE);

            let rc = new PIXI.Container();
            rc.x = i * SYMBOL_CONTAINER_WIDTH;
            rc.width = SYMBOL_CONTAINER_WIDTH;
            rc.height = 3 * SYMBOL_SIZE;

            game.reelContainer.addChild(rc);

            const reel = {
                container: rc,
                symbols: [],
                position: 0,
                previousPosition: 0,
                blur: new PIXI.filters.BlurFilter(),
            };

            reel.blur.blurX = 0;
            reel.blur.blurY = 0;
            rc.filters = [reel.blur];

            // Build the symbols
            for (let j = 0; j < 12; j++) {
                let randomSymbol = Math.floor(Math.random() * game.slotTextures.length);
                let rand = randomSymbol === 0 ? 1 : randomSymbol;
                const symbol = new PIXI.Sprite(game.slotTextures[rand]);
                symbol.fake = j < 4;
                symbol.symbolElement = rand;
                symbol.y = j * SYMBOL_CONTAINER_SIZE;
                symbol.x = (SYMBOL_CONTAINER_WIDTH / 2) - (SYMBOL_WIDTH / 2);
                count++;
                symbol.symbolPosition = count - 2;
                symbol.column = i;
                symbol.row = j;
                reel.symbols.push(symbol);
                symbol.zOrder = 2;
                rc.addChild(symbol);
            }

            game.reels.push(reel);
            bg.tint = color[i];
            bg.width = SYMBOL_CONTAINER_WIDTH;
            bg.height = 3 * SYMBOL_SIZE;
            bg.alpha = 0.4
            //rc.addChild(bg);
        }

        let freeCount = 0;
        let freeMargin = 0;
        /*BUILD FREESPIN CONTAINER*/
        for (let a = 0; a < 3; a++) {
            let fRc = new PIXI.Container();


            fRc.zIndex = 5;
            if (a > 0)
                freeMargin += totalMargin / 2;

            freeS.reelContainer.addChild(fRc);

            const freeSreel = {
                container: fRc,
                symbols: [],
                position: 0,
                previousPosition: 0,
                blur: new PIXI.filters.BlurFilter(),
            };

            freeSreel.blur.blurX = 0;
            freeSreel.blur.blurY = 0;
            fRc.filters = [freeSreel.blur];
            fRc.name = 'freeSReel'

            if (a === 0) {
                fRc.width = SYMBOL_CONTAINER_WIDTH;
            } else if (a === 1) {
                fRc.width = SYMBOL_CONTAINER_WIDTH * 3
                fRc.x = SYMBOL_CONTAINER_WIDTH;
            } else if (a === 2) {
                fRc.width = SYMBOL_CONTAINER_WIDTH
                fRc.x = SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER - SYMBOL_CONTAINER_WIDTH
            }


            // Build the symbols
            let symbolLength = 12//4;

            for (let b = 0; b < symbolLength; b++) {
                let randomSymbol = Math.floor(Math.random() * freeS.slotTextures.length);
                let rand = randomSymbol === 0 ? 1 : randomSymbol;

                const freeSsymbol = new PIXI.Sprite(a !== 1 ? freeS.slotTextures[rand] : freeS.bigSlotTextures[rand]);
                freeSsymbol.symbolElement = rand;
                // Scale the symbol to fit symbol area.

                freeSsymbol.width = a !== 1 ? SYMBOL_CONTAINER_WIDTH : freeSsymbol.width;
                freeSsymbol.height = a !== 1 ? SYMBOL_SIZE : freeSsymbol.height;
                freeSsymbol.y = a !== 1 ? b * SYMBOL_SIZE + (b * SYMBOL_MARGIN_TOP) : b * freeSsymbol.width + (b * SYMBOL_MARGIN_TOP);

                freeCount++;
                freeSsymbol.symbolPosition = freeCount - 2;
                freeSsymbol.pickPosition = a + (b - 1) * 5;
                freeSsymbol.column = a;
                freeSsymbol.row = a;
                freeSreel.symbols.push(freeSsymbol);
                freeSsymbol.zOrder = 2;
                fRc.addChild(freeSsymbol);
            }

            freeS.reels.push(freeSreel);
        }
        /*END FREESPIN CONTAINER*/

        for (let i = 0; i <= 2; i++) {
            let gameAnticipation = [];
            if (i === 1) { //BIGSYMBOL
                gameAnticipation = new PIXI.AnimatedSprite(freeS.spritesheet.bigSymbols_anticipation.animations['bigSymbols/fgBigSymAnticiaption']);
            } else {
                gameAnticipation = new PIXI.AnimatedSprite(resources['basegame'].spritesheet.animations['symbols/anticipation/bgSymAnticiaption']);
            }
            gameAnticipation.name = 'anticipation' + i;
            gameAnticipation.zIndex = 1;
            gameAnticipation.loop = true;
            gameAnticipation.visible = false;
            if (i === 0)
                gameAnticipation.x = 80//game.reelContainer.x;
            else if (i === 1)
                gameAnticipation.x = GAME_WIDTH / 2 - gameAnticipation.width / 2;
            else
                gameAnticipation.x = 747//game.reelContainer.children[game.reelContainer.children.length - 1].x + game.reelContainer.x;

            gameAnticipation.y = 80;
            gameAnticipation.animationSpeed = 0.5;
            game.anticipation.push(gameAnticipation);
            gameContainer.addChild(gameAnticipation);
        }


        const mask = new PIXI.Sprite(PIXI.Texture.WHITE);
        const freeSpinMask = new PIXI.Sprite(PIXI.Texture.WHITE);
        /*mask.tint = 0xff0000;*/
        freeSpinMask.height = mask.height = SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP);
        freeSpinMask.width = mask.width = settings.REEL_NUMBER * SYMBOL_CONTAINER_WIDTH;

        game.reelContainer.addChild(mask);
        game.reelContainer.mask = mask;
        freeS.reelContainer.addChild(freeSpinMask);
        freeS.reelContainer.mask = freeSpinMask;


        game.gameContainer.addChild(game.reelContainer, game.reelBackground, game.reelOverlay, freeS.reelContainer);
        game.reelOverlay.x = game.reelContainer.x /*+ 5*/;
        game.reelOverlay.y = 100//game.reelContainer.y /*- 5*/;
        game.reelOverlay.width = game.reelContainer.width + 20;
        game.reelOverlay.height = game.reelContainer.height - SYMBOL_SIZE + 60;
        game.reelOverlay.alpha = 0;
        game.reelOverlay.zIndex = 2;

        setTimeout(function () {
            gameCompleted.loadLinesIndicators(resources);
            //gameCompleted.loadLines(resources);
        }, 2000)
        this.buildWinLine();
    }

    buildWinLine() {
        this.winLine = new PIXI.Graphics();
        this.winLine.name = 'win-line';
        this.winLine.visible = false;

        this.winLine.beginFill(0x00d0ff);
        this.winLine.lineStyle(1, 0x1000ff);
        this.winLine.drawRect(0, 0, game.reelContainer.width, 6);
        this.winLine.x = game.reelContainer.x;
        this.winLine.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - 2);
        this.winLine.zIndex = 4;
        app.stage.addChild(this.winLine);
    }

    buildBackground() {

        this.backgroundImg = new PIXI.Sprite(this.backgroundElements['background']);
        this.backgroundImg.width = GAME_WIDTH;
        this.backgroundImg.height = GAME_HEIGHT;
        this.gameContainer.addChild(this.backgroundImg);

        this.chairsImg.y = 0;
        this.chairsImg.x = (GAME_WIDTH / 2) - (this.chairsImg.width / 2);
        this.chairsImg.visible = false;
        this.chairsImg.zIndex = 8;

        this.curtainsImg.y = 0;
        this.curtainsImg.x = (GAME_WIDTH / 2) - (this.curtainsImg.width / 2);
        this.curtainsImg.visible = false;

        this.reelDividerImg.y = 0;
        this.reelDividerImg.name = 'reel-divider';
        this.reelDividerImg.x = (GAME_WIDTH / 2) - (this.reelDividerImg.width / 2);
        this.reelDividerImg.visible = false;

        this.sideIndicatorsImg.y = 18;
        this.sideIndicatorsImg.name = 'side-indicators';
        this.sideIndicatorsImg.x = (GAME_WIDTH / 2) - (this.sideIndicatorsImg.width / 2);
        this.sideIndicatorsImg.visible = false;
        this.sideIndicatorsImg.zIndex = 2;

        this.leftSideIndicatorNumbersImg.y = 92;
        this.leftSideIndicatorNumbersImg.name = 'left-side-indicator-numbers';
        this.leftSideIndicatorNumbersImg.x = 77;
        this.leftSideIndicatorNumbersImg.visible = false;
        this.leftSideIndicatorNumbersImg.zIndex = 3;

        this.rightSideIndicatorNumbersImg.y = 92
        this.rightSideIndicatorNumbersImg.name = 'right-side-indicator-numbers';
        this.rightSideIndicatorNumbersImg.x = GAME_WIDTH - 100;
        this.rightSideIndicatorNumbersImg.visible = false;
        this.rightSideIndicatorNumbersImg.zIndex = 3;

        this.sideIndicatorsImg.addChild(this.leftSideIndicatorNumbersImg, this.rightSideIndicatorNumbersImg);

        /*this.sideIndicatorNumbersImg.y = 0;
        this.sideIndicatorNumbersImg.name = 'side-indicator-numbers';
        this.sideIndicatorNumbersImg.x = (GAME_WIDTH / 2) - (this.sideIndicatorsImg.width / 2);
        this.sideIndicatorNumbersImg.visible = false;
        this.sideIndicatorNumbersImg.zIndex = 3;*/

        this.titleImg.name = 'image';

        /*this.app.stage.addChild(this.chairsImg);*/
        /*gameContainer.addChild(this.chairsImg);*/
        chairsContainer.addChild(this.chairsImg);
        /*curtainsContainer.addChild(this.curtainsImg);*/
        gameContainer.addChild(this.reelDividerImg);
        gameContainer.addChild(this.sideIndicatorsImg);
        //this.app.stage.addChild(this.sideIndicatorNumbersImg);
        titleContainer.addChild(this.titleImg);
    }

    buildFooter() {
        let isMobile = !settings.DEVICE.includes('D')
        footer.show(isMobile)
    }
}



