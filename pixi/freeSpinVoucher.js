class freeSpinVoucher {
    constructor() {

    }

    init() {
        this.freeCouponContainer = new PIXI.Container();
        var graphics = new PIXI.Graphics();
        graphics.width = 300;
        graphics.height = 75;

        graphics.beginFill(0x000000);

        graphics.lineStyle(1, 0xFFFFFF);
        graphics.drawRect(0, 0, 300, 80);
        graphics.name = 'graphic';
        graphics.alpha = 0.6;
        /*this.awardedText = new PIXI.Text("FREE SPIN AWARDED \nStarting " + settings.FSV_REMAINING + " of " + settings.FSV_AWARDED + " Plays\n\nGood Luck", {*/
        this.awardedText = new PIXI.Text("FREE SPINS VOUCHER REDEEMED \nStarting " + settings.FSV_AWARDED + " Complimentary Spins\n\nGood Luck", {
            font: "Arial",
            fill: "0xffffff",
            fontSize: 15,
            align: "center"
        });

        this.freeCouponContainer.addChild(graphics);
        this.freeCouponContainer.addChild(this.awardedText);
        this.awardedText.x = (this.freeCouponContainer.width / 2) - this.awardedText.width / 2;
        this.awardedText.y = 5;

        this.freeCouponContainer.zIndex = 9;

        app.stage.addChild(this.freeCouponContainer);
        this.freeCouponContainer.x = 20//GAME_WIDTH / 2 - graphics.width / 2;
        this.freeCouponContainer.y = -this.freeCouponContainer.height
    }

    async showHideAwardedModal() {
        await sleep(1000);
        this.freeCouponContainer.x = 20//GAME_WIDTH / 2 - this.freeCouponContainer.width / 2;
        this.freeCouponContainer.width = 300;
        TweenMax.to(this.freeCouponContainer, 0.6, {y: 0, ease: Bounce.easeOut});
        await sleep(2000);
        TweenMax.to(this.freeCouponContainer, 0.6, {y: -this.freeCouponContainer.height, ease: Power2.easeIn});
        await sleep(1000);
        this.freeCouponContainer.x = 20;

        this.awardedText.text = "FREE SPIN \nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + "\n\n" + settings.FSV_REMAINING + " plays remaining";
        this.awardedText.x = (this.freeCouponContainer.width / 2) - this.awardedText.width / 2;
        TweenMax.to(this.freeCouponContainer, 0.6, {y: 0, ease: Bounce.easeOut});
        spinH.activeSpinButton();
    }

    async closeFreeCouponSession() {
        setSettings.REQUEST_END = false;
        disableEnableButtons(false);
        footer.showHideDarkSpinButton(true);
        await sleep(2000);
        startGame();
        await waitUntilIsDone('REQUEST_END');
        await sleep(1000);
        TweenMax.to(this.freeCouponContainer, 0.6, {
            y: -this.freeCouponContainer.height,
            ease: Power2.easeIn,
            onComplete: function (e) {
                modalDialog.show('We hope that you have enjoyed your complementary game play!<br>You have won <span style="color: #FFFF00">' + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + '</span> which has been added to your bonus balance.', 'reward')
                disableEnableButtons(true);
                /*setSettings.PROMO_CHIPS += parseFloat(settings.FSV_TOTAL_WIN).toFixed(2)*/
                setSettings.CAN_BET = true;
                footer.showHideDarkSpinButton(false);
                footer.spinButtonSprite.visible = true;
                startGame();
            },
        });

        await sleep(1000);
        setSettings.FSV_TOTAL_WIN = 0;
        setSettings.FSV_COUPON_ID = "";
    }

    start() {
        disableEnableButtons(false);
    }

    end() {
        disableEnableButtons(true);
    }

    awardedTextControl(startGame = false) {
        if (settings.FSV_STATUS)
            this.awardedText.text = "FREE SPIN \nTOTAL WIN: " + user.currencySymbol + (parseFloat(settings.FSV_TOTAL_WIN) + parseFloat(settings.TOTAL_BONUS_WIN || 0)).toFixed(2) + "\n\n" + (settings.FSV_REMAINING - startGame) + " plays remaining";

    }
}

