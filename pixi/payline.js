class Payline {
    constructor() {
        this.lines = [];
        this.paylinesNumber = 25
    }

    loadLines() {
        for (let i = 1; i <= this.paylinesNumber; i++) {
            const line = new PIXI.Sprite(PIXI.Texture.from('line' + i));
            line.width = REEL_WIDTH * 5;
            line.visible = false;

            this.lines.push(line);
            game.reelContainer.addChild(line);
        }
    }



    hideAllLines() {
        for (let i = 0; i < this.paylinesNumber; i++) {
            this.lines[i].visible = false;
        }
    }

    async showAllWinningLines() {
        let self = this;
        for (let i = 0; i < game.paylines.length; i++) {
            let payline = game.paylines[i];
            if (payline.payline > 0) {
                self.lines[payline.payline].visible = true;
            }
        }

        await sleep(2000);

        self.hideAllLines();
    }
}

/*function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}*/
