class freeSpin {

    constructor() {
        this.name = 'free-spin';
        this.spritesheet = [];
        this.runnerAnimation = [];
        this.dialog = [];
        this.curtainsStatus = false;
        this.slotTextures = [];
        this.bigSlotTextures = [];
        this.reels = [];
        this.tweening = [];
        this.slotPosition = [];
        this.reelContainer = new PIXI.Container();
        this.reelBackground = new PIXI.Sprite();
        this.reelOverlay = new PIXI.Sprite(PIXI.Texture.WHITE);
        this.reelOverlay.tint = 0x000000;
        this.symbols = [];
        this.reelContainer.name = 'free-s-reel-container';
        this.anticipationStatus = false;
        this.freeSpinPanel = {};
        this.totalWin = '';
        this.totalWinAmount = '';

        this.totalWin = new PIXI.Text('TOTAL WIN', {
            font: "10px Arial",
            fill: "0xffffff",
            fontWeight: 'bold',
            fontSize: 11
        });
        this.totalWin.name = 'total-win-text';
        this.totalWin.x = 693;
        this.totalWin.y = 525;
        this.totalWin.zIndex = 2;
        this.totalWin.visible = false;

        this.totalWinAmount = new PIXI.Text('', {font: "10px Arial", fill: "0xffffff", fontSize: 18});
        this.totalWinAmount.name = 'total-win-amount';
        this.totalWinAmount.zIndex = 2;
        this.totalWinAmount.visible = false;
        this.panelBackup = {};
        this.randomElement = {0: true, 1: true, 2: true};

        gameContainer.addChild(this.totalWin);
        gameContainer.addChild(this.totalWinAmount);
        footer.totalBalanceImage.addChild(this.totalWinAmount);

        app.ticker.add((delta) => {
            let now = new Date();

            const remove = [];
            for (let i = 0; i < this.tweening.length; i++) {

                const t = this.tweening[i];

                const phase = Math.min(1, (now - t.start) / t.time);
                t.propertyBeginValue = 0; //reset value
                if (i === this.tweening.length - 1) {
                    t.target = settings.TARGET_PROPERTY /*+ 5*/; //reset value
                } else {
                    t.target = settings.TARGET_PROPERTY; //reset value*/
                }

                t.object[t.property] = freeSpin.lerp(t.propertyBeginValue, t.target, t.easing(phase));

                if (t.change)
                    t.change(t);

                if (phase > 0.3)
                    this.randomElement[parseInt(t.object.reelPosition)] = false;

                if (phase === 1) {
                    t.object[t.property] = t.target;

                    if (parseInt(t.object.reelPosition) === 0 && spinH.firstReel.includes(settings.FS_FEATURES_RES)) {

                        this.anticipationStatus = true;
                        game.anticipation.forEach(function (val, key) {
                            val.visible = true;
                            val.gotoAndPlay(0)
                        });
                    }
                    if (parseInt(t.object.reelPosition) === 2)
                        setSettings.ANTICIPATION = false;

                    if (t.complete) t.complete(t);
                    remove.push(t);
                } else if (settings.STOP_REEL) {
                    for (let l = 0; l < Object.keys(game.randomElement).length; l++) {
                        this.randomElement[l] = false;
                    }
                    setSettings.ANTICIPATION = false;
                    reelStop.play();
                    footer.winAmount.text = '';
                    t.object.scatterSymbol = false;
                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                }

                if (this.anticipationStatus && parseInt(t.object.reelPosition) === 2) {
                    t.time += 2000;
                    t.start = Date.now(Date.now());
                    t.target = 60; //reset value
                    this.anticipationStatus = false;
                }
            }

            for (let i = 0; i < remove.length; i++)
                this.tweening.splice(this.tweening.indexOf(remove[i]), 1);

        });

        app.ticker.add((delta) => {
            // Update the slots.
            /*if (this.spinStatus)*/
            for (let i = 0; i < this.reels.length; i++) {
                const r = this.reels[i];
                // Update blur filter y amount based on speed.
                // This would be better if calculated with time in mind also. Now blur depends on frame rate.
                r.blur.blurY = (r.position - r.previousPosition) * 40;
                r.previousPosition = r.position;

                // Update symbol positions on reel.
                for (let j = 0; j < r.symbols.length; j++) {
                    const s = r.symbols[j];
                    const prevy = s.y;
                    if (i === 1)
                        s.y = ((r.position + j) % r.symbols.length) * s.height - s.height + (j * SYMBOL_MARGIN_TOP);
                    else
                        s.y = ((r.position + j) % r.symbols.length) * SYMBOL_CONTAINER_SIZE - SYMBOL_CONTAINER_SIZE /*+ (j * SYMBOL_MARGIN_TOP)*/;

                    if ((s.y < 0 && prevy > SYMBOL_SIZE * 3 && i === 1) || (s.y < 0 && prevy > SYMBOL_SIZE) || Object.keys(spinH.slotPosition).length > 0) {

                        if (typeof spinH.freeSlotPosition[i] !== "undefined") {
                            r.reelPosition = i;
                            let symbolSource = i !== 1 ? this.slotTextures : this.bigSlotTextures;
                            //console.log(spinH.freeSlotPosition, 'spinH.freeSlotPosition');
                            s.texture = symbolSource[spinH.freeSlotPosition[i][j]];
                            /*let tezt = new PIXI.Text(j, {font: "Arial", fill: "0xffe000"})
                            tezt.style.fontSize = 18;
                            s.addChild(tezt);*/
                            //s.texture = !this.randomElement[i] ? symbolSource[spinH.freeSlotPosition[i][j]] : symbolSource[Math.floor(Math.random() * symbolSource.length)];
                            s.visible = true;
                            s.symbolElement = spinH.freeSlotPosition[i][j];
                            //if(!this.randomElement[i]) {
                            /*delete spinH.freeSlotPosition[i][j];
                            if (Object.keys(spinH.freeSlotPosition[i]).length === 0)
                                delete spinH.freeSlotPosition[i];*/
                            //}
                        }
                    }
                }
            }
        });
    }

    init() {
        this.dialogContainer = new PIXI.Container();
        this.dialogContainer.zIndex = 1;
        this.dialogContainer.zIndex = 12;
        this.dialogContainer.name = 'dialog-container';
        this.dialogContainer.visible = false;
        this.dialogBoxStageLight = new PIXI.Sprite(this.dialog.textures['themedTemplates/stage_light.png']);
        this.dialogBoxStageLight.alpha = 0;
        this.dialogBoxImage = new PIXI.Sprite(this.dialog.textures['themedTemplates/Dialogue-box.png']);

        this.lightFlashing = new PIXI.AnimatedSprite(this.dialog.animations['themedTemplates/dialogBoxLights']);
        this.dialogBoxText = new PIXI.Text("\nIT’S SHOW TIME.  \nFREE GAMES AWARDED", {
            font: "Arial",
            fill: "0x000000",
            fontSize: 25,
            align: "center"
        });

        this.dialogContainer.addChild(this.dialogBoxStageLight, this.dialogBoxImage, this.lightFlashing, this.dialogBoxText);
        this.dialogContainer.y = 0;
        this.dialogBoxText.x = this.dialogContainer.width / 2 - this.dialogBoxText.width / 2;
        this.dialogBoxText.y = 210;
        this.lightFlashing.loop = true;
        this.lightFlashing.animationSpeed = 0.100;
        //this.lightFlashing.play();

        this.freeSpinContainer = new PIXI.Container();
        this.freeSpinContainer.name = 'free-spin-container';
        var graphics = new PIXI.Graphics();
        graphics.width = 300;
        graphics.height = 75;

        graphics.beginFill(0x000000);

        graphics.lineStyle(1, 0xFFFFFF);
        graphics.drawRect(0, 0, 300, 80);
        graphics.name = 'graphic';
        graphics.alpha = 0.6;
        this.awardedText = new PIXI.Text("", {
            font: "Arial",
            fill: "0xffffff",
            fontSize: 15,
            align: "center"
        });

        this.freeSpinContainer.addChild(graphics);
        this.freeSpinContainer.addChild(this.awardedText);
        this.awardedText.x = (this.freeSpinContainer.width / 2) - this.awardedText.width / 2;
        this.awardedText.y = 5;

        this.freeSpinContainer.zIndex = 9;

        gameContainer.addChild(this.freeSpinContainer);
        gameContainer.addChild(this.dialogContainer);
        this.freeSpinContainer.x = 50;
        this.freeSpinContainer.y = -this.freeSpinContainer.height;

        this.animations = {
            /*"curtains_transition": this.spritesheet.transitions.animations['curtain close-open/curtain_close'],*/
            "frame_symbol_flashing": this.spritesheet.fsframes.animations['fsframe'],
            /*"frame_big_symbol_flashing": this.spritesheet.fsframes.animations['bigSymbols/fgBigSymHighlight'],*/
            "frame_big_symbol_flashing": this.spritesheet.fsframes.animations['bigSymbols/frame_hilight_lg'],
        };

        this.curtainsAnimation();
        this.symbols = this.spritesheet;

        /*this.courtainsAnimationOpen = new PIXI.AnimatedSprite(freeS.animations['curtains_transition']);
        this.courtainsAnimationOpen.loop = false;
        this.courtainsAnimationOpen.zIndex = 7;
        this.courtainsAnimationOpen.animationSpeed = 0.450;
        app.stage.addChild(this.courtainsAnimationOpen);*/

        /*this.slotTexturesHighlighted = {
            'symbol0_h': this.spritesheet.static.textures["standard size/fssym_wild_n.png"],
            'symbol1_h': this.spritesheet.static.textures["standard size/fssym_ms1_n.png"],
            'symbol2_h': this.spritesheet.static.textures["standard size/fssym_ms2_n.png"],
            'symbol3_h': this.spritesheet.static.textures["standard size/fssym_ms3_.png"],
            'symbol4_h': this.spritesheet.static.textures["standard size/fssym_ms4_n.png"],
            'symbol5_h': this.spritesheet.static.textures["standard size/fssym_A_n.png"],
            'symbol6_h': this.spritesheet.static.textures["standard size/fssym_K_n.png"],
            'symbol7_h': this.spritesheet.static.textures["standard size/fssym_Q_n.png"],
            'symbol8_h': this.spritesheet.static.textures["standard size/fssym_J_n.png"],
            'symbol9_h': this.spritesheet.static.textures["standard size/fssym_10_n.png"],
            'symbol10_h': this.spritesheet.static.textures["standard size/fssym_9_n.png"],
            'symbol11_h': this.spritesheet.static.textures["standard size/fssym_scatter_n.png"]
        };*/
    }

    curtainsAnimation() {
        /* const multiPack = freeS.spritesheet.transitions[0].data.meta.related_multi_packs;

         let multiPackTextureMap = {...freeS.spritesheet.transitions[0].textures};


         if (multiPack && multiPack.length > 0) {
             multiPack.forEach((mpack) => {
                 const textureName = mpack.replace(".json", "");
                 const _mpackSheet = app.loader.resources[textureName];
                 if (_mpackSheet.textures) {

                     multiPackTextureMap = Object.assign(
                         multiPackTextureMap,
                         _mpackSheet.textures
                     );
                 }
             });
         }
         let multiPackTextureMapArray = Object.values(multiPackTextureMap)
         multiPackTextureMapArray.reverse();*/
        /*this.runnerAnimation = new PIXI.AnimatedSprite(multiPackTextureMapArray);*/
        this.runnerAnimation = new PIXI.AnimatedSprite(freeS.spritesheet.transitions.animations['curtainClose']);
        this.runnerAnimation.name = 'curtains';

        this.runnerAnimation.animationSpeed = 0.275;
        this.runnerAnimation.loop = false;
        this.runnerAnimation.visible = true
        this.runnerAnimation.zIndex = 3;

        curtainsContainer.addChild(this.runnerAnimation);
    }

    async startFreeSpin() {
        await waitUntilIsTrue('GAME_START');
        disableEnableButtons(false);
        footer.showHideDarkSpinButton(true, 'startFreeSpin');
        await this.enable();
        oAL.sound.background.pause();
        freeSpinBackground.play();
        spinH.activeSpinButton();
    }

    async stopFreeSpin() {
        footer.showHideDarkSpinButton(true, 'stopFreeSpin');
        disableEnableButtons(false);
        setSettings.REQUEST_END = false;
        await waitUntilIsDone('REQUEST_END');
        await this.showDialogBox('reward', "FREE GAMES ENDED \n" + user.currencySymbol + parseFloat(settings.FS_TOTAL_WIN).toFixed(2) + '\nHAS BEEN ADDED TO YOUR BALANCE.');
        //modalDialog.show('We hope that you have enjoyed your complementary game play!<br>You have won <span style="color: #FFFF00">' + user.currencySymbol + parseFloat(settings.FS_TOTAL_WIN).toFixed(2) + '</span> which has been added to your balance.', 'reward')
        await sleep(1000);

        /*setSettings.PROMO_CHIPS += parseFloat(settings.FREE_SPIN_TOTAL_WIN).toFixed(2)*/
        setSettings.CAN_BET = true;

        /*footer.spinButtonSprite.visible = true;*/
        await waitUntilIsDone("MODAL_DIALOG_STATUS")
        await this.disabled();

        freeSpinBackground.pause();
        oAL.sound.background.play();
    }

    async enable() {
        this.totalWinAmount.x = this.totalWinAmount.parent.width / 2 - this.totalWinAmount.width / 2;
        this.totalWinAmount.y = this.totalWinAmount.parent.height - this.totalWinAmount.height -settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
        let self = this;
        return new Promise(async (resolve, reject) => {
            setSettings.FS_ENABLE_STATUS = true;
            let freeSpinTriggerSound = freeSpinTrigger.play();
            freeSpinTriggerSound.on('end', async function () {
                await self.openCloseCurtains(true);
                self.switchBackground(true);
                self.switchPanel(true);
                self.switchCurrentReelPanel();
                await sleep(500);
                await self.openCloseCurtains(false);
                //await self.showDialogBox(true);
                //await self.showDialogModal(true);
                self.curtainsStatus = true;
                return resolve();
            })
        })
    }

    async disabled() {
        let self = this;
        return new Promise(async (resolve, reject) => {
            setSettings.FS_ENABLE_STATUS = false;
            self.hideAndResetModal()
            await sleep(500)
            await self.openCloseCurtains(true, 'close');
            self.switchBackground(false);
            self.switchPanel(false);
            self.switchCurrentReelPanel(false);
            await sleep(500)
            disableEnableButtons(true);
            footer.showHideDarkSpinButton(false, 'disabled');
            await self.openCloseCurtains(false, 'close');
            await sleep(500);
            self.curtainsStatus = false;
            return resolve();
        })
    }

    async openCloseCurtains(courtainStatus, fsStatus = 'open') {
        let self = this;
        return new Promise(async (resolve, reject) => {

                footer.hideElementsForCurtains(false, !settings.DEVICE.includes('D'));
                if(!settings.DEVICE.includes('D') && footer.mobileBetSet.visible === true)
                    footer.closeCoinsAction();

                if(!settings.DEVICE.includes('D') && footer.mobileInfoBar.visible === true)
                    footer.closeMenuAction();

                let cStart = curtainStart.play();
                cStart.on('end', function () {
                    curtain.loop = true;
                    curtain.play();
                });
                this.runnerAnimation.gotoAndPlay(0);

                if (!courtainStatus && fsStatus === 'open') {
                    await self.showDialogBox();
                    return resolve();
                }
                this.runnerAnimation.onFrameChange = async function (a) {
                    let f = 8 / freeS.runnerAnimation.totalFrames;
                    if (courtainStatus) {
                        freeS.runnerAnimation.zIndex = settings.DEVICE.includes('D') ? 7 : 10;
                        game.reelOverlay.alpha += f / 10;
                    } else if (!courtainStatus && fsStatus === 'close')
                        game.reelOverlay.alpha = 0;
                };
                this.runnerAnimation.onComplete = function () {
                    footer.hideElementsForCurtains(true, !settings.DEVICE.includes('D'));
                    curtain.stop();
                    curtainClose.play();
                    setSettings.STOP_PAYLINE_ANIMATION = true;

                    self.runnerAnimation.textures.reverse();

                    if (!courtainStatus)
                        freeS.runnerAnimation.zIndex = 3;

                    if (!courtainStatus && fsStatus === 'open') {
                        freeSpinStart.play();
                        //await self.showDialogBox();
                    }

                    if (fsStatus === 'close')
                        footer.showHideSpinButton(true);

                    return resolve();
                };
            }
        )
    }

    switchBackground(freeSpinStatus) {

        oAL.backgroundImg.texture = freeSpinStatus ? this.spritesheet.background.textures['freespins_bg.png'] : oAL.backgroundElements['background'];
        //oAL.backgroundImg.x = oAL.backgroundImg.parent.width / 2 - oAL.backgroundImg.width / 2;

        oAL.titleImg.texture = freeSpinStatus ? this.spritesheet.background.textures['freespins_title.png'] : oAL.backgroundElements['title'];
        oAL.titleImg.x = (GAME_WIDTH / 2) - (oAL.titleImg.width / 2);
        game.reelBackground.texture = freeSpinStatus ? this.spritesheet.background.textures['reel_bg_02.png'] : ''
        //game.reelBackground.x = game.reelBackground.parent.width / 2 - game.reelBackground.width / 2;
        game.reelBackground.x = freeS.reelContainer.x
        game.reelBackground.y = 100//REEL_CONTAINER_MARGIN_TOP
    }


    switchPanel(freeSpinStatus) {
        if (freeSpinStatus) {
            this.freeSpinContainer.visible = false;
            footer.footerBgTexture.texture = this.freeSpinPanel['footer_panel_bg'];
            //oAL.panel = this.freeSpinPanel;

            footer.linesBetAmount.text = settings.GAME_PLAY_MODE === 'demo' ? '99999' : settings.FS_COUNT;
            let linesBet = footerContainer.getChildByName('lines-bet-image').getChildByName('lines-bet-text')
            if(settings.DEVICE.includes('D')) {
                linesBet.text = 'FREE SPINS REMAINING';
                linesBet.style = {fill: '0xffffff', wordWrap: true, fontSize: 11, fontWeight: 'bold'};
                /*footer.linesBet.x = 204;
                footer.linesBet.y = 6;*/
                footer.linesBetAmount.style = {fill: '0xffffff', wordWrap: true, fontSize: 14};
                /*footer.linesBetAmount.x = 232;*/
                footer.betImage.visible = false;
                footer.totalBalanceImage.visible = true;
                footer.plusButton.visible = false;
                footer.minusButton.visible = false;
                footer.winImage.x = footer.winImage.x - footer.totalBalanceImage.width;
                footer.winImage.getChildByName('win-text').style = {font: "Arial", fill: "0xffffff", fontWeight: "bold", fontSize: 11};
                footer.winAmount.style = {font: "10px Arial", fill: "0xffffff", fontSize: 18};
                footer.paylineWin.style = {fill: "0xfcc603", font: "Arial", fontSize: 12};
                this.totalWin.zIndex = 9;
                this.totalWinAmount.zIndex = 9;
                this.totalWin.visible = true;
                this.totalWinAmount.visible = true;
            } else {
                linesBet.text = settings.GAME_PLAY_MODE === 'demo' ? 'LINES' : 'SPINS LEFT';
                linesBet.style = {font: "10px Arial", fill: '0xffffff', fontSize: 20};
                //footer.linesBet.x = settings.GAME_PLAY_MODE === 'demo' ? 85 : 55;
                /*footer.linesBet.y = 4;*/
                /*footer.linesBetAmount.x = settings.GAME_PLAY_MODE === 'demo' ? 85 : 105;*/
                footer.balanceImage.getChildByName('balance-text').text = 'BALANCE';
                footer.bonusImage.getChildByName('bonus-text').text = settings.GAME_PLAY_MODE === 'demo' ? 'DEMO MODE' : 'BONUS';

                footer.bonusImage.getChildByName('bonus-text').x =  (footer.bonusImage.width / 2) - (footer.bonusImage.getChildByName('bonus-text').width / 2);
                footer.betImage.visible = settings.GAME_PLAY_MODE === 'demo';
                footer.totalBalanceImage.visible = true;
                footer.winImage.x = footer.winImage.x - footer.totalBalanceImage.width;
                /*footer.winImage.x = settings.GAME_PLAY_MODE === 'demo' ? 490 : 270;*/
                this.totalWin.visible = settings.GAME_PLAY_MODE !== 'demo';
                this.totalWin.style = {font: "10px Arial", fill: "0xff8080", fontSize: 20};
                this.totalWinAmount.style = {font: "10px Arial", fill: 0xffffff, fontSize: 20};
                this.totalWinAmount.visible = settings.GAME_PLAY_MODE !== 'demo';
                this.totalWin.zIndex = settings.GAME_PLAY_MODE !== 'demo' ? 9 : 0;
                this.totalWinAmount.zIndex = settings.GAME_PLAY_MODE !== 'demo' ? 9 : 0;
                //this.totalWin.x = 830;
                //this.totalWinAmount.x = 860;
            }

            linesBet.text = 'FREE SPINS REMAINING';
            linesBet.style = {fill: '0xffffff', wordWrap: true, fontSize: 11, fontWeight: 'bold'};
            linesBet.x = footer.linesBetImage.width / 2 - linesBet.width / 2;


            footer.linesBetAmount.text = settings.FS_COUNT;
            //footer.linesBetAmount.style = {fill: '0xffffff', wordWrap: true, fontSize: 14};
            footer.linesBetAmount.x = footer.linesBetImage.width / 2 - footer.linesBetAmount.width / 2;

            footer.betImage.visible = false;
            footer.plusButton.visible = false;
            footer.minusButton.visible = false;

            /*footer.winImage.x = 180;*/
            /*footer.winImage.getChildByName('win-text').style = {
                font: "Arial",
                fill: "0xffffff",
                fontWeight: "bold",
                fontSize: 11
            };*/

            /*footer.winAmount.style = {font: "10px Arial", fill: "0xffffff", fontSize: 18};

            footer.paylineWin.style = {fill: "0xfcc603", font: "Arial", fontSize: 12};*/

            this.totalWin.visible = true;
            this.totalWinAmount.visible = true;

        } else {
            footer.retrieveFooterValues(!settings.DEVICE.includes('D'));
        }
    }

    showDialogBox(flag = 'initial', awardedText = "\nIT’S SHOW TIME.  \nFREE GAMES AWARDED") {
        let self = this;
        this.dialogContainer.visible = true;
        self.lightFlashing.play();
        this.dialogContainer.y = 0;
        this.dialogBoxText.text = awardedText;
        self.dialogBoxText.style.fontSize = 25
        if (flag !== 'initial') {
            game.reelOverlay.alpha = 0.4;
            self.dialogBoxText.style.fontSize = 18
        }

        this.dialogBoxText.x = this.dialogContainer.width / 2 - this.dialogBoxText.width / 2;
        return new Promise((resolve, reject) => {
            TweenMax.to(self.dialogBoxStageLight, 0.5, {
                boxShadow: "0px 0px 20px 5px rgba(255,255,255)",
                repeat: 8,
                alpha: 1,
                yoyo: true,
                onComplete: async function () {
                    await sleep(1000);
                    TweenMax.to(self.dialogContainer, 1, {
                        onComplete: async function () {
                            await self.showInfoModal(true);
                            self.awardedTextControl();
                            game.reelOverlay.alpha = 0;
                            return resolve();
                        },
                        onUpdate: function () {
                            let f = 8 / freeS.runnerAnimation.totalFrames;

                            if (game.reelOverlay.alpha > 0)
                                game.reelOverlay.alpha -= f / 10
                        },
                        onStart: async function (e) {
                            settings.SWITCH_LINES_BET = true;
                            self.lightFlashing.stop();
                            self.dialogBoxStageLight.alpha = 0;
                        },
                        y: -self.dialogContainer.height,
                        ease: Bounce.easeOut,
                    });
                }
            })
        })
    }

    async hideAndResetModal() {
        let self = this;
        return new Promise((resolve, reject) => {
            TweenMax.to(self.freeSpinContainer, 0.6, {
                onComplete: function (e) {
                    self.awardedText.text = "";
                    self.awardedText.x = (self.freeSpinContainer.width / 2) - self.awardedText.width / 2;
                    self.freeSpinContainer.x = GAME_WIDTH / 2 - self.freeSpinContainer.width / 2;

                    return resolve();
                },
                y: -self.freeSpinContainer.height, ease: Power0.easeOut,
            });
        })
    }

    async showInfoModal(showInfoModal) {
        let self = this;
        return new Promise((resolve, reject) => {
            TweenMax.to(this.freeSpinContainer, 0.6, {
                onStart: function () {
                    console.log('start')
                },
                onComplete: function (e) {
                    console.log('complete')
                    return resolve();
                },
                y: showInfoModal ? 0 : -self.freeSpinContainer.height, ease: Bounce.easeOut,
            });
        })
    }

    /**
     * USE WITH DEFAULT SYMBOL
     * */
    switchCurrentReelPanelOld(status = true) {
        for (let i = 0; i < game.reels.length; i++) {
            const r = game.reels[i];
            // Update symbol texture on current reel.
            for (let j = 0; j < r.symbols.length; j++) {
                const s = r.symbols[j];
                let symbolSource = settings.FS_ENABLE_STATUS ? freeS.slotTextures : game.slotTextures;
                s.texture = symbolSource[s.symbolElement];
            }
        }
    }

    /**
     * USE WITH DIFFERENT SYMBOL
     * */
    switchCurrentReelPanel(status = true) {
        if (status) {
            game.reelContainer.visible = false;
            freeS.reelContainer.visible = true;
            oAL.reelDividerImg.visible = false
        } else {
            /*GO TO NORMAL SPIN*/
            game.reelContainer.visible = true;
            freeS.reelContainer.visible = false;
            oAL.reelDividerImg.visible = true
        }
    }

    awardedTextControl(startGame = false) {
        if (settings.FS_STATUS) {
            this.awardedText.text = "FREE SPIN \nTOTAL WIN: " + user.currencySymbol + (parseFloat(settings.FS_TOTAL_WIN)).toFixed(2) + "\n\n" + (settings.FS_COUNT - startGame) + " plays remaining";
            this.awardedText.x = (this.freeSpinContainer.width / 2) - this.awardedText.width / 2;
            this.totalWinAmount.text = user.currencySymbol + (parseFloat(settings.FS_TOTAL_WIN)).toFixed(2);

            if(settings.SWITCH_LINES_BET /*&& settings.GAME_PLAY_MODE !== 'demo'*/)
                footer.linesBetAmount.text = settings.FS_COUNT;
        }
    }

    static lerp(a1, a2, t) {

        return a1 * (1 - t) + a2 * t;
    }

    backout(amount) {
        return (t) => (--t * t * ((amount + 1) * t + amount) + 1);
    }
}
