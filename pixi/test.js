class Test {
    constructor() {
        this.lines = [];
        this.stop = false;
        this.activePaylineIndicators = [];
        this.winnLineIndex = 0;
    }

    destroy() {
        this.stop = true;
    }

    async showWinningSymbolsNew() {
        if (this.stop) return;
        if (settings.GAME_RUNNING) return;

        gameCompleted.addRemoveBlurAllSymbols(true);

        let gamePayline = game.paylines[this.winnLineIndex];
        let paylineText = '';

        if (typeof gamePayline === "undefined") return;

        if (typeof gamePayline !== "undefined")
            paylineText = "LINE " + gamePayline.payline + " WINS " + user.currencySymbol + formatMoney(gamePayline.amount.toFixed(2));

        if (typeof gamePayline !== "undefined" && gamePayline.payline === -1)
            paylineText = "SCATTER PAYS " + user.currencySymbol + game.paylines[this.winnLineIndex].amount.toFixed(2);

        if (game.paylines[this.winnLineIndex].amount.toFixed(2) > 0) {
            gameCompleted.winText.text = user.currencySymbol + game.paylines[this.winnLineIndex].amount.toFixed(2);
            gameCompleted.winText.x = (GAME_WIDTH / 2) - (gameCompleted.winText.width / 2);
            gameCompleted.winText.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - (gameCompleted.winText.height / 2));
            //oAL.winLine.visible = true;
        }
        footer.paylineWin.text = paylineText;
        if ((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)) {
            //footer.paylineWin.x = (footer.paylineWin.parent.width / 2) - (footer.paylineWin.width / 2);
            footer.paylineWin.y = footer.paylineWin.parent.height - footer.paylineWin.height;
            if(settings.SWITCH_LINES_BET && settings.DEVICE.includes('D')) {
                footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2;
            } else if(settings.SWITCH_LINES_BET && !settings.DEVICE.includes('D')) {
                footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2;
            } else {
                footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2;
            }
        } else {
            //footer.paylineWin.x = (footer.paylineWin.parent.width / 2) - (footer.paylineWin.width / 2);
            footer.paylineWin.y = footer.paylineWin.parent.height / 2 - footer.paylineWin.height;
            if(settings.SWITCH_LINES_BET && settings.DEVICE.includes('D')) {
                footer.paylineWin.y = footer.paylineWin.parent.height / 2 - footer.paylineWin.height;
            } else if(settings.SWITCH_LINES_BET && !settings.DEVICE.includes('D')) {
                footer.paylineWin.y = footer.paylineWin.parent.height / 2 - footer.paylineWin.height;
            }
            else {
                footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2
            }
        }

        let symbolValue;
        let symbolElement;

        Test.hideAllChildFromFakeContainer();
        /*Add animations*/
        lineWin.play();
        if (/*gameCompleted.firstLoop && */!settings.STOP_PAYLINE_ANIMATION) {
            for (let j = 0; j < game.paylines[this.winnLineIndex].win_symbols.length; j++) {
                let paylineContainer = game.fakeReelContainer.getChildByName(game.paylines[this.winnLineIndex].payline);

                if (spinH.gameType.name === 'free-spin' && [1, 2, 3].includes(game.paylines[this.winnLineIndex].win_symbols[j].col)) {

                    let symbol = paylineContainer.getChildByName(j + '-' + game.paylines[this.winnLineIndex].win_symbols[j].value);
                    if (symbol === null)
                        continue;

                    freeS.reelContainer.children[1].children[1].visible = false;

                    /*if (symbol.getChildByName('big-frame-animation') !== null)
                        symbol.getChildByName('big-frame-animation').visible = false
                    if (symbol.getChildByName('big-symbol') !== null)
                        symbol.getChildByName('big-symbol').visible = false*/

                    if (paylineContainer.getChildByName('big') !== null)
                        paylineContainer.getChildByName('big').visible = false
                        /*TweenMax.to(symbol, 0.2, {*/

                        /*});*/
                } else {

                    /*symbolValue = game.paylines[this.winnLineIndex].win_symbols[j].value;

                    symbolElement = game.symbolAnimation[symbolValue];

                    let symbol = paylineContainer.getChildByName(j + '-' + symbolValue);
                    console.log(game.paylines[this.winnLineIndex].payline, j + '-' + symbolValue, symbol.bigSymbolContainer, 'symbol123');

                    let symbolFrame = symbol.getChildByName('frame-animation');
                    if (symbolFrame !== null) {
                        symbolFrame.visible = true;
                        symbolFrame.zIndex = 2;
                    }
                    let symbolAnim;
                    if ([0, 9].includes(symbolValue)) {
                        symbolAnim = symbol.getChildByName('symbolAnim-' + symbolValue + '-h');
                        if (symbolAnim !== null) {
                            symbolAnim.gotoAndPlay(0);
                            symbolAnim.visible = true;
                            symbolAnim.loop = false;
                        }
                    } else {
                        symbolAnim = symbol.getChildByName('symbolAnim-' + symbolValue);
                        if (symbolAnim !== null) {
                            symbolAnim.gotoAndPlay(0);
                            symbolAnim.visible = true;
                            symbolAnim.loop = false;
                        }
                    }*/
                }

                symbolValue = game.paylines[this.winnLineIndex].win_symbols[j].value;

                symbolElement = game.symbolAnimation[symbolValue];

                let symbol = paylineContainer.getChildByName(j + '-' + symbolValue);
                symbol.visible = true;

                let symbolFrame = symbol.getChildByName('frame-animation');
                if (symbolFrame !== null) {
                    symbolFrame.visible = true;
                    symbolFrame.zIndex = 2;
                }
                let symbolAnim;
                if ([0, 9].includes(symbolValue)) {
                    symbolAnim = symbol.getChildByName('symbolAnim-' + symbolValue + '-h');
                    if (symbolAnim !== null) {
                        symbolAnim.gotoAndPlay(0);
                        symbolAnim.visible = true;
                        symbolAnim.loop = false;
                    }
                } else {
                    symbolAnim = symbol.getChildByName('symbolAnim-' + symbolValue);
                    if (symbolAnim !== null) {
                        symbolAnim.gotoAndPlay(0);
                        symbolAnim.visible = true;
                        symbolAnim.loop = false;
                    }
                }
            }
        }

        if (settings.STOP_PAYLINE_ANIMATION)
            return;

        game.fakeReelContainer.getChildByName(game.paylines[this.winnLineIndex].payline).visible = true;

        if (typeof gameCompleted.linesIndicators[game.paylines[this.winnLineIndex].payline] !== "undefined" && !settings.STOP_PAYLINE_ANIMATION)
            gameCompleted.linesIndicators[game.paylines[this.winnLineIndex].payline].visible = true;

        if (game.paylines[this.winnLineIndex].payline !== 0 && !settings.STOP_PAYLINE_ANIMATION) {

            if (this.stop) return;

            this.activePaylineIndicators.push(game.paylines[this.winnLineIndex].payline);

            await sleep(1000);
            gameCompleted.hideActivePayline();
            gameCompleted.hideAllLinesIndicators();

            /*if (game.paylines.length > 0 && typeof game.paylines[this.winnLineIndex] !== "undefined" && game.paylines[this.winnLineIndex].payline > 0) {
                gameCompleted.lines[game.paylines[this.winnLineIndex].payline - 1].visible = false;
            }*/
            console.log('ajunge aici2', game.paylines.length === this.winnLineIndex + 1 , !settings.STOP_PAYLINE_ANIMATION);
            if (game.paylines.length === this.winnLineIndex + 1 && !settings.STOP_PAYLINE_ANIMATION) {
                console.log('ajunge aici21');
                if ((spinH.autospin && !settings.FS_STATUS) || (settings.FS_STATUS && freeS.curtainsStatus) || (settings.FSV_STATUS)) {
                    console.log('ajunge aici22');
                    spinH.winningRound = false;
                    spinH.removeAllAnimatedSprite();
                    gameCompleted.addRemoveBlurAllSymbols(false);
                    spinH.activeSpinButton();
                    return;
                } else {
                    gameCompleted.firstLoop = false;
                    this.winnLineIndex = 0;
                }
            } else {
                this.winnLineIndex++;
            }

            if (!settings.GAME_RUNNING && !settings.STOP_PAYLINE_ANIMATION) {

                spinH.removeAllAnimatedSprite();
                gameCompleted.addRemoveBlurAllSymbols(false);
                if (this.stop) return;

                this.showWinningSymbolsNew();
            }
        } else {
            game.paylines[this.winnLineIndex] = [];
        }
    }

    static hideAllChildFromFakeContainer() {
        for (let i = 0; i < game.fakeReelContainer.children.length; i++) {
            game.fakeReelContainer.children[i].visible = false;
        }
    }
}
