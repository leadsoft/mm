class Footer {
    constructor(app, gameContainer) {

        this.infoBtn;
        this.soundButton;
        this.fastSpinButton;
        this.autoSpinButton;
        this.minusButton;
        this.plusButton;
        this.cImage;
        this.showPreQualifier;
        this.darkSpinButtonSprite;
        this.stopSpinButtonSprite;
        this.linesBet;
        this.spinCounter = 0;
        this.linesBetAmount;
        this.homeBtn;
        this.mobileBetSet;
        this.mobileCoinsEnable;

        this.mobileCoinPlus;
        this.mobileCoinMinus;
        this.mobileInfoBar;
        this.mobileNormalSpin;
        this.coinMenuContainer;
        this.mobileMenu;
        this.mobileMenuBtn;
        this.isMuted = false;
        this.isPressedQuick = false;
        this.isPressedAutospin = false;
        this.gameContainer = gameContainer;
        this.app = app;
        /*footerContainer = new PIXI.Container();
        footerContainer.name = 'footer-container';*/

        footerContainer.height = settings.FOOTER_HEIGHT;
        footerContainer.y = GAME_HEIGHT - settings.FOOTER_HEIGHT;

        this.footerBgTexture = new PIXI.Sprite();
        this.footerBgTexture.name = 'bg-panel'

        this.coinsImage = new PIXI.Sprite();
        this.coinsImage.name = 'coin-image';
        footerContainer.addChild(this.coinsImage);

        this.betImage = new PIXI.Sprite();
        this.betImage.name = 'bet-image';
        this.betImage.zIndex = 1;
        footerContainer.addChild(this.betImage);

        this.linesBetImage = new PIXI.Sprite();
        this.linesBetImage.name = 'lines-bet-image';
        this.linesBetImage.zIndex = 1;
        footerContainer.addChild(this.linesBetImage);

        this.winImage = new PIXI.Sprite();
        this.winImage.name = 'win-image';
        footerContainer.addChild(this.winImage);

        this.balanceImage = new PIXI.Sprite();
        this.balanceImage.zIndex = 1;
        this.balanceImage.name = 'balance-image';
        this.balanceImage.visible = false;
        footerContainer.addChild(this.balanceImage);

        this.totalBalanceImage = new PIXI.Sprite();
        this.totalBalanceImage.zIndex = 1;
        this.totalBalanceImage.name = 'total-balance-image';
        this.totalBalanceImage.visible = false;
        footerContainer.addChild(this.totalBalanceImage);

        let isMobile = !settings.DEVICE.includes('D');

        this.addPreQualification(isMobile);
        this.addQualification(isMobile);

        /*TODO: Need to show just if is real mode*/
        this.bonusImage = new PIXI.Sprite();
        this.bonusImage.name = 'bonus-image';
        this.bonusImage.visible = false;
        footerContainer.addChild(this.bonusImage);

        this.userBalance = new PIXI.Text('', {font: "10px Arial", fill: "0x0000000"});
        this.userBalance.name = 'balance-value';
        this.userBalance.style.fontSize = 17;
        this.balanceImage.addChild(this.userBalance);

        this.userBonusBalance = new PIXI.Text('', {font: "10px Arial", fill: "0x0000000"});
        this.userBonusBalance.name = 'bonus-value';
        this.userBonusBalance.style.fontSize = 17;
        this.bonusImage.addChild(this.userBonusBalance);

        this.paylineWin = new PIXI.Text('', {font: "Arial", fill: "0x66b3ff"});
        this.paylineWin.name = 'payline-win';
        this.paylineWin.style.fontSize = 12;
        this.winImage.addChild(this.paylineWin);

        this.amountPerLine = new PIXI.Text('', {font: 'Arial', fill: "0xffffff"});
        this.amountPerLine.name = 'amount-per-line'
        this.amountPerLine.style.fontSize = 24;

        this.coinsImage.addChild(this.amountPerLine);

        this.winAmount = new PIXI.Text('', {font: "10px Arial", fill: "0x000000", fontWeight: "bold"});
        this.winAmount.name = 'win-amount';
        this.winAmount.style.fontSize = 18;
        this.winImage.addChild(this.winAmount);

        this.betAmount = new PIXI.Text('', {font: "10px Arial", fill: "0xffffff"});
        this.betAmount.name = 'bet-amount';
        this.betAmount.style.fontSize = 18;
        this.betImage.addChild(this.betAmount);

        this.linesBetAmount = new PIXI.Text('9999', {font: "10px Arial", fill: "0x000000"});
        this.linesBetAmount.name = 'lines-bet-amount';
        this.linesBetAmount.style.fontSize = 14;
        this.linesBetImage.addChild(this.linesBetAmount);

        this.spinButtonSprite = new PIXI.Sprite(oAL.panel['spin']);

        this.disableFooter = false;

        this.mobileCoinSelectorContainer = new PIXI.Sprite();
        this.mobileCoinSelectorContainer.alpha = 0;

        this.mobileCoinSelectorContainer.name = "mobile-coin-selector"
    }

    fixTournamentTextPosition() {
        footer.betsToQualify.x = (footer.betsToQualify.parent.width / 2) - (footer.betsToQualify.width / 2);
        footer.betsToQualify.y = (footer.betsToQualify.parent.height / 2) - (footer.betsToQualify.height / 2);
        footer.rank.x = (footer.rank.parent.width / 2) - (footer.rank.width / 2);
        footer.rank.y = (footer.rank.parent.height / 2) - (footer.rank.height / 2);
        footer.timeLeft.x = (footer.timeLeft.parent.width / 2) - (footer.timeLeft.width / 2);
        footer.timeLeft.y = (footer.timeLeft.parent.height / 2) - (footer.timeLeft.height / 2);
        footer.rankBalance.x = (footer.rankBalance.parent.width / 2) - (footer.rankBalance.width / 2);
        footer.rankBalance.y = (footer.rankBalance.parent.height / 2) - (footer.rankBalance.height / 2);
        footer.betsMade.x = (footer.betsMade.parent.width / 2) - (footer.betsMade.width / 2);
        footer.betsMade.y = (footer.betsMade.parent.height / 2) - (footer.betsMade.height / 2);
    }

    setFooterImages() {
        this.coinsImage.texture = oAL.panel['screen_short'];
        /*footerContainer.*/
        this.footerBgTexture.texture = oAL.backgroundElements['footer_panel'];
        this.rankBalanceImage.texture = oAL.panel['footer_panel_bg'];
        this.timeLeftImage.texture = oAL.panel['screen_med'];
        this.rankImage.texture = oAL.panel['screen_short'];
        this.betsToQualifyImage.texture = oAL.panel['footer_panel_bg'];
        this.betsMadeImage.texture = oAL.panel['screen_short'];
        setSettings.MIN_BET = settings.MIN_BET
        setSettings.USER_BALANCE = settings.USER_BALANCE
        setSettings.PROMO_CHIPS = settings.PROMO_CHIPS

        this.mobileCoinSelectorContainer.texture = oAL.panel['mobile_coin_selector_bg'];

        this.mobileCoinSelectorContainer.x = GAME_WIDTH;
        this.mobileCoinSelectorContainer.y = 0;

        this.app.stage.addChild(this.mobileCoinSelectorContainer);
        setFooterContainerTextures()
    }

    setPosition(isMobile) {

        this.coinsImage.x = footerCoordinates.bet_per_line_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.coinsImage.y = footerCoordinates.bet_per_line_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;

        this.mobileCoinSelectorContainer.mobile = true;
        setFooterContainerPosition()
        /*if(isMobile)
            this.fastSpinButton.visible = false;*/
    }

    addPreQualification(isMobile) {
        this.balanceImage.visible = true;
        /*this.balanceImage.x = 358;*/

        this.preQualificationContainer = new PIXI.Container();
        footerContainer.addChild(this.preQualificationContainer);
        this.preQualificationContainer.alpha = 0;
        this.betsToQualifyImage = new PIXI.Sprite();
        this.betsToQualifyImage.name = 'bets-to-qualify-image';
        this.betsToQualifyImage.x = footerCoordinates.bets_to_qualify_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.betsToQualifyImage.y = footerCoordinates.bets_to_qualify_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;

        this.betsMadeImage = new PIXI.Sprite();
        this.betsMadeImage.name = 'bets-made-image';
        this.betsMadeImage.x = footerCoordinates.bets_made_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.betsMadeImage.y = footerCoordinates.bets_made_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;


        this.betsToQualify = new PIXI.Text('', {font: "Arial", fill: "0xffe000"});
        this.betsToQualify.name = 'bets-to-qualify';
        this.betsToQualify.style.fontSize = 18;
        this.betsToQualifyImage.addChild(this.betsToQualify);

        this.betsMade = new PIXI.Text('', {font: "Arial", fill: "0xffe000"});
        this.betsMade.name = 'bets-made';
        this.betsMade.style.fontSize = 18;
        this.betsMadeImage.addChild(this.betsMade);

        this.preQualificationContainer.addChild(this.betsToQualifyImage);
        this.preQualificationContainer.addChild(this.betsMadeImage);
    }

    addQualification(isMobile) {
        this.balanceImage.visible = true;
        /*this.balanceImage.x = 358;*/
        this.qualificationContainer = new PIXI.Container();
        this.qualificationContainer.alpha = 0;
        footerContainer.addChild(this.qualificationContainer);

        this.rankBalanceImage = new PIXI.Sprite();
        this.rankBalanceImage.name = 'rank-balance-image';
        this.rankBalanceImage.x = footerCoordinates.rank_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.rankBalanceImage.y = footerCoordinates.rank_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;

        this.timeLeftImage = new PIXI.Sprite();
        this.timeLeftImage.name = 'time-left-image';
        this.timeLeftImage.x = footerCoordinates.time_left_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.timeLeftImage.y = footerCoordinates.time_left_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;


        this.rankImage = new PIXI.Sprite();
        this.rankImage.name = 'rank-image';
        this.rankImage.x = footerCoordinates.rank_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.rankImage.y = footerCoordinates.rank_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;


        this.timeLeft = new PIXI.Text('', {font: "Arial", fill: "0xffe000"});
        this.timeLeft.name = 'time-left';
        this.timeLeft.style.fontSize = 18;
        this.timeLeftImage.addChild(this.timeLeft);

        this.rankBalance = new PIXI.Text('', {font: "Arial", fill: "0xffe000"});
        this.timeLeft.name = 'rank-balance';
        this.rankBalance.style.fontSize = 18;
        this.rankBalanceImage.addChild(this.rankBalance);

        this.rank = new PIXI.Text('', {font: "Arial", fill: "0xffe000"});
        this.rank.name = 'rank';
        this.rank.style.fontSize = 18;
        this.rankImage.addChild(this.rank);

        this.qualificationContainer.addChild(this.rankBalanceImage);
        this.qualificationContainer.addChild(this.timeLeftImage);
        this.qualificationContainer.addChild(this.rankImage);
    }

    showRealPanel() {
        this.balanceImage.visible = 'true';
        this.bonusImage.visible = 'true';
    }

    showDemoPanel() {
        this.balanceImage.visible = 'true';
    }

    show(isMobile) {
        //footerContainer.zIndex = 2;
        this.footerBgTexture.x = GAME_WIDTH / 2 - this.footerBgTexture.texture.width / 2;
        if (!isMobile)
            footerContainer.addChild(this.footerBgTexture);
        /*this.app.stage.addChild(footerContainer);*/


        this.addmobileMenu(isMobile);
        this.addMobileInfoBar(isMobile);
        this.addInfoButton(isMobile);
        this.addSpinButton(isMobile);
        this.addAutoSpinButton(isMobile);
        this.addFastSpinButton(isMobile);
        this.addHomeButton(isMobile);
        this.addSoundButton(isMobile);
        this.addCoinButtons(isMobile);

        addText(isMobile);
        this.addDarkSpinButton(isMobile);
        this.addStopSpinButton(isMobile);
        this.addMobileBetIcons(isMobile);
        this.addMobileBetSet(isMobile);
        this.addCoinMenu(isMobile);
        this.addCoinsPlus(isMobile);
        this.addCoinsMinus(isMobile);

        //this.addMobileNormalSpin(isMobile);

        this.adjustFooter(isMobile);
        setActionContainerTextures();

    }

    addMobileBetIcons(isMobile) {
        if (isMobile) {
            this.mobileCoinsEnable = new PIXI.Sprite(oAL.panel['coins_enable_n']);
            this.mobileCoinsEnable.anchor.x = 0;
            this.mobileCoinsEnable.anchor.y = 0;
            this.mobileCoinsEnable.x = 985;
            this.mobileCoinsEnable.y = 154;
            this.mobileCoinsEnable.zIndex = 6;
            this.mobileCoinsEnable.name = 'mobile-coins-enable';
            this.mobileCoinsEnable.interactive = true;
            this.mobileCoinsEnable.buttonMode = true;
            this.mobileCoinsEnable.visible = false;

            this.app.stage.addChild(this.mobileCoinsEnable);

            this.mobileCoinsEnable.on('pointerover', () => {
                this.mobileCoinsEnable.texture = oAL.panel['mobile_coins_enable_h'];
            });
            this.mobileCoinsEnable.on('pointerout', () => {
                this.mobileCoinsEnable.texture = oAL.panel['coins_enable_n'];
            });
            this.mobileCoinsEnable.on('pointerdown', () => {
                this.mobileCoinsEnable.visible = false;
                this.mobileCoinsClose.visible = true;
                TweenMax.to(this.mobileBetSet, 1, {
                    x: 700,
                });
                TweenMax.to(this.mobileCoinPlus, 1, {
                    x: 718,
                });
                TweenMax.to(this.mobileCoinMinus, 1, {
                    x: 883,
                });
                TweenMax.to(this.amountPerLine, 1, {
                    x: 803,
                });
            });
        }
    }

    closeCoinsAction() {
        /*footer.mobileCoinsClose.visible = false;*/
        /*footer.mobileCoinsEnable.visible = true;
        TweenMax.to(footer.mobileBetSet, 1, {
            x: 1070,
        });
        TweenMax.to(footer.mobileCoinPlus, 1, {
            x: 1070,
        });
        TweenMax.to(footer.mobileCoinMinus, 1, {
            x: 1070,
        });
        TweenMax.to(footer.amountPerLine, 1, {
            x: 1070,
        });*/
    }

    closeMenuAction() {

    }

    addMobileInfoBar(isMobile) {
        if (isMobile) {
            this.mobileInfoBar = new PIXI.Sprite(/*oAL.panel['menu_mask_2']*/);
            this.mobileInfoBar.name = 'mobile-menu-bar';
            this.mobileInfoBar.x = actionContainer.width;
            this.mobileInfoBar.visible = false;
            this.mobileMenu.addChild(this.mobileInfoBar);
        }
    }

    addmobileMenu(isMobile) {
        let self = this;
        if (isMobile) {
            this.mobileMenu = new PIXI.Container();
            this.mobileMenu.status = false;
            this.mobileMenu.sortableChildren = true
            this.mobileMenu.zIndex = 6;
            this.mobileMenu.name = 'mobile-open-menu';
            this.mobileMenu.interactive = true;
            this.mobileMenu.buttonMode = true;

            actionContainer.addChild(this.mobileMenu);

            this.mobileMenuBtn = new PIXI.Sprite();
            this.mobileMenuBtn.name = 'mobile-menu-btn';
            this.mobileMenuBtn.zIndex = 9;
            this.mobileMenuBtn.interactive = true;
            this.mobileMenu.addChild(this.mobileMenuBtn);

            this.mobileMenuBtn.on('pointerover', () => menuActions.pointerover());
            this.mobileMenuBtn.on('pointerout', () => menuActions.pointerout());
            this.mobileMenuBtn.on('pointerdown', () => menuActions.pointerdown());
        }
    }

    addMobileBetSet(isMobile) {
        if (isMobile) {
            this.mobileBetSet = new PIXI.Sprite(oAL.panel['mobile_bet_set']);
            this.mobileBetSet.anchor.x = 0;
            this.mobileBetSet.anchor.y = 0;
            this.mobileBetSet.x = 700;
            this.mobileBetSet.y = 150;
            this.mobileBetSet.name = 'mobile-bet-set';
            this.mobileBetSet.zIndex = 5;
            this.app.stage.addChild(this.mobileBetSet);
        }
    }

    showHideSpinButton(status) {
        if (status) {
            this.spinButtonSprite.visible = true;
            this.stopSpinButtonSprite.visible = false;
            this.darkSpinButtonSprite.visible = false;
        } else {
            this.spinButtonSprite.visible = false;
        }
    }

    showHideStopSpinButton(status) {
        if (status) {
            this.stopSpinButtonSprite.visible = true;
            this.spinButtonSprite.visible = false;
            this.darkSpinButtonSprite.visible = false;
        } else {
            this.stopSpinButtonSprite.visible = false;

            if (freeS.curtainsStatus && !settings.FS_STATUS) {
                this.darkSpinButtonSprite.visible = true;
            } else {
                this.spinButtonSprite.visible = true;
            }
        }
    }

    showHideDarkSpinButton(status, from) {
        console.log(from, status, 'from-status')
        if (status) {
            this.darkSpinButtonSprite.visible = true;
            this.spinButtonSprite.visible = false;
            this.stopSpinButtonSprite.visible = false;

        } else {
            this.darkSpinButtonSprite.visible = false;
        }
    }

    addDarkSpinButton(isMobile) {
        this.darkSpinButtonSprite = new PIXI.Sprite();
        this.darkSpinButtonSprite.buttonMode = true;
        this.darkSpinButtonSprite.interactive = false;
        this.darkSpinButtonSprite.visible = false;
        this.darkSpinButtonSprite.zIndex = 8;

        actionContainer.addChild(this.darkSpinButtonSprite);
    }

    addStopSpinButton(isMobile) {
        let self = this;
        this.stopSpinButtonSprite = new PIXI.Sprite();
        let stopSpinButton = this.stopSpinButtonSprite;
        this.stopSpinButtonSprite.buttonMode = true;
        this.stopSpinButtonSprite.interactive = true;
        this.stopSpinButtonSprite.visible = false;

        this.stopSpinButtonSprite.on('pointerdown', stopSpin);

        stopSpinButton.on('pointerover', function () {
            self.stopSpinButtonSprite.texture = oAL.panel['stop_spin_hover'];
        });
        stopSpinButton.on('pointerout', function () {
            self.stopSpinButtonSprite.texture = oAL.panel['stop_spin'];
        });

        this.stopSpinButtonSprite.zIndex = 8;
        actionContainer.addChild(this.stopSpinButtonSprite);

        function stopSpin() {
            setSettings.STOP_REEL = true;
        }
    }

    addSpinButton(isMobile) {
        let self = this;
        let spinButton = this.spinButtonSprite;
        /*self.spinButtonSprite.texture = oAL.panel['spin'];*/
        spinButton.buttonMode = true;

        /* spinButton.x = (function () {
             let position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
             if (isMobile)
                 position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

             return position;
         }());
         spinButton.y = (function () {
             let position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
             if (isMobile)
                 position = GAME_HEIGHT / 2 - spinButton.height / 2//footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

             return position;
         }());*/

        // make the button interactive...
        spinButton.interactive = true;
        spinButton.buttonMode = true;
        if (game.autospin) {
            spinButton.enabled = false;
        }
        spinButton.name = 'spin-btn'
        spinButton.on('pointerover', spinActions.pointerover);
        spinButton.on('pointerout', spinActions.pointerout);

        spinButton.on('pointerdown', spinActions.pointerdown);
        spinButton.zIndex = 8;
        //this.app.stage.addChild(spinButton);
        actionContainer.addChild(spinButton);

        async function spin() {
            if (self.disableFooter) {
                return;
            }

            if (sound)
                button.play();
            /*button.play()*/
            setSettings.ACTIVITY = 8;
            //game.startPlay();

            if (isMobile) {
                self.switchMobileCoinSelector(true)
            }

            spinH.activeSpinButton();
        }
    }

    addAutoSpinButton(isMobile) {
        this.autoSpinButton = new PIXI.Sprite(/*oAL.panel['autoplay']*/);
        this.autoSpinButton.buttonMode = true;
        //autoSpinButton.anchor.set(0.5);

        this.autoSpinButton.name = 'auto-spin-btn';
        // make the button interactive...
        this.autoSpinButton.interactive = true;
        this.autoSpinButton.buttonMode = true;

        this.autoSpinButton.on('pointerover', autoSpinActions.pointerover);
        this.autoSpinButton.on('pointerout', autoSpinActions.pointerout);
        this.autoSpinButton.on('pointerdown', autoSpinActions.pointerdown);
        /*footerContainer.addChild(autoSpinButton);*/
        this.autoSpinButton.zIndex = 8;
        actionContainer.addChild(this.autoSpinButton);
    }

    addFastSpinButton(isMobile) {
        this.fastSpinButton = new PIXI.Sprite()
        this.fastSpinButton.buttonMode = true;
        this.fastSpinButton.name = 'fast-spin';

        // make the button interactive...
        this.fastSpinButton.interactive = true;
        this.fastSpinButton.buttonMode = true;

        this.fastSpinButton.on('pointerover', fastSpinActions.pointerover);
        this.fastSpinButton.on('pointerout',fastSpinActions.pointerout);

        this.fastSpinButton.on('pointerdown', fastSpinActions.pointerdown);
        /*footerContainer.addChild(fastSpinButton);*/
        this.fastSpinButton.zIndex = 8;
        if (isMobile)
            this.mobileInfoBar.addChild(this.fastSpinButton);
        else
            actionContainer.addChild(this.fastSpinButton);
    }

    addHomeButton(isMobile) {
        let self = this;
        this.homeBtn = new PIXI.Sprite(/*oAL.panel['home_n']*/);
        this.homeBtn.buttonMode = true;
        this.homeBtn.name = 'home-btn';

        // make the button interactive...
        this.homeBtn.interactive = true;
        this.homeBtn.buttonMode = true;
        this.homeBtn.visible = isMobile;

        this.homeBtn.on('pointerdown', function () {
            setSettings.ACTIVITY = 8;

            window.location.href = settings.HOME_LINK;
            //location.reload()
        });

        this.homeBtn.on('pointerover', function () {
            this.texture = oAL.panel['home_h'];
        });

        this.homeBtn.on('pointerout', function () {
            this.texture = oAL.panel['home_n'];
        });

        this.homeBtn.zIndex = 8;
        actionContainer.addChild(this.homeBtn);
    }

    addInfoButton(isMobile) {

        this.infoBtn = new PIXI.Sprite(oAL.panel['info']);
        this.infoBtn.buttonMode = true;
        this.infoBtn.name = 'info-btn';

        // make the button interactive...
        this.infoBtn.interactive = true;
        this.infoBtn.buttonMode = true;

        let self = this;

        this.infoBtn.on('pointerover', function () {
            self.infoBtn.texture = oAL.panel['info_hover'];
        });
        this.infoBtn.on('pointerout', function () {
            self.infoBtn.texture = oAL.panel['info'];
        });

        this.infoBtn.on('pointerdown', info);
        this.infoBtn.zIndex = 8;
        if (isMobile)
            this.mobileInfoBar.addChild(this.infoBtn);
        else
            actionContainer.addChild(this.infoBtn);

        function info() {
            setSettings.ACTIVITY = 8;
            infoModal.showInfo();
            if (self.disableFooter) {
                return;
            }

            if (sound)
                infoButton.play()
        }
    }

    addSoundButton(isMobile) {
        this.soundButton = new PIXI.Sprite(oAL.panel['sound']);
        this.soundButton.buttonMode = true;
        this.soundButton.name = 'sound-btn';

        // make the button interactive...
        this.soundButton.interactive = true;
        this.soundButton.buttonMode = true;
        let self = this;
        this.soundButton.zIndex = 8;

        if (isMobile)
            this.mobileInfoBar.addChild(this.soundButton);
        else
            actionContainer.addChild(this.soundButton);
        loadJSON(function (response) {
            let data = JSON.parse(response);
            if (!data.soundStatus) {
                self.soundButton.texture = oAL.panel['mute_sound'];
                self.isMuted = true;
            }
        });

        this.soundButton.on('pointerover', function () {
            if (self.isMuted) {
                self.soundButton.texture = oAL.panel['mute_sound_hover'];
            } else {
                self.soundButton.texture = oAL.panel['sound_hover'];
            }

        });
        this.soundButton.on('pointerout', function () {
            if (self.isMuted) {
                self.soundButton.texture = oAL.panel['mute_sound'];
            } else {
                self.soundButton.texture = oAL.panel['sound'];
            }

        });

        this.soundButton.on('pointerdown', function () {
            if (self.disableFooter) {
                return;
            }
            setSettings.ACTIVITY = 8;


            self.isMuted = PIXI.sound.toggleMuteAll();

            if (self.isMuted) {
                self.soundButton.texture = oAL.panel['mute_sound'];
            } else {
                self.soundButton.texture = oAL.panel['sound'];
            }
        });

    }

    addCoinButtons(isMobile) {

        this.plusButton = new PIXI.Sprite(oAL.panel['plus_coin']);
        this.plusButton.buttonMode = true;
        //this.plusButton.x = this.coinsImage.width - this.plusButton.width + 30;
        this.plusButton.x = -12;
        this.plusButton.y = -29;
        this.plusButton.name = 'plus-button';

        // make the button interactive...
        this.plusButton.interactive = true;
        this.plusButton.buttonMode = true;

        this.minusButton = new PIXI.Sprite(oAL.panel[settings.MIN_BET > 0.01 ? 'minus_coin' : 'disable_minus_coin']);
        this.minusButton.buttonMode = true;
        this.minusButton.x = 89;
        this.minusButton.y = -29;
        this.minusButton.name = 'minus-button';

        // make the button interactive...
        this.minusButton.interactive = true;
        this.minusButton.buttonMode = true;

        this.amountPerLine.visible = false;

        this.minusButton.on('pointerover', function () {
            if (settings.MIN_BET !== 0.01) {
                self.minusButton.texture = oAL.panel['minus_coin_hover'];
            }
        });
        this.minusButton.on('pointerout', function () {
            if (settings.MIN_BET === 0.01) {
                self.minusButton.texture = oAL.panel['disable_minus_coin'];
                button.stop()
            } else {
                self.minusButton.texture = oAL.panel['minus_coin'];
            }

        });

        this.minusButton.on('pointerdown', minus);
        this.coinsImage.addChild(this.minusButton);


        this.plusButton.on('pointerover', function () {
            if (settings.MIN_BET !== 10) {
                self.plusButton.texture = oAL.panel['plus_coin_hover'];
            }
        });
        this.plusButton.on('pointerout', function () {
            if (settings.MIN_BET === 10) {
                self.plusButton.texture = oAL.panel['disable_plus_coin'];
                button.stop()
            } else {
                self.plusButton.texture = oAL.panel['plus_coin'];
            }

        });

        this.plusButton.on('pointerdown', plus);
        this.coinsImage.addChild(this.plusButton);
        let self = this;

        if (isMobile) {
            self.mobileCoinSelectorContainer.addChild(this.amountPerLine)
            footer.amountPerLine.x = (footer.amountPerLine.parent.width / 2) - (footer.amountPerLine.width / 2);
            footer.amountPerLine.y = (footer.amountPerLine.parent.height / 2) - (footer.amountPerLine.height / 2);
            self.mobileCoinSelectorContainer.addChild(self.plusButton)
            self.mobileCoinSelectorContainer.addChild(self.minusButton)

            this.plusButton.x = this.plusButton.parent.width - this.plusButton.width - 5;
            this.plusButton.y = this.plusButton.parent.height - this.plusButton.height - 17;
            this.minusButton.x = 5;
            this.minusButton.y = this.minusButton.parent.height - this.minusButton.height - 17;
            /*this.minusButton.visible = false;
            this.plusButton.visible = false;*/

            this.amountPerLine.visible = true;
            this.coinsImage.visible = false;
        }

        function plus() {
            if (self.disableFooter) {
                return;
            }
            setSettings.STOP_PAYLINE_ANIMATION = true;
            setSettings.ACTIVITY = 8;

            if (sound)
                button.play()

            if (settings.MIN_BET >= 0.01 && settings.MIN_BET < 0.05) {
                self.minusButton.texture = oAL.panel['minus_coin'];
                setSettings.MIN_BET = settings.MIN_BET + 0.01;
            } else if (settings.MIN_BET > 0.04 && settings.MIN_BET < 0.25) {
                setSettings.MIN_BET = settings.MIN_BET + 0.05;
            } else if (settings.MIN_BET >= 0.25 && settings.MIN_BET < 1) {
                setSettings.MIN_BET = settings.MIN_BET + 0.25;
            } else if (settings.MIN_BET >= 1 && settings.MIN_BET < 10) {
                setSettings.MIN_BET = settings.MIN_BET + 1;
            }
            if (settings.MIN_BET === 10) {
                self.plusButton.texture = oAL.panel['disable_plus_coin'];
                button.stop()
            }
        }

        function minus() {
            if (self.disableFooter) {
                return;
            }
            setSettings.STOP_PAYLINE_ANIMATION = true;
            setSettings.ACTIVITY = 8;

            if (sound)
                button.play()


            if (settings.MIN_BET < 0.02) {
                setSettings.MIN_BET = 0.01;

            } else if (settings.MIN_BET < 0.06) {
                setSettings.MIN_BET = settings.MIN_BET - 0.01;
            } else if (settings.MIN_BET > 0.06 && settings.MIN_BET <= 0.25) {
                setSettings.MIN_BET = settings.MIN_BET - 0.05;
            } else if (settings.MIN_BET >= 0.25 && settings.MIN_BET <= 1) {
                setSettings.MIN_BET = settings.MIN_BET - 0.25;
            } else if (settings.MIN_BET <= 10 && settings.MIN_BET > 1) {
                setSettings.MIN_BET = settings.MIN_BET - 1;
                self.plusButton.texture = oAL.panel['plus_coin'];
            }

            if (settings.MIN_BET === 0.01) {
                self.minusButton.texture = oAL.panel['disable_minus_coin'];
                button.stop()
            }
        }
    }

    switchMobileCoinSelector(status = false) {
        let self = this;
        let active = self.mobileCoinSelectorContainer.x === GAME_WIDTH;
        if (status)
            active = false;
        TweenMax.to(self.mobileCoinSelectorContainer, 0.4, {
            x: active ? GAME_WIDTH - self.mobileCoinSelectorContainer.width - 70 : GAME_WIDTH,
            alpha: active ? 1 : 0
        });
    }

    tournamentPanelFadeOutIn(showPreQualifier) {
        if (this.showPreQualifier !== showPreQualifier)
            TweenMax.to(footer[showPreQualifier ? 'preQualificationContainer' : 'qualificationContainer'], 0.4, {
                alpha: 0,
                onComplete: async function (e) {

                    footer[!showPreQualifier ? 'preQualificationContainer' : 'qualificationContainer'].alpha = 0;
                    footer[!showPreQualifier ? 'preQualificationContainer' : 'qualificationContainer'].visible = true;
                    TweenMax.to(footer[!showPreQualifier ? 'preQualificationContainer' : 'qualificationContainer'], 0.8, {alpha: 1})
                }
            });

        this.showPreQualifier = showPreQualifier;
    }

    retrieveFooterValues(isMobile) {
        this.winAmount.style = isMobile ? {font: "10px Arial", fill: "0xffffff", fontSize: 20} : {
            font: "10px Arial",
            fill: "0x000000",
            fontWeight: "bold",
            fontSize: 18
        };

        let linesBet = footerContainer.getChildByName('lines-bet-image').getChildByName('lines-bet-text')
        linesBet.text = settings.GAME_PLAY_MODE === 'demo' ? 'LINES' : 'SPINS LEFT';
        linesBet.x = footer.linesBetImage.width / 2 - linesBet.width / 2;
        this.linesBetAmount.text = "9999";
        this.linesBetAmount.name = 'lines-bet-amount';
        this.linesBetAmount.style = isMobile ? footer.linesBetAmount.style = {
            fill: "0xffffff",
            font: "10px Arial",
            fontSize: 20
        } : {font: "10px Arial", fill: "0x000000", fontWeight: "bold", fontSize: 14};
        this.linesBetAmount.x = this.linesBetImage.width / 2 - this.linesBetAmount.width / 2;
        this.linesBetAmount.y = this.linesBetImage.height - this.linesBetAmount.height;

        this.paylineWin.x = 0;
        this.paylineWin.style = {fill: "0x66b3ff", font: "Arial", fontSize: 12};

        this.betImage.visible = true;
        this.plusButton.visible = true;
        this.minusButton.visible = true;
        this.totalBalanceImage.visible = true;
        freeS.totalWin.visible = false;
        freeS.totalWinAmount.visible = false;

        /*this.winImage.x = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        this.winImage.y = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;*/
        setFooterContainerPosition();
        this.winImage.getChildByName('win-text').style = isMobile ? {
            font: "10px Arial",
            fill: "0xffffff",
            fontSize: 20
        } : {font: "Arial", fill: "0x000000", fontWeight: "bold", fontSize: 20};

        this.footerBgTexture.texture = oAL.backgroundElements['footer_panel'];
        oAL.panel = freeS.panelBackup['original_panel'];
    }

    hideElementsForCurtains(status, isMobile) {
        if (isMobile) {
            this.mobileBetSet.zIndex = status ? 5 : -1;
            this.mobileInfoBar.zIndex = status ? 5 : -1;
            this.soundButton.zIndex = status ? 6 : -1;
            this.soundButton.zIndex = status ? 6 : -1;
            //this.mobileFastSpin.zIndex = status ? 6 : -1;
            this.infoBtn.zIndex = status ? 6 : -1;
            /*this.mobileCloseMenu.zIndex = status ? 6 : -1;*/
            this.coinMenuBtn.zIndex = status ? 6 : -1;
            this.mobileMenu.zIndex = status ? 6 : -1;
            this.mobileCoinPlus.zIndex = status ? 6 : -1;
            this.amountPerLine.zIndex = status ? 6 : -1;
            this.mobileCoinMinus.zIndex = status ? 6 : -1;
            /*this.mobileCoinsClose.zIndex = status ? 6 : -1;*/
            this.mobileCoinsEnable.zIndex = status ? 6 : -1;
            this.spinButtonSprite.zIndex = status ? 8 : -1;
            this.darkSpinButtonSprite.zIndex = status ? 8 : -1;
            this.autoSpinButton.zIndex = status ? 6 : -1;
            this.homeBtn.zIndex = status ? 9 : -1;
            /*this.mobileNormalSpin.zIndex = status ? 6 : -1;*/
        }

    }

    addCoinMenu() {
        let self = this;
        if (isMobile) {
            this.coinMenuContainer = new PIXI.Container();
            this.coinMenuContainer.status = false;
            /*this.mobileMenu.x = 985;*/

            this.coinMenuContainer.sortableChildren = true
            this.coinMenuContainer.zIndex = 6;
            this.coinMenuContainer.name = 'coin-menu-container';
            this.coinMenuContainer.buttonMode = true;

            actionContainer.addChild(this.coinMenuContainer);
            this.coinMenuBtn = new PIXI.Sprite(/*oAL.panel['coins_enable_n']*/);
            this.coinMenuBtn.name = 'coin-btn';
            this.coinMenuBtn.zIndex = 9
            this.coinMenuBtn.interactive = true;
            this.coinMenuContainer.addChild(this.coinMenuBtn);
            this.sliderBar = [];

            this.sliderBar = new PIXI.Sprite(/*oAL.panel['menu_mask_2']*/);
            this.sliderBar.name = 'slider-bar';
            this.sliderBar.x = actionContainer.width;
            this.sliderBar.visible = false;
            this.coinMenuContainer.addChild(this.sliderBar);


            this.coinMenuBtn.on('pointerover', () => coinMenuBtnActions.pointerover());
            this.coinMenuBtn.on('pointerout', () => coinMenuBtnActions.pointerout());
            this.coinMenuBtn.on('pointerdown', () => coinMenuBtnActions.pointerdown());
        }
    }

    addCoinsPlus(isMobile) {
        if (isMobile) {
            this.mobileCoinPlus = new PIXI.Sprite(oAL.panel['coins_plus_n']);
            this.mobileCoinPlus.anchor.x = 0;
            this.mobileCoinPlus.anchor.y = 0;
            this.mobileCoinPlus.x = (function () {
                let position = footerCoordinates.plus_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
                if (isMobile)
                    position = footerCoordinates.plus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

                return position;
            }());
            this.mobileCoinPlus.y = (function () {
                let position = footerCoordinates.plus_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
                if (isMobile)
                    position = footerCoordinates.plus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

                return position;
            }());
            this.mobileCoinPlus.zIndex = 6;
            this.mobileCoinPlus.name = 'coins-plus';
            this.mobileCoinPlus.interactive = true;
            this.mobileCoinPlus.buttonMode = true;

            this.amountPerLine.zIndex = 6;

            this.sliderBar.addChild(this.mobileCoinPlus);
            this.sliderBar.addChild(this.amountPerLine);

            this.mobileCoinPlus.on('pointerover', () => {
                this.mobileCoinPlus.texture = oAL.panel['coins_plus_h'];
            });
            this.mobileCoinPlus.on('pointerout', () => {
                this.mobileCoinPlus.texture = oAL.panel['coins_plus_n'];
            });
            this.mobileCoinPlus.on('pointerdown', plus);

        }
    }

    addCoinsMinus(isMobile) {
        if (isMobile) {
            this.mobileCoinMinus = new PIXI.Sprite(oAL.panel['coins_minus_n']);
            this.mobileCoinMinus.anchor.x = 0;
            this.mobileCoinMinus.anchor.y = 0;
            this.mobileCoinMinus.x = (function () {
                let position = footerCoordinates.minus_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
                if (isMobile)
                    position = footerCoordinates.minus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x

                return position;
            }());
            this.mobileCoinMinus.y = (function () {
                let position = footerCoordinates.minus_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
                if (isMobile)
                    position = footerCoordinates.minus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

                return position;
            }());
            this.mobileCoinMinus.zIndex = 6;
            this.mobileCoinMinus.name = 'coins-minus';
            this.mobileCoinMinus.interactive = true;
            this.mobileCoinMinus.buttonMode = true;

            /*this.app.stage.addChild(this.mobileCoinMinus);*/
            this.sliderBar.addChild(this.mobileCoinMinus);

            this.mobileCoinMinus.on('pointerover', () => {
                this.mobileCoinMinus.texture = oAL.panel['coins_minus_h'];
            });
            this.mobileCoinMinus.on('pointerout', () => {
                this.mobileCoinMinus.texture = oAL.panel['coins_minus_n'];
            });
            this.mobileCoinMinus.on('pointerdown', minus);

        }
    }

    adjustFooter(isMobile) {
        if (isMobile) {
            setTimeout(() => {
                this.footerBgTexture.x = 0;
                this.footerBgTexture.y = 0;
                this.footerBgTexture.zIndex = 8;
                //footerContainer.zIndex = 5;
                /*this.linesBet.style = {fill: "0xffffff", font: "10px Arial", fontSize: 20};
                this.linesBet.text = "LINES";*/
                this.linesBetAmount.style = {fill: "0xffffff", font: "10px Arial", fontSize: 20};
                this.balanceImage.getChildByName('balance-text').style = {
                    font: "10px Arial",
                    fill: "0xffffff",
                    fontSize: 20
                };
                this.userBalance.style = {font: "10px Arial", fill: "0xffffff", fontSize: 20};

                this.bonusImage.getChildByName('bonus-value').style = {
                    font: "10px Arial",
                    fill: "0xffffff",
                    fontSize: 20
                };
                this.bonusImage.getChildByName('bonus-text').style = {
                    font: "10px Arial",
                    fill: "0xffffff",
                    fontSize: 20
                };
                this.bonusImage.visible = true;
                this.winImage.getChildByName('win-text').style = {font: "10px Arial", fill: "0xffffff", fontSize: 20};
                this.winImage.getChildByName('win-amount').style = {font: "10px Arial", fill: "0xffffff", fontSize: 20};
                this.betImage.getChildByName('bet-amount').style = {font: "10px Arial", fill: "0xffffff", fontSize: 20};

                this.soundButton.zIndex = 6;
                this.infoBtn.zIndex = 6;
                this.winAmount.style = {font: "10px Arial", fill: "0xffffff", fontSize: 18};
                //footerContainer.zIndex = 9;
                footer.betAmount.x = footer.betImage.width / 2 - footer.betAmount.width / 2;
                footer.betAmount.y = footer.betImage.height - footer.betAmount.height;
                this.userBonusBalance.y = footer.bonusImage.height - this.userBonusBalance.height;
                this.userBonusBalance.x = footer.bonusImage.width / 2 - this.userBonusBalance.width / 2;
                this.userBalance.y = footer.bonusImage.height - this.userBalance.height;
                this.userBalance.x = footer.bonusImage.width / 2 - this.userBalance.width / 2;
                let winAmount = this.winImage.getChildByName('win-amount');
                winAmount.x = this.winImage.width / 2 - winAmount.width / 2;
                //this.linesBet.x = this.linesBetImage.width / 2 - this.linesBet.width / 2;
                this.linesBetAmount.x = this.linesBetImage.width / 2 - this.linesBetAmount.width / 2;
                this.linesBetAmount.y = footer.linesBetImage.height - this.linesBetAmount.height;
            }, 100);
        } else {
            /*this.linesBet.y = 2
            this.linesBetAmount.y = this.footerBgTexture.height / 2 - this.linesBet.height / 2;
            this.linesBetAmount.x = this.linesBet.width / 2 - this.linesBetAmount.width / 2;*/
        }
    }
}

