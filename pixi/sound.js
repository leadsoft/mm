let sound = true;

const freeSpinBackground = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/freespinBackground.mp3?ver=' + version,
    preload: true
});

const tickerStart = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/tcikerStart.mp3?ver=' + version,
    preload: true
});

const tickerMid = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/tickerMid.mp3?ver=' + version,
    preload: true
});

const tickerEnd = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/tickerEnd.mp3?ver=' + version,
    preload: true
});

const startSpin = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/spin.mp3?ver=' + version,
    preload: true
});

const reelStop = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/reelStop.mp3?ver=' + version,
    preload: true
});

const button = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/button.mp3?ver=' + version,
    preload: true
});

const majorWildWin = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/majorWildWin.mp3?ver=' + version,
    preload: true
});

const minorWin = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/minorWin.mp3?ver=' + version,
    preload: true
});

const majorWin = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/majorWin.mp3?ver=' + version,
    preload: true
});

const lineWin = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/lineWin.mp3?ver=' + version,
    preload: true
});

const curtainStart = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/curtainStart.mp3?ver=' + version,
    preload: true
});

const curtain = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/curtain.mp3?ver=' + version,
    preload: true
});

const curtainClose = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/curtainClose.mp3?ver=' + version,
    preload: true
});

const infoButton = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/infopageButton.mp3?ver=' + version,
    preload: true
});

const scatterDrop1 = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/scatterDrop1.mp3?ver=' + version,
    preload: true
});

const scatterDrop2 = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/scatterDrop2.mp3?ver=' + version,
    preload: true
});

const scatterDrop3 = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/scatterDrop3.mp3?ver=' + version,
    preload: true
});

const freeSpinTrigger = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/freeSpinTrigger.mp3?ver=' + version,
    preload: true
});

const freeSpinStart = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/freeSpinStart.mp3?ver=' + version,
    preload: true
});

const anticipation = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/anticipation.mp3?ver=' + version,
    preload: true
});


/*OLD*/
const scatter = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/regular_win.mp3?ver=' + version,
    preload: true
});

const wildFeature = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/wild_feature.mp3?ver=' + version,
    preload: true
});

const logoSound = PIXI.sound.Sound.from({
    url: 'newAssets/sounds/preloader_logo.mp3?ver=' + version,
    preload: true
});
