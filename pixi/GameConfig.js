const GAME_WIDTH = 1066;
const GAME_HEIGHT = 600;
const REEL_WIDTH = 180;
const SYMBOL_SIZE = 100;
const SYMBOL_CONTAINER_SIZE = 114;
const SYMBOL_WIDTH = 150;
const SYMBOL_CONTAINER_WIDTH = 167;
const SYMBOL_MARGIN_TOP = 15//15;
const REEL_CONTAINER_MARGIN_TOP = 108;
const GAME_ID = 914;
const BIG_SYMBOL_WIDTH = 501
const BIG_SYMBOL_HEIGHT = 342

function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}

let tickerActiveStatus = false;
let firstTick = true;

let testMode = false;
let realChipFirst = false;
if (typeof getQueryParams(document.location.search).test !== "undefined") {
    testMode = true;
    if (getQueryParams(document.location.search).test === 'false')
        testMode = false;
}

if (typeof getQueryParams(document.location.search).realChipFirst !== "undefined") {
    realChipFirst = true;
    if (getQueryParams(document.location.search).realChipFirst === 'false')
        realChipFirst = false;
}

const settings = {
    ACTION_CONTAINER_BUTTONS_MARGIN: 10,
    ACTION_CONTAINER_MARGIN: 100,
    FOOTER_CONTAINER_MARGIN: 60,
    FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN: 10,
    SYMBOL_FRAME_SPEED: 0.300,
    TARGET_PROPERTY: 60,
    MODAL_DIALOG_STATUS: false,
    INITIAL_WIN: false,
    ERROR_LOADING: false,
    REQUEST_END: false,
    STOP_PAYLINE_ANIMATION: false,
    REAL_CHIP_FIRST: realChipFirst,
    CURRENT_SPIN_INDEX: 0,
    STATUS_SENT: false, //use for waitUntilIsDone
    GAME_START: false,
    PROXY_URL: '206.223.191.19:8080',
    LAST_BET_AMOUNT: /*getCookie("LAST_BET_AMOUNT") || */0,
    USER_BALANCE: 0,
    RANK_USER_BALANCE: 0,
    PROMO_CHIPS: '',
    START_GAME_PAYLINES: 25,
    START_GAME: false,
    REEL_NUMBER: 5,
    MIN_BET: 0,
    REEL_TOTAL_MARGIN: 60,//148,
    START_GAME_BET: 0,
    LAST_WIN_AMOUNT: 0,
    CAN_BET: true,
    STOP_REEL: false,
    ACTIVITY: 0,
    GAME_RUNNING: false,
    SPIN_STATUS: false,
    BASE_URL: "206.223.191.28:8083",
    GAME_MODE: 'G',
    TEST_MODE: testMode,
    GAME_USERNAME: typeof getQueryParams(document.location.search).user !== "undefined" ? getQueryParams(document.location.search).user : 0,
    HOME_LINK: typeof getQueryParams(document.location.search).redirectUrl !== "undefined" ? getQueryParams(document.location.search).redirectUrl : window.location.href,
    GAME_PASSWORD: typeof getQueryParams(document.location.search).pwd !== "undefined" ? getQueryParams(document.location.search).pwd : 0,
    CID: typeof getQueryParams(document.location.search).cid !== "undefined" ? getQueryParams(document.location.search).cid : 'TT',
    GAME_VERSION: typeof getQueryParams(document.location.search).vers !== "undefined" ? getQueryParams(document.location.search).vers : '5',
    DEVICE: typeof getQueryParams(document.location.search).clientType !== "undefined" ? getQueryParams(document.location.search).clientType : 'D',
    REAL_PARAMS: typeof getQueryParams(document.location.search).real !== "undefined" ? getQueryParams(document.location.search).real : 'demo',
    GAME_PLAY_MODE: 'demo',
    TOURNAMENT_ID: typeof getQueryParams(document.location.search).tournament !== "undefined" ? getQueryParams(document.location.search).tournament : 0,
    TOURNAMENT_RANK_TYPE: 1,
    TOURNAMENT_TIME: 0,
    TOURNAMENT_RANK: '',
    TOURNAMENT_PLAY_COUNT: '',
    TOURNAMENT_QUALIFY_NUM: '',
    TOURNAMENT_REBUY: false,
    TOURNAMENT_ACCOUNT_CURRENCY: 'USD',
    TOURNAMENT_START: 0,
    TOURNAMENT_END: 0,
    TOURNAMENT_NAME: '',
    TOURNAMENT_REBUY_STAKE: 0,
    TOURNAMENT_REBUY_FEE: 0,
    TOURNAMENT_CURRENCY: 'USD',
    FOOTER_HEIGHT: 80,
    TICK_STATUS: false,
    FSV_STATUS: false,
    FSV_REMAINING: 0,
    FSV_AWARDED: 0,
    FSV_MINIMUM_WIN: 0,
    FSV_MAXIMUM_WIN: 0,
    FSV_TOTAL_WIN: 0,
    FSV_COUPON_ID: "",
    FSV_END: 1,
    TICKER_SOUND_STATUS: false,
    FS_ENABLE_STATUS: false,
    FS_STATUS: false,
    FS_START: false,
    FS_END: false,
    FS_MULTIPLIER: 0,
    FS_SUBSTITUTE_MULTIPLIER: 0,
    FS_TRIGGER_REELS: 0,
    FS_TOTAL_WIN: 0,
    FS_ORIGINAL_COUNT: 0,
    FS_ORIGINAL_MULTIPLIER: 0,
    FS_SPIN_SPUN: 0,
    ANTICIPATION: false,
    RESET_SPIN_VALUE: false,
    FS_FEATURES_STATUS: false,
    FS_FEATURES_NAME: '',
    FS_FEATURES_WIN: 0,
    FS_FEATURES_RES: '',
    SWITCH_LINES_BET: false
};
var setSettings = new Proxy(settings, {
    set: async function (target, key, value) {
        target[key] = value;

        switch (key) {
            case 'FSV_END':
                if (parseInt(value) === 1 && settings.FSV_STATUS) {
                    setSettings.CAN_BET = false;

                    await waitUntilIsDone('GAME_RUNNING');
                    await waitUntilIsDone('BONUS_ROUND');
                    setSettings.FSV_STATUS = false;
                    setSettings.FSV_COUPON_ID = "";
                    setSettings.STATUS_SENT = false;
                    fsv.closeFreeCouponSession();
                }
                if (typeof fsv.awardedText !== "undefined") {
                    await waitUntilIsDone('GAME_RUNNING');
                    setSettings.STATUS_SENT = false;
                    //freeCoupon.awardedText.text = "FREE SPIN \nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FREE_SPIN_TOTAL_WIN).toFixed(2) + "\n\n" + settings.FREE_SPIN_REMAINING + " plays remaining";
                }

                break;
            case 'ANTICIPATION':
                if (value) {
                    anticipation.play();
                } else {
                    game.anticipation.forEach(function (val, key) {
                        val.visible = false;
                        val.gotoAndStop(0)
                    });
                    anticipation.stop();
                }
                break;
            case 'STOP_PAYLINE_ANIMATION':
                if (value) {
                    spinH.removeAllAnimatedSprite();
                    gameCompleted.addRemoveBlurAllSymbols(false);
                    gameCompleted.hideAllLines();
                    gameCompleted.winText.text = '';
                    oAL.winLine.visible = false;
                    game.fakeReelContainer.removeChildren();
                    if (settings.TICK_STATUS) {
                        await waitUntilIsDone('TICK_STATUS');
                        footer.paylineWin.text = '';
                        footer.winAmount.text = ''
                    } else {
                        footer.paylineWin.text = '';
                        footer.winAmount.text = ''
                    }
                }
                break;
            case 'SPIN_STATUS':
                if (value) {
                    oAL.tick.remainValue = 0;
                }
                break;
            case 'START_GAME':
                if (value && settings.FSV_STATUS) {
                    fsv.awardedTextControl(true);
                    fsv.awardedTextControl(true);
                }
                break;
            case 'TICK_STATUS':
                if (value && !settings.GAME_RUNNING && oAL.tick.playTick) {
                    /*tickerStartBalance();*/
                }
                break;
            case 'TOURNAMENT_PLAY_COUNT':
                footer.betsMade.text = value;
                footer.betsMade.x = (footer.betsMade.parent.width / 2) - (footer.betsMade.width / 2);
                footer.betsMade.y = (footer.betsMade.parent.height / 2) - (footer.betsMade.height / 2);
                break;
            case 'TOURNAMENT_QUALIFY_NUM':
                if (parseInt(value) === 0 && footer.betsToQualify.text !== value) {
                    await waitUntilIsDone('GAME_RUNNING');
                    await sleep(200);
                    footer.tournamentPanelFadeOutIn(true);
                } else if (parseInt(value) > 0) {
                    footer.tournamentPanelFadeOutIn(false);
                }

                footer.betsToQualify.text = value;
                footer.betsToQualify.x = (footer.betsToQualify.parent.width / 2) - (footer.betsToQualify.width / 2);
                footer.betsToQualify.y = (footer.betsToQualify.parent.height / 2) - (footer.betsToQualify.height / 2);
                break;
            case 'TOURNAMENT_RANK':
                footer.rank.text = value;
                footer.rank.x = (footer.rank.parent.width / 2) - (footer.rank.width / 2);
                footer.rank.y = (footer.rank.parent.height / 2) - (footer.rank.height / 2);
                break;
            case 'TOURNAMENT_TIME':
                var now = moment(settings.TOURNAMENT_TIME, "YYYYMMDD H:mm").toDate();
                var then = moment(settings.TOURNAMENT_END, "YYYYMMDD H:mm").toDate();
                var diff = moment.duration(moment(then).diff(moment(now)));
                var days = parseInt(diff.asDays()); //84
                var hours = parseInt(diff.asHours()); //2039 hours, but it gives total hours in given miliseconds which is not expacted.
                hours = hours - days * 24;  // 23 hours
                var minutes = parseInt(diff.asMinutes()); //122360 minutes,but it gives total minutes in given miliseconds which is not expacted.
                minutes = minutes - (days * 24 * 60 + hours * 60); //20 minutes.

                footer.timeLeft.text = days + 'd ' + hours + 'h ' + minutes + 'm';
                footer.timeLeft.x = (footer.timeLeft.parent.width / 2) - (footer.timeLeft.width / 2);
                footer.timeLeft.y = (footer.timeLeft.parent.height / 2) - (footer.timeLeft.height / 2);
                break;
            case 'USER_BALANCE':
                footer.userBalance.text = user.currencySymbol + formatMoney(parseFloat(value).toFixed(2));
                /*footer.userBalance.x = (footer.userBalance.parent.width / 2) - (footer.userBalance.width / 2);
                footer.userBalance.y = (footer.userBalance.parent.height / 2) - (footer.userBalance.height / 2);*/
                if (settings.DEVICE.includes('D')) {
                    footer.userBalance.x = 50;
                    footer.userBalance.y = -24.5;
                }
                break;
            case 'RANK_USER_BALANCE':
                footer.rankBalance.text = user.currencySymbol + formatMoney(parseFloat(value).toFixed(2));
                footer.rankBalance.x = (footer.rankBalance.parent.width / 2) - (footer.rankBalance.width / 2);
                footer.rankBalance.y = (footer.rankBalance.parent.height / 2) - (footer.rankBalance.height / 2);
                break;
            case 'PROMO_CHIPS':
                await waitUntilIsDone('SPIN_STATUS');
                footer.userBonusBalance.text = user.currencySymbol + formatMoney(parseFloat(value).toFixed(2));
                /*footer.userBonusBalance.x = (footer.userBonusBalance.parent.width / 2) - (footer.userBonusBalance.width / 2);
                footer.userBonusBalance.y = (footer.userBonusBalance.parent.height / 2) - (footer.userBonusBalance.height / 2);*/
                if (settings.DEVICE.includes('D')) {
                    footer.userBonusBalance.x = 38.5;
                    footer.userBonusBalance.y = -24.5;
                }
                break;
            case 'GAME_PLAY_MODE':
                document.title += ' - ' + value;

                break;
            case 'MIN_BET':
                footer.amountPerLine.text = user.currencySymbol + value.toFixed(2);
                setSettings.START_GAME_BET = settings.MIN_BET * settings.START_GAME_PAYLINES;
                if (settings.DEVICE.includes('D')) {
                    footer.amountPerLine.x = (footer.amountPerLine.parent.width / 2) - (footer.amountPerLine.width / 2);
                    footer.amountPerLine.y = (footer.amountPerLine.parent.height / 2) - (footer.amountPerLine.height / 2) + (footer.amountPerLine.parent.mobile ? 17 : 0);
                }
                break;
            case 'START_GAME_BET':
                footer.betAmount.text = user.currencySymbol + value.toFixed(2);
                /*footer.betAmount.x = (footer.betAmount.parent.width / 2) - (footer.betAmount.width / 2);
                footer.betAmount.y = (footer.betAmount.parent.height / 2) - (footer.betAmount.height / 2);*/
                //if(settings.DEVICE.includes('D')) {
                setTimeout(() => {
                    footer.betAmount.x = footer.betImage.width / 2 - footer.betAmount.width / 2;
                    footer.betAmount.y = footer.betImage.height - footer.betAmount.height;
                }, 1000)

                //}
                break;
            case 'LAST_WIN_AMOUNT':
                winAmount = value.toFixed(2);
                if (settings.FS_STATUS) {
                    freeS.awardedTextControl();
                }

                if (value === 0) {
                    current = 0;
                    //footer.winAmount.x = (footer.winAmount.parent.width / 2) - (footer.winAmount.width / 2);
                    footer.winAmount.x = 374.5;
                    //footer.winAmount.y = (footer.winAmount.parent.height / 2) - (footer.winAmount.height / 2);
                    footer.winAmount.y = 2.5;
                }

                break;
            case "STOP_REEL":
                footer.showHideStopSpinButton(false);
                if (freeS.curtainsStatus && !settings.FS_STATUS)
                    footer.showHideDarkSpinButton(true, 'STOP_REEL');
                break;
            case 'FS_START':
                if (value && !settings.FS_ENABLE_STATUS && settings.FS_STATUS) {
                    await waitUntilIsTrue('GAME_START');
                    await sleep(1000)
                    freeS.startFreeSpin();
                }
                break;
            case 'FS_STOP':
                if (value && settings.FS_ENABLE_STATUS && !settings.FS_STATUS) {
                    //await sleep(500);
                    freeS.stopFreeSpin();
                }
                break;
            case 'FS_STATUS':
                await waitUntilIsDone('GAME_RUNNING');
                if (value && !settings.FS_ENABLE_STATUS) {
                    disableEnableButtons(false);
                    footer.showHideDarkSpinButton(true, 'FS_STATUS');
                }
                break;
            default:
                break;
        }

        return true;
    }
});
let winAmount = 0;
let current = 0;

function startInterval() {
    setInterval(async function () {
        if (settings.ACTIVITY === 0) {
            if (oAL.sound.background.volume > 0.24)
                for (let i = 0; i < 200; i++) {
                    await sleep(5);
                    if (oAL.sound.background.volume > 0.02)
                        oAL.sound.background.volume -= 0.001;
                }
        } else {
            oAL.sound.background.volume = 0.25;
            setSettings.ACTIVITY -= 1;
        }
    }, 1000);
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
