class Rebuy {

    constructor(gameContainer) {
        this.gameContainer = gameContainer;
        this.rebuyPopupContainer = new PIXI.Container();
        this.rebuyPopupContainer.interactive = false;
        this.rebuyPopupContainer.button = true;

        this.rebuyPopupContainer.on('pointerdown', this.showRebuyModal);
        this.rebuyPopupContainer.name = 'rebuy-container';

        /*this.rebuyPanelSprite = new PIXI.Sprite(PIXI.Texture.fromImage('./newAssets/rebuy_popup_seq/rebuy_popup0015.png'));*/
        this.rebuyPanelSprite = new PIXI.Sprite();
        this.rebuyPopupContainer.addChild(this.rebuyPanelSprite);


        this.rebuyText = new PIXI.Text('REBUY', {font: "Arial", fill: "0xffe000"});
        this.rebuyText.name = 'rebuyText';
        this.rebuyText.style.fontSize = 18;
        this.rebuyPopupContainer.addChild(this.rebuyText);
    }

    showRebuy() {
        //this.rebuyPanelSprite.texture = oAL.footer['rebuy_pop_up'];
        this.gameContainer.addChild(this.rebuyPopupContainer);

        this.rebuyPopupContainer.zIndex = 0;
        this.rebuyPopupContainer.alpha = 0;
        this.rebuyPopupContainer.x = (this.gameContainer.width / 2) - (this.rebuyPopupContainer.width / 2);
        this.rebuyPopupContainer.y = this.gameContainer.height - footer.footerContainer.height

        this.rebuyText.x = (this.rebuyText.parent.width / 2) - (this.rebuyText.width / 2);
        this.rebuyText.y = (this.rebuyText.parent.height / 2) - (this.rebuyText.height / 2);
    }

    showRebuyPopup() {
        let self = this;
        TweenMax.to(this.rebuyPopupContainer, 0.5, {
            y: this.gameContainer.height - this.rebuyPopupContainer.height - footer.footerContainer.height,
            alpha: 1,
            onComplete: function (e) {
                self.rebuyPopupContainer.interactive = true;
            },
        });
    }

    hideRebuyPopup() {
        let self = this;
        TweenMax.to(this.rebuyPopupContainer, 0.3, {
            y: this.gameContainer.height - footer.footerContainer.height,
            alpha: 0,
            onComplete: function (e) {
                self.rebuyPopupContainer.interactive = false;
            },
        });
    }

    showRebuyModal() {
        //let text = 'Would you like to Rebuy into tournament ' + settings.TOURNAMENT_ID + ' (' + settings.TOURNAMENT_NAME + ')? Your balance will be debited ' + user.currency+ formatMoney(parseFloat(settings.TOURNAMENT_REBUY_STAKE).toFixed(2));
        /*let text = new PIXI.Text("Would you like to Rebuy into tournament |@count#FFFF00^ (@name)?\nIf Yes, your balance will be debited |@string @value#FFFF00^.", {fontFamily : 'Arial', fontSize: 12, fill : 0xff1010, align : 'center'})*/

        /*TODO: request for currency*/
        if (setSettings.TOURNAMENT_CURRENCY !== user.currency)
            requestForCurrency();

        let text = "TEST";
        /*let text = new pixiMultistyleText("Let's make some <ml>multiline</ml>\nand <ms>multistyle</ms> text for\n<pixi>Pixi.js!</pixi>",
            {
                "default": {
                    fontFamily: "Arial",
                    fontSize: "24px",
                    fill: "#cccccc",
                    align: "center"
                },
                "ml": {
                    fontStyle: "italic",
                    fill: "#ff8888"
                },
                "ms": {
                    fontStyle: "italic",
                    fill: "#4488ff"
                },
                "pixi": {
                    fontSize: "64px",
                    fill: "#efefef"
                }
            });*/


        /*modalDialog.show(messages.rebuy_default, 'rebuy');*/
        modalDialog.showRebuyDefaultModal(messages.rebuy_default, 'rebuy');
    }

    hideRebuyModal() {

    }
}


