(function () {
    var _debug, _iPhoneManager, _enabled = false, _mainWindow, _resizeDelayMS = 300, _isGameInit = false;

    function iPhoneManager() {
        // manages full screen expereince on iOS devices
        // initiated by gesture from user
        var _rootEl, _slideContainerEl, _slideAnimEl, _isLargeAddressBar, _isLandscape, _clientHeight,
            _portraitMinUIRatio = 0.91502,
            _landscapeMinUIRatio = 0.94933,
            _s1ImgPath = "./lib/common/ios/slide.png",
            _s2ImgPath = "./ios/slide.png";

        function init() {
            checkLandscape();
            initElements();
        }

        function initElements() {
            _rootEl = createElement("fullscreen-root-hidden", true);        // create root element
            createAnimElement();
        }

        function createElement(className, append) {
            var div = document.createElement("div");
            div.className = className;
            if (append) {
                document.body.appendChild(div);
            }
            return div;
        }

        function createAnimElement() {
            var div = createElement("hide", true);
            div.id = "slide-container";

            var img = document.createElement("img");
            img.className = "slide-up";
            img.id = "slide-img";


            var isS1 = document.documentElement.className.indexOf("s1") !== -1;
            img.src = isS1 ? _s1ImgPath : _s2ImgPath;

            div.appendChild(img);

            _slideContainerEl = div;
            _slideAnimEl = img;

            _slideAnimEl.onanimationend = onSlideAnimationEnd.bind(this);
        }

        function onSlideAnimationEnd() {
            if (_debug) {
                console.log("onSlideAnimationEnd");
                console.log("this: " + this);
            }
            hideSlideAnim();
            updateStyle(false);
        }

        // check to see if fullscreen root is visible and update the style 
        function updateStyle(isVisible) {
            _rootEl.className = isVisible ? "fullscreen-root-visible" : "fullscreen-root-hidden";
        }

        // key logic module
        function resizeHandler(event) {
            if (_debug) {
                console.log("[iPhoneScroll] ============================================= resizeHandler(" + event + ")");
            }

            if (!_isGameInit) {
                return;
            }

            checkLandscape();

            var screen = window.screen;
            var screenHeight = _isLandscape ? Math.min(screen.width, screen.height) : Math.max(screen.width, screen.height);

            _clientHeight = getClientHeight();
            var wasLargeAddress = _isLargeAddressBar;
            _isLargeAddressBar = checkLargeAddressBar(screenHeight);

            if (_isLargeAddressBar) {

                if (!wasLargeAddress) {
                    if (_debug) {
                        console.log("[iPhoneScroll] onResize() ====== fullscreen overlay SHOWN ======== ");
                    }
                    updateStyle(true);
                    showSlideAnim();
                }
            } else {
                if (wasLargeAddress) {
                    if (_debug) {
                        console.log("[iPhoneScroll] onResize()  ====== fullscreen overlay HIDE ======== ");
                    }
                    updateStyle(false);
                    hideSlideAnim();
                }
            }
        }

        function checkLandscape() {
            _isLandscape = window.innerWidth > window.innerHeight;
        }

        function checkLargeAddressBar(screenHeight) {
            // check if address bar is large or small
            var ratio = window.innerHeight / screenHeight;
            if (_debug) {
                console.log(" checkLargeAddress()");
                console.log("       window.innerHeight: " + window.innerHeight);
                console.log("       screenHeight: " + screenHeight);
                console.log("       ratio: " + ratio);
            }

            if (_isLandscape) {
                return ratio < _landscapeMinUIRatio;
            } else {
                return ratio < _portraitMinUIRatio;
            }
        }

        function getClientHeight() {
            if (_debug) {
                console.log("[iPhoneScroll] getClientHeight() -> window.innerHeight: " + window.innerHeight);
            }
            var height = window.innerHeight;
            return height;
        }

        function showSlideAnim() {
            _slideContainerEl.className = "show";
        }

        function hideSlideAnim() {
            _slideContainerEl.className = "hide";
        }

        init();
        var iPhoneManger = {};
        iPhoneManger.resizeHandler = resizeHandler;
        return iPhoneManger;
    }


    function debounce(fxn, delay) {
        let timeoutID;
        if (_debug) {
            console.log("[debounce] fxn " + fxn + " delay: " + delay);
        }

        return function () {
            const context = this;
            const args = arguments;
            if (timeoutID) {
                clearTimeout(timeoutID);
            }
            timeoutID = setTimeout(() => {
                if (_debug) {
                    console.log("[debounce] exec debounced fxn " + fxn + " after delay: " + delay);
                }
                fxn.apply(context, args)
            }, delay);
        }
    }

    function checkiOS() {
        var agent = navigator.userAgent;
        return (agent.indexOf("iPod") > -1 || agent.indexOf("iPhone") > -1 || agent.indexOf("iPad") > -1);
    }

    function init() {
        checkDebug();
        if (_debug) {
            console.log("[IOSFullScreen]    init()");
        }

        _enabled = checkiOS();
        if (!_enabled) {
            return;
        }

        // add iPhone to MainWindow (<html>) element
        var mainWindow = document.getElementsByClassName("MainWindow")[0];
        if (!mainWindow) {
            if (_debug) {
                console.log("[IOSFullScreen] MainWindow class not detected, aborting init");
            }
            return;
        }
        _mainWindow = mainWindow;
        _mainWindow.className += " iPhone";

        initIPhoneManager();
    }

    function checkDebug() {
        var query = window.location.search.substring(1);
        let regex = new RegExp("&debug=true&");
        _debug = regex.test(query);
    }

    function initIPhoneManager() {
        if (_debug) {
            console.log("[IOSFullScreen] initIPhoneManager()");
        }
        var manager = iPhoneManager && iPhoneManager();
        if (!manager) {
            if (_debug) {
                console.log("[iPhoneScroll] manager not initated, aborting");
            }
            return;
        }
        _iPhoneManager = manager;

        var onResize = function () {
            if (_debug) {
                console.log("[iPhoneScroll] window event onResize()");
            }
            manager.resizeHandler.apply(manager, arguments);
        };

        window.addEventListener("resize", debounce(onResize, _resizeDelayMS), false);

        // resetPhoneManager();
    }

    function resetPhoneManager() {
        if (_enabled) {
            _iPhoneManager && _iPhoneManager.resizeHandler();
            window.scrollTo(0, 0);
        }
    }

    function onInit() {
        _isGameInit = true;
        resetPhoneManager();
    }

    var iOSGesture = {};
    iOSGesture.onInit = onInit.bind(this);

    // main function 
    init();
    window.iOSGesture = iOSGesture;
})();
